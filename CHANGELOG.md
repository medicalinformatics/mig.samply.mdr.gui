# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.3.1] - 2020-06-24
### Fixed
- concept association query reply is handled correctly now

## [2.3.0] - 2020-06-24
### Added
- docker files included
- concept association added to staging import
### Removed
- identifiers are no longer shown during element creation
- in the related elements view, the current element is no longer displayed in "other representations"
### Fixed
- Updating dataelementgroups working as intended again
- name of source correctly shown in element creation wizard
- typos fixed

## [2.2.1] - 2020-06-12
### Fixed
- Wrong Value Domain Type on import was fixed in new dal version

## [2.2.0] - 2020-06-12
### Added
- Tree comparison of namespaces
- Similarity search between 2 namespaces
- Now supports keycloak > 4.4.0
- An 'about' dialog has been added to see build information
- Use JOOQ as database library
- Support auth server move via env variable
- Page to manage credentials for other sources
### Changed
- Drafts are now deleted instead of being marked as outdated
- Update parent.pom
### Removed
- Samply simple DAO removed
### Fixed
- Performance issue solved when converting staged elements to drafts
- Negative values for max string length are no longer allowed
- The button to add permitted values no longer disappears when the content of the page is too long
- Startup no longer fails if no log4j config file is found

## [2.1.0] - 2019-09-06
### Added
- Add recommendation search and selection when creating a new data element
### Changed
- Excel import supports elements without defined validation
- Excel import supports string, integer and float elements without further constraints without 
requiring an extra validation entry
### Fixed
- Fixed misleading labels on import file page
- Failed attempts to contact MDM no longer prevent creating/editing dataelements

## [2.0.1] - 2019-07-10
### Fixed
- Error when creating new dataelement

## [2.0.0] - 2019-07-02
### Added
- Missing translations for languages
- Flyway for database migration (from mdr-dal)
- Concept association added to dataelements
### Deprecated
- Local date format
- Local time format
### Removed
- Homebrew database upgrade mechanism (from mdr-dal)
- Maintenance mode (from mdr-dal)

## [1.12.0] - 2019-04-25
### Added
- Dataelements without validation type can be created as drafts
### Changed
- Staging area is now tree-based with inspire tree
- google codestyle applied

## [1.11.0] - 2019-02-08
### Added
- Excel Import via GUI
- XML Import via REST
- Staging Area to display imported elements
### Changed
- Use the XSD-defined format as standard for staged elements and convert excel files accordingly
- Scroll namespaces and elements independently
### Removed
- Identifiers of dataelements are no longer customizable
### Fixed
- Paginator for staging are fixed
- Bug in permitted value creation fixed
- Redirect URI fixed when scheme is changed by proxy redirect

## [1.10.9] - 2018-03-23
### Changed
- Allow choosing a namespace identifier instead of using a fixed one
### Fixed
- Menu no longer instantly closing on screens with width < 990px
- Race condition when creating namespaces will now be avoided

## [1.10.8] - 2018-01-25
### Added
- Initial Release