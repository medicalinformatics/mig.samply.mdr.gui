FROM tomcat:9-jdk8-openjdk-slim

EXPOSE 80/tcp

ENV AUTH_HOST="" \
    AUTH_PUB_KEY="" \
    AUTH_REALM="" \
    USE_SAMPLY_AUTH="false" \
    CLIENT_ID="" \
    CLIENT_SECRET="" \
    DB_HOST="" \
    DB_NAME="samplymdr" \
    DB_USER_NAME="samplymdr" \
    DB_USER_PW="samplymdr" \
    CATALINA_OPTS="-Djava.awt.headless=true -Xmx2g -Xms256m -XX:+UseConcMarkSweepGC -XX:MaxPermSize=2g -Djava.util.prefs.userRoot=/usr/local/tomcat" \
    DEBUG="false" \
    JPDA_ADDRESS="8000" \
    JPDA_TRANSPORT="dt_socket"

ARG MVN_USER
ARG MVN_PW
ARG VERSION

RUN apt-get update && apt-get install -y coreutils curl iputils-ping openssl postgresql-client unzip vim procps \
  && mkdir -p /conf/samply \
  && mkdir -p /conf/tomcat

COPY docker/certs/ /usr/local/share/ca-certificates/
COPY docker/conf/ /conf/samply/

RUN sed -i -e 's^8080^80^g' conf/server.xml \
  && cp conf/* /conf/tomcat/

COPY docker/base.sh .
COPY docker/start.sh .
COPY docker/run_tomcat.sh .
RUN chmod +x ./base.sh
RUN chmod +x ./run_tomcat.sh
RUN chmod +x ./start.sh
COPY target/mdr-gui.war webapps/ROOT.war

RUN cd webapps \
  && curl -X GET https://repo.mig-frankfurt.de/repository/oss-release/de/mig/samply/mdr-rest/4.4.0/mdr-rest-4.4.0.war -u $MVN_USER:$MVN_PW --output rest.war \
  && unzip -q rest.war -d rest \
  && rm rest.war

LABEL VERSION=$VERSION

CMD ./start.sh && ./run_tomcat.sh
