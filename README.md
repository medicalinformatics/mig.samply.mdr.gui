# Samply MDR GUI

Samply MDR is an implementation of ISO 11179-3. It offers various REST
interfaces, a convenient graphical user interface, an adapter to other
MDR implementations like the caDSR MDR, and other great features.

This project implements the Graphical User Interface *only*.

## Installation

See the MDR Bundle install guide.

## Usage

Open your web browser and navigate to the application, e.g.
`http://localhost:8080/mdr-gui`, see your application servers documentation for more information.
The admin interface is available under the `/admin/` path, e.g. `https://mdr.samply.de/admin` or `http://localhost:8080/mdr-gui/admin/`.

## Technology

This project uses the following technologies:

* Servlet filter for authentication (OAuth2) and authorization, e.g. the /user/ folder
* JSF 2.2 (currently the mojarra implementation)
    * composite components for
        * lists of definitions and designations
        * lists of elements and a single element
        * the details view of an elements
        * the constraints of a namespace
        * list of permitted values
        * list of slots
        * the value domain of a data element
* Bootstrap, JQuery(-UI), Select2,

## Build

Use maven to build the `war` file:

```
mvn clean package
```
