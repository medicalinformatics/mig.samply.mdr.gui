Samply MDR Docker Image
=======================

## Build

To build the image run in the root folder `docker build 
--no-cache
--build-arg VERSION="<version>" 
--build-arg MVN_USER="<nexus user>"
--build-arg MVN_PW="<nexus user password>"
-t docker.mig-frankfurt.de/samply/mdr:<version>
.`  
Example:  
`docker build --no-cache
 --build-arg VERSION="2.2.0"
 --build-arg MVN_USER="someuser"
 --build-arg MVN_PW="somepassword"
-t docker.mig-frankfurt.de/samply/mdr:2.2.0
 .`
 
## Usage

To run the mdr container, you need a postgresql database and a keycloak server. If you create a postgresql
container with the provided `docker-compose.db.yml`, the `init.sql` script is run on startup, creating
the needed database and user. If you choose to use an existing postgresql server, create a database
and user there, following the example in `init.sql`.