#!/bin/bash

CERT_DIR="/certs"

# copy config files
cp -ru /conf/samply/* /etc/samply/
cp -ru /conf/tomcat/* /usr/local/tomcat/conf/

# import certs

if [ -d "$CERT_DIR" ];then
    for file in $CERT_DIR/*; do
        keytool -importcert -file $file -keystore /usr/lib/jvm/java-1.7-openjdk/jre/lib/security/cacerts -noprompt -storepass changeit
    done
fi

# wait for db connection

if [ ! -z $DB_HOST ]; then
  export PGPASSWORD=
  echo "checking for db..."
  until psql postgresql://$DB_USER_NAME:$DB_USER_PW@$DB_HOST:$DB_PORT/$DB_NAME -c '\l'; do
    sleep 1
  done
  echo "db found"
fi

exit 0
