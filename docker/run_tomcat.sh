#!/bin/bash

if [ $DEBUG == "true" ]; then
  catalina.sh jpda run
elif [ $DEBUG == "false" ]; then
  catalina.sh run
else
  echo "ERROR: wrong DEBUG environment variable:" $DEBUG
  echo "valid values are: true, false"
  echo "aborting ..."
  exit 1
fi

exit 0