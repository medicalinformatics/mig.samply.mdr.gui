#!/bin/bash
./base.sh
echo $?

sed -i -e "s^AUTH_HOST^"$AUTH_HOST"^g" "/etc/samply/mdr.oauth2.xml"
sed -i -e "s^AUTH_PUB_KEY^"$AUTH_PUB_KEY"^g" "/etc/samply/mdr.oauth2.xml"
sed -i -e "s^CLIENT_ID^"$CLIENT_ID"^g" "/etc/samply/mdr.oauth2.xml"
sed -i -e "s^CLIENT_SECRET^"$CLIENT_SECRET"^g" "/etc/samply/mdr.oauth2.xml"
sed -i -e "s^AUTH_REALM^"$AUTH_REALM"^g" "/etc/samply/mdr.oauth2.xml"
if [ $USE_SAMPLY_AUTH == "false" -o $USE_SAMPLY_AUTH == "true" ]; then
  sed -i -e "s^USE_SAMPLY_AUTH^"$USE_SAMPLY_AUTH"^g" "/etc/samply/mdr.oauth2.xml"
else
  echo "wrong ALLOW_SELF_SIGNED_CERTS environment variable: "$USE_SAMPLY_AUTH
  echo "valid values are: true, false"
  echo "aborting ..."
  exit 1
fi

sed -i -e "s^DB_HOST^"$DB_HOST"^g" "/etc/samply/mdr.postgres.xml"
sed -i -e "s^DB_NAME^"$DB_NAME"^g" "/etc/samply/mdr.postgres.xml"
sed -i -e "s^DB_USER_NAME^"$DB_USER_NAME"^g" "/etc/samply/mdr.postgres.xml"
sed -i -e "s^DB_USER_PW^"$DB_USER_PW"^g" "/etc/samply/mdr.postgres.xml"

update-ca-certificates

for file in /usr/local/share/ca-certificates/*; do
    if [ -f "$file" ]; then
      	file2=${file##*/}
	      alias=${file2%.*}
        echo "importing $file to keystore as $alias"
        keytool -import -alias "$alias" -keystore /usr/local/openjdk-8/jre/lib/security/cacerts -storepass changeit -noprompt -trustcacerts -file "$file"
    fi
done

exit 0
