/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.datasource;

import com.google.gson.Gson;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ComparisonDao;
import de.samply.mdr.gui.beans.DataElementWizard;
import de.samply.mdr.gui.beans.UserBean;
import de.samply.mdr.util.Message;
import de.samply.mdr.util.StagingUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/** A pseudo REST resource that can access managed beans and provides recommendations. */
@ManagedBean
@RequestScoped
public class RecommendationDatasource {

  private static final String QUERY_PARAM_SEARCHSTRING = "searchstring";
  private static final String QUERY_PARAM_LANGUAGE = "language";
  private static final String QUERY_PARAM_LIMIT = "limit";
  private static final String QUERY_PARAM_THRESHOLD = "threshold";
  private static final Logger logger = LogManager.getLogger(RecommendationDatasource.class);
  private static final String CONTENT_TYPE_JSON_UTF8 = "application/json; charset=utf-8";
  private static final Gson gson = new Gson();

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{dataElementWizard}")
  private DataElementWizard dataElementWizard;

  /** TODO: add javadoc. */
  public void getRecommendedElements() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    final HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
    Map<String, String> queryParameterMap =
        StagingUtil.getQueryParameters(request.getQueryString());

    String searchString = queryParameterMap.get(QUERY_PARAM_SEARCHSTRING);
    dataElementWizard.setSearchedString(searchString);
    // TODO: This should be used at some point in the future.
    String language = queryParameterMap.get(QUERY_PARAM_LANGUAGE);
    int limit = Integer.parseInt(queryParameterMap.get(QUERY_PARAM_LIMIT));
    dataElementWizard.setLimit(limit);
    int threshold = Integer.parseInt(queryParameterMap.get(QUERY_PARAM_THRESHOLD));
    dataElementWizard.setThreshold(threshold);

    response.setContentType(CONTENT_TYPE_JSON_UTF8);
    try {
      PrintWriter printWriter = response.getWriter();
      String rs = "";
      if (userBean.getLoginValid()) {
        try (DSLContext ctx = ResourceManager.getDslContext()) {
          rs = gson.toJson(ComparisonDao.getRecommendedElements(
              ctx, userBean.getUserId(), searchString, limit, threshold));
        } catch (DataAccessException e) {
          logger.error("Error while trying to get recommended elements: " + e);
          Message.addError("serverError");
        }
      }
      logger.debug(rs);
      printWriter.print(rs);
    } catch (IOException ex) {
      logger.error(ex);
      throw new FacesException(ex);
    }
    context.responseComplete();
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public DataElementWizard getDataElementWizard() {
    return dataElementWizard;
  }

  public void setDataElementWizard(DataElementWizard dataElementWizard) {
    this.dataElementWizard = dataElementWizard;
  }
}
