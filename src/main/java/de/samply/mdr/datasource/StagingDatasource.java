/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.datasource;

import com.google.gson.Gson;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ImportDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.StagingDao;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.gui.beans.StagingBean;
import de.samply.mdr.gui.beans.UserBean;
import de.samply.mdr.util.StagingUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;

/** A pseudo REST resource that can access managed beans. */
@ManagedBean
@RequestScoped
public class StagingDatasource {

  public static final String QUERY_PARAM_NODENAME = "node";
  private static final Logger logger = LogManager.getLogger(StagingDatasource.class);
  private static final String CONTENT_TYPE_JSON_UTF8 = "application/json; charset=utf-8";
  private static final Gson gson = new Gson();

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{stagingBean}")
  private StagingBean stagingBean;

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public StagingBean getStagingBean() {
    return stagingBean;
  }

  public void setStagingBean(StagingBean stagingBean) {
    this.stagingBean = stagingBean;
  }

  /** TODO: add javadoc. */
  public void getNamespaceNodes(String namespaceName) {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
    Map<String, String> queryParameterMap =
        StagingUtil.getQueryParameters(request.getQueryString());

    response.setContentType(CONTENT_TYPE_JSON_UTF8);
    try {
      PrintWriter printWriter = response.getWriter();
      String rs;
      if (userBean.getLoginValid()) {
        rs = getMembers(queryParameterMap);
      } else {
        rs = "";
      }
      logger.debug(rs);
      printWriter.print(rs);
    } catch (IOException ex) {
      throw new FacesException(ex);
    }
    context.responseComplete();
  }

  private String getMembers(Map<String, String> queryParameterMap) {
    String nodeName = queryParameterMap.get(QUERY_PARAM_NODENAME);

    if ("root".equals(nodeName)) {
      return StagingUtil.createInitialTree(stagingBean.getNamespaces());
    } else {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        if (nodeName.startsWith(StagingUtil.NODE_PREFIX_NAMESPACE)) {
          DescribedElement namespace = NamespaceDao.getNamespace(ctx, userBean.getUserId(),
              nodeName.substring(StagingUtil.NODE_PREFIX_NAMESPACE.length()));
          Namespace ns = (Namespace) namespace.getElement();
          List<Import> imports =
              ImportDao.getImportsFromUserAndNamespace(ctx, userBean.getUserId(), ns.getId());
          return StagingUtil.createImportTreeNodes(imports);
        } else if (nodeName.startsWith(StagingUtil.NODE_PREFIX_IMPORT)) {
          String idString = nodeName.substring(StagingUtil.NODE_PREFIX_IMPORT.length());
          try {
            List<Staging> elements =
                StagingDao.getStagedRootElementsByImportId(ctx, Integer.parseInt(idString));
            return StagingUtil.createTreeNodes(userBean.getUserId(), elements);
          } catch (Exception nfe) {
            logger.error("Invalid import id requested: " + nodeName + ": " + nfe.getMessage());
          }
        } else {
          List<Staging> elements =
              StagingDao.getStagedElementsByParentId(ctx, Integer.parseInt(nodeName));
          return StagingUtil.createTreeNodes(userBean.getUserId(), elements);
        }
      }
    }
    return "";
  }
}
