/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui;

import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Message;
import de.samply.string.util.StringUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Some validators that validate definitions, slots, permissible values, etc.
 *
 * @author paul
 */
public class BeanValidators {

  /** Disable instantiation. */
  private BeanValidators() {}

  /** Validated the given value domain. */
  public static boolean validateValidation(ValueDomainDto valueDomain) {
    boolean valid = true;

    switch (valueDomain.getDatatype()) {
      case LIST:
        /*
         * Check if every value is unique and not empty.
         */
        HashSet<String> permittedValues = new HashSet<>();
        for (AdapterPermissibleValue v : valueDomain.getPermittedValues()) {
          if (permittedValues.contains(v.getValue())) {
            Message.addError("duplicateValue");
            valid = false;
          } else {
            permittedValues.add(v.getValue());
          }

          if (StringUtil.isEmpty(v.getValue())) {
            Message.addError("emptyValue");
            valid = false;
          }

          valid = BeanValidators.validateDefinitions(v.getDefinitions()) && valid;
        }
        break;

      case INTEGER:
        /*
         * Check if the range makes sense.
         */
        if (valueDomain.getWithinRange()
            && valueDomain.getUseRangeFrom()
            && valueDomain.getUseRangeTo()) {
          if (!((valueDomain.getRangeFrom() instanceof Long)
                  && (valueDomain.getRangeTo() instanceof Long))
              && !((valueDomain.getRangeFrom() instanceof BigDecimal)
                  && (valueDomain.getRangeTo() instanceof BigDecimal))
              && !((valueDomain.getRangeFrom() instanceof BigInteger)
                  && (valueDomain.getRangeTo() instanceof BigInteger))) {
            Message.addError("invalidInteger");
            valid = false;
          }

          /*
           * In Mojarra the NumberConverter creates a BigDecimal, even if the user entered "100".
           * Convert it to a long
           */
          if (valueDomain.getRangeFrom() instanceof BigDecimal
              && valueDomain.getRangeTo() instanceof BigDecimal) {
            valueDomain.setRangeFrom(((BigDecimal) valueDomain.getRangeFrom()).toBigInteger());
            valueDomain.setRangeTo(((BigDecimal) valueDomain.getRangeTo()).toBigInteger());
          }

          if (valueDomain.getRangeFrom().longValue() >= valueDomain.getRangeTo().longValue()) {
            Message.addError("invalidRange");
            valid = false;
          }
        }
        break;

      case FLOAT:
        /*
         * Check if the range makes sense
         */
        if (valueDomain.getWithinRange()
            && valueDomain.getUseRangeFrom()
            && valueDomain.getUseRangeTo()) {
          if (valueDomain.getRangeFrom().doubleValue() >= valueDomain.getRangeTo().doubleValue()) {
            Message.addError("invalidRange");
            valid = false;
          }
        }
        break;

      case STRING:
        /*
         * Try to parse the regular expression. If it fails it is not a valid regex.
         */
        if (valueDomain.getUseRegex()) {
          try {
            Pattern.compile(valueDomain.getFormat());
          } catch (Exception e) {
            Message.addError("invalidRegex");
            valid = false;
          }
        }
        break;

      case CATALOG:
        if (StringUtil.isEmpty(valueDomain.getCatalog())) {
          Message.addError("catalogNotSelected");
          valid = false;
        }
        break;
      default:
        return true;
    }

    return valid;
  }

  /** Validates the list of definitions. */
  public static boolean validateDefinitions(List<AdapterDefinition> definitions) {
    boolean valid = true;

    if (definitions.size() == 0) {
      Message.addError("noDefinitions");
      valid = false;
    }

    for (AdapterDefinition d : definitions) {
      if (StringUtil.isEmpty(d.getDesignation())) {
        Message.addError("emptyDesignation");
        valid = false;
      }
    }
    return valid;
  }

  /**
   * Validates the given list of slots. Empty slots are removed with this operation.
   *
   * @param slots list of slots that are verified in this method
   * @return true if all slots are valid, false otherwise
   */
  public static boolean validateSlots(List<SlotDto> slots) {
    boolean valid = true;

    Iterator<SlotDto> iterator = slots.iterator();

    while (iterator.hasNext()) {
      SlotDto slot = iterator.next();

      if (StringUtil.isEmpty(slot.getName()) && StringUtil.isEmpty(slot.getValue())) {
        iterator.remove();
        continue;
      }

      if (StringUtil.isEmpty(slot.getName())) {
        Message.addError("emptyName");
        valid = false;
      }

      if (StringUtil.isEmpty(slot.getValue())) {
        Message.addError("emptyValue");
        valid = false;
      }
    }

    return valid;
  }

  /**
   * Validates the given list of conceptAssociations. Empty conceptAssociations are removed with
   * this operation.
   *
   * @param conceptAssociations list of conceptAssociations that are verified in this method
   * @return true if all conceptAssociations are valid, false otherwise
   */
  // TODO: has to be improved after deciding which attributes have to be mandatory
  public static boolean validateConceptAssociation(List<ConceptAssociation> conceptAssociations) {
    boolean valid = true;

    Iterator<ConceptAssociation> iterator = conceptAssociations.iterator();

    while (iterator.hasNext()) {
      ConceptAssociation conceptAssociation = iterator.next();

      if (StringUtil.isEmpty(conceptAssociation.getSystem())
          && StringUtil.isEmpty(conceptAssociation.getVersion())
          && StringUtil.isEmpty(conceptAssociation.getTerm())
          && StringUtil.isEmpty(conceptAssociation.getText())) {
        iterator.remove();
        continue;
      }

      if (StringUtil.isEmpty(conceptAssociation.getSystem())) {
        Message.addError("emptySystem");
        valid = false;
      }

      if (StringUtil.isEmpty(conceptAssociation.getVersion())) {
        Message.addError("emptyVersion");
        valid = false;
      }

      if (StringUtil.isEmpty(conceptAssociation.getTerm())) {
        Message.addError("emptyTerm");
        valid = false;
      }

      if (StringUtil.isEmpty(conceptAssociation.getText())) {
        Message.addError("emptyText");
        valid = false;
      }
    }

    return valid;
  }

  /**
   * Checks if the definitions are valid for the given namespace constraints.
   *
   * @param constraints the namespace constraints
   * @param definitions the list of definitions
   * @return True if the definitions are valid. False otherwise
   */
  public static boolean validateConstraints(
      List<String> constraints, List<AdapterDefinition> definitions) {

    boolean valid = true;

    for (String language : constraints) {
      boolean found = false;
      for (AdapterDefinition definition : definitions) {
        if (definition.getLanguage().toString().equals(language)) {
          found = true;
        }
      }

      if (!found) {
        Message.addError("languageMissing", JsfUtil.getString(language));
        valid = false;
      }
    }

    return valid;
  }

  /**
   * Checks if the slots are valid for the given namespace constraints.
   *
   * @param constraints the namespace constraints
   * @param slots list of slots
   * @return True if the slots are valid. False otherwise.
   */
  public static boolean validateConstraintsSlots(List<String> constraints, List<SlotDto> slots) {

    boolean valid = true;

    for (String slotValue : constraints) {
      boolean found = false;
      for (SlotDto slot : slots) {
        if (slot.getName().equals(slotValue)) {
          found = true;
          break;
        }
      }

      if (!found) {
        Message.addError("slotMissing", slotValue);
        valid = false;
      }
    }

    return valid;
  }

  /**
   * Checks if the permissible value is valid for the given namespace constraints.
   *
   * @param constraints namespace constraints
   * @param value the permissible value
   * @return True if the permissible value is valid. False otherwise.
   */
  public static boolean validateValueConstraints(
      List<String> constraints, AdapterPermissibleValue value) {

    boolean valid = true;

    for (String language : constraints) {
      boolean found = false;

      for (AdapterDefinition definition : value.getDefinitions()) {
        if (definition.getLanguage().toString().equals(language)) {
          found = true;
        }
      }

      if (!found) {
        Message.addError(
            "valueLanguageMissing", value.getValue(), JsfUtil.getString(language));
        valid = false;
      }
    }

    return valid;
  }
}
