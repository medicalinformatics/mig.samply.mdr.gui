/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui;

import de.samply.auth.client.jwt.KeyLoader;
import de.samply.common.config.OAuth2Client;
import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.db.migration.MigrationUtil;
import java.io.FileNotFoundException;
import java.security.PublicKey;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.xml.sax.SAXException;

/**
 * The static configuration class that stores all configuration values of this application.
 *
 * @author paul
 */
public class MdrConfig {

  /** The default project name. This is also the *default* prefix for Samply Common Config. */
  public static final String DEFAULT_PROJECT_NAME = "samply";
  /** The configuration file for the postgresql database. */
  public static final String FILE_POSTGRES = "mdr.postgres.xml";
  /** The configuration file for the OAuth2 configuration. */
  public static final String FILE_OAUTH = "mdr.oauth2.xml";
  /** The configuration file for the log4j2 configuration. */
  public static final String FILE_LOG4J2 = "log4j2.xml";

  private static final Logger logger = LogManager.getLogger(MdrConfig.class);
  /**
   * The *current* project name. This is also the prefix for Samply Common Config. This value should
   * not be changed unless the application has not been initialized yet.
   */
  private static String projectName = "samply";
  /** The OAuth2 client configuration. */
  private static OAuth2Client oauth2;

  /** The identity providers public key. */
  private static PublicKey publicKey;

  /** The database credentials. */
  private static Postgresql psql;

  /** The *current* database version. */
  private static int dbVersion = 0;

  /**
   * If true the server is in maintenance mode: REST interface returns 503, web interface redirects
   * to /maintenance.xhtml or /admin/maintenance.xhtml
   */
  private static Boolean maintenanceMode = false;

  private static JAXBContext jaxbContext;

  /**
   * TODO: add javadoc.
   */
  public static void initialize(String fallback)
      throws FileNotFoundException, JAXBException, SAXException, ParserConfigurationException {

    try {
      Configurator.initialize(
          MdrConfig.class.getCanonicalName(),
          FileFinderUtil.findFile(FILE_LOG4J2, projectName, fallback).getAbsolutePath());
    } catch (FileNotFoundException e) {
      logger.error("Could not find log4j config file");
    }

    psql =
        JAXBUtil.findUnmarshall(
            FILE_POSTGRES, getJaxbContext(), Postgresql.class, projectName, fallback);

    ResourceManager.initialize(
        psql.getHost(),psql.getDatabase(), psql.getUsername(), psql.getPassword());

    oauth2 =
        JAXBUtil.findUnmarshall(
            FILE_OAUTH, getJaxbContext(), OAuth2Client.class, projectName, fallback);
    publicKey = KeyLoader.loadKey(oauth2.getHostPublicKey());

    logger.debug("Establishing first connection to check the db version");

    MigrationUtil.migrateDatabase();
  }

  /**
   * Returns the JAXBContext for Postgresql and OAuth2Client classes. Creates a new one if
   * necessary.
   */
  private static synchronized JAXBContext getJaxbContext() throws JAXBException {
    if (jaxbContext == null) {
      jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    }
    return jaxbContext;
  }

  public static PublicKey getPublicKey() {
    return publicKey;
  }

  public static Postgresql getPsql() {
    return psql;
  }

  public static int getDbVersion() {
    return dbVersion;
  }

  public static OAuth2Client getOauth2() {
    return oauth2;
  }

  public static Boolean getMaintenanceMode() {
    return maintenanceMode;
  }

  public static String getProjectName() {
    return projectName;
  }

  public static void setProjectName(String projectName) {
    MdrConfig.projectName = projectName;
  }
}
