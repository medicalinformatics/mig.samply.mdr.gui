/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui;

import de.samply.mdr.dal.dao.ScopedIdentifierRelationDao.RelationDirection;
import de.samply.mdr.dal.jooq.enums.RelationType;

public class MdrElementRelation {

  private MdrElement mdrElement;
  private RelationType relationType;
  private RelationDirection direction;

  public MdrElementRelation() {
  }

  /**
   * Create a new MdrElementRelation.
   */
  public MdrElementRelation(MdrElement mdrElement,
      RelationType relationType,
      RelationDirection direction) {
    this.mdrElement = mdrElement;
    this.relationType = relationType;
    this.direction = direction;
  }

  public MdrElement getMdrElement() {
    return mdrElement;
  }

  public void setMdrElement(MdrElement mdrElement) {
    this.mdrElement = mdrElement;
  }

  public RelationType getRelationType() {
    return relationType;
  }

  public void setRelationType(RelationType relationType) {
    this.relationType = relationType;
  }

  public RelationDirection getDirection() {
    return direction;
  }

  public void setDirection(RelationDirection direction) {
    this.direction = direction;
  }
}
