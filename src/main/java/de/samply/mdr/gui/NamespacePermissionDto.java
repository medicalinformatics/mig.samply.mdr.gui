/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui;

import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.string.util.StringUtil;
import java.io.Serializable;

/**
 * Represents a permission (read, read & write) on a specific namespace for a specific user.
 *
 * @author paul
 */
public class NamespacePermissionDto implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The users email address. */
  private String email;

  /** The users real name. */
  private String realName;

  /** The label for the external identity provider if there is one. */
  private String externalLabel;

  /** The permission mode. */
  private Permission permission;

  /** The users ID. */
  private int userId;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Permission getPermission() {
    return permission;
  }

  public void setPermission(Permission permission) {
    this.permission = permission;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  /** Returns the label for this identity provider. */
  public String getLabel() {
    if (StringUtil.isEmpty(realName)) {
      return email;
    } else {
      return realName
          + " <"
          + email
          + ">"
          + (StringUtil.isEmpty(externalLabel) ? "" : " (" + externalLabel + ")");
    }
  }

  public String getExternalLabel() {
    return externalLabel;
  }

  public void setExternalLabel(String externalLabel) {
    this.externalLabel = externalLabel;
  }
}
