/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui;

import de.samply.string.util.StringUtil;
import java.io.FileNotFoundException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

/** Initializes the application. * loads the driver * loads all config files */
@WebListener
public class ServletListener implements ServletContextListener {

  private static final Logger logger = LogManager.getLogger(ServletListener.class);

  private static Timer timer;

  @Override
  public void contextInitialized(ServletContextEvent event) {
    try {
      String projectName = event.getServletContext().getInitParameter("projectName");

      if (StringUtil.isEmpty(projectName)) {
        projectName = MdrConfig.getProjectName();
      }

      MdrConfig.setProjectName(projectName);

      logger.info("Registering PostgreSQL driver");
      Class.forName("org.postgresql.Driver").newInstance();

      String fallback = event.getServletContext().getRealPath("/WEB-INF");

      MdrConfig.initialize(fallback);

      logger.info("Starting cleanup task");
      TimerTask task = new CleanupTask();

      timer = new Timer();
      timer.scheduleAtFixedRate(task, 5000, 1000 * 60 * 10);

      logger.info("Cleanup task started. Listener finished.");

      logger.info("Context initialized");
    } catch (FileNotFoundException
        | JAXBException
        | SAXException
        | ParserConfigurationException
        | InstantiationException
        | IllegalAccessException
        | ClassNotFoundException e) {
      logger.error(e);
    }
  }

  @Override
  public void contextDestroyed(ServletContextEvent event) {
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    while (drivers.hasMoreElements()) {
      Driver driver = drivers.nextElement();
      try {
        DriverManager.deregisterDriver(driver);
        logger.info("Unregistering driver " + driver.toString());
      } catch (SQLException e) {
        logger.error(e);
      }
    }

    if (timer != null) {
      timer.cancel();
    }
  }
}
