/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui;

import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.lib.AdapterValueDomain;
import de.samply.mdr.util.Mapper;
import java.util.HashMap;
import java.util.List;

/**
 * The internal value domain. This class combines many different value domains, e.g. a value domain
 * with an integer range or a value domain with a list of permissible values.
 *
 * @author paul
 */
public class ValueDomainDto extends AdapterValueDomain {

  private static final long serialVersionUID = -5051133454904262119L;

  /** The URN of the selected catalog. */
  private String catalog;

  /** The list of all subcodes. */
  private List<MdrElement> subCodes;

  /** The catalog root TreeNode. */
  private TreeNode catalogRoot = null;

  /** The hashmap index for tree nodes, the scoped identifier ID is the key. */
  private HashMap<Integer, TreeNode> index = null;

  /** The hashmap index for tree nodes, the URN is the key. */
  private HashMap<String, TreeNode> urnIndex = null;

  /** Returns the CSS-Class for the given URN. Used in catalogs. */
  public String getState(String urn) {
    switch (urnIndex.get(urn).getState()) {
      case ALL:
        return "fa-check-square-o";

      case SOME:
        return "fa-minus-square-o";

      default:
        return "fa-square-o";
    }
  }

  /** Updates the entire tree states. */
  public void updateTreeState() {
    for (TreeNode node : catalogRoot.getChildren()) {
      updateState(node);
    }
  }

  /** Updates the given TreeNode status. Recursive function. */
  private ChildrenSelectionState updateState(TreeNode node) {
    node.setState((node.isSelected() ? ChildrenSelectionState.ALL : ChildrenSelectionState.SOME));
    int checked = 0;

    for (TreeNode child : node.getChildren()) {
      ChildrenSelectionState state = updateState(child);
      if (state != ChildrenSelectionState.ALL && node.getState() == ChildrenSelectionState.ALL) {
        node.setState(ChildrenSelectionState.SOME);
      }

      if (state != ChildrenSelectionState.NONE) {
        ++checked;
      }
    }

    if (node.getState() == ChildrenSelectionState.SOME && checked == 0) {
      node.setState(ChildrenSelectionState.NONE);
    }

    return node.getState();
  }

  /**
   * TODO: add javadoc.
   */
  public String getNumericRange() {
    if (withinRange) {
      ValueDomain convert = Mapper.convert(this);
      return convert.getFormat();
    } else {
      return "";
    }
  }

  public String getCatalog() {
    return catalog;
  }

  public void setCatalog(String catalog) {
    this.catalog = catalog;
  }

  public TreeNode getCatalogRoot() {
    return catalogRoot;
  }

  public void setCatalogRoot(TreeNode root) {
    this.catalogRoot = root;
  }

  public List<MdrElement> getSubCodes() {
    return subCodes;
  }

  public void setSubCodes(List<MdrElement> subCodes) {
    this.subCodes = subCodes;
  }

  public HashMap<Integer, TreeNode> getIndex() {
    return index;
  }

  public void setIndex(HashMap<Integer, TreeNode> index) {
    this.index = index;
  }

  public HashMap<String, TreeNode> getUrnIndex() {
    return urnIndex;
  }

  public void setUrnIndex(HashMap<String, TreeNode> urnIndex) {
    this.urnIndex = urnIndex;
  }
}
