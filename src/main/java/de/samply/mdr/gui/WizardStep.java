/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * A class that represents a step in a wizard.
 *
 * @author paul
 */
public class WizardStep implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The title of this step in the wizard process. */
  private String title;

  /** The number of this step. */
  private int number;

  /** The link. */
  private List<String> href;

  /** If true, this step has been completed. */
  private Boolean complete;

  private Boolean active;

  /** If true, this step is the last step in the wizard. */
  private Boolean last;

  public WizardStep() {}

  /** TODO: add javadoc. */
  public WizardStep(String title, int number, String... href) {
    this.title = title;
    this.number = number;
    this.href = Arrays.asList(href);
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public Boolean getComplete() {
    return complete;
  }

  public void setComplete(Boolean complete) {
    this.complete = complete;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Boolean getLast() {
    return last;
  }

  public void setLast(Boolean last) {
    this.last = last;
  }

  public List<String> getHref() {
    return href;
  }

  public void setHref(List<String> href) {
    this.href = href;
  }
}
