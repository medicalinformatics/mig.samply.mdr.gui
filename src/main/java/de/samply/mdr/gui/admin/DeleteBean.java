/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie, Deutsches
 * Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.mdr.gui.admin;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.beans.UserBean;
import de.samply.mdr.util.Message;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/** Handles the deletion of elements in the admin interface. */
@ManagedBean
@ViewScoped
public class DeleteBean implements Serializable {

  private static final long serialVersionUID = 9153665655652156261L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** The URN that will be deleted. */
  private String urn;

  /** Deletes the URN from the database. */
  public String delete() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ScopedIdentifier identifier = ScopedIdentifier.fromUrn(urn);

      if (identifier.getElementType() == Elementtype.DATAELEMENT
          || identifier.getElementType() == Elementtype.DATAELEMENTGROUP
          || identifier.getElementType() == Elementtype.RECORD) {

        IdentifiedDao.delete(ctx, userBean.getUserId(), urn);
        Message.addInfo("admin.elementDeleted");

        urn = null;
      }
    }

    return null;
  }

  public String getUrn() {
    return urn;
  }

  public void setUrn(String urn) {
    this.urn = urn;
  }
}
