/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.admin;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.UserDao;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * This bean manages the list of all namespaces in the admin interface.
 *
 * @author paul
 */
@ManagedBean
@ViewScoped
public class NamespaceListBean implements Serializable {

  private static final long serialVersionUID = -961055283846486065L;

  /** The list of namespaces. */
  private List<MdrNamespace> namespaces;

  /** TODO: add javadoc. */
  @PostConstruct
  public void initialize() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      namespaces =
          Mapper.convertNamespaces(NamespaceDao.getNamespaces(ctx), Language.EN);
    }
  }

  /** Returns the creator of the given namespace. */
  public String getCreator(Namespace namespace) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      User user = UserDao.getUser(ctx, namespace.getCreatedBy());
      return user.getEmail()
          + " ("
          + (user.getExternalLabel() == null ? "Local" : user.getExternalLabel())
          + ")";
    }
  }

  /** Toggles the namespaces visibility. */
  public String toggle(Namespace namespace) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      namespace.setHidden(!namespace.getHidden());

      ctx.connection(c -> c.setAutoCommit(false));
      NamespaceDao.updateNamespace(ctx, namespace);
      ctx.connection(Connection::commit);
    }
    return null;
  }

  public List<MdrNamespace> getNamespaces() {
    return namespaces;
  }

  public void setNamespaces(List<MdrNamespace> namespaces) {
    this.namespaces = namespaces;
  }
}
