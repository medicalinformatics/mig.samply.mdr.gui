/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.gui.WizardStep;
import java.io.Serializable;
import java.util.List;

/**
 * An abstract Wizard Helper class.
 *
 * @author paul
 */
public abstract class AbstractWizard implements Serializable {

  private static final long serialVersionUID = 1L;

  /** Returns all available steps in this wizard, used for the wizard status bar. */
  public abstract List<WizardStep> getSteps();

  /**
   * Returns all available steps in this wizard, used for the wizard status bar. The argument is
   * used to determine the already completed steps.
   */
  public List<WizardStep> getSteps(String view) {
    List<WizardStep> target = getSteps();

    int step = 1;
    boolean found = false;

    for (WizardStep s : target) {
      if (s.getHref().contains(view)) {
        found = true;
      }

      if (!found) {
        ++step;
      }
    }

    int index = 1;
    for (WizardStep s : target) {
      if (index < step) {
        s.setComplete(true);
        s.setActive(false);
      } else if (index == step) {
        s.setComplete(false);
        s.setActive(true);
      } else {
        s.setComplete(false);
        s.setActive(false);
      }

      s.setLast(false);

      index++;
    }

    target.get(target.size() - 1).setLast(true);

    return target;
  }

  /**
   * Returns true if the given viewId is the first step in this wizard.
   *
   * @param viewId the current viewId (view.viewId)
   * @return true if the given view is the first step in this wizard.
   */
  public Boolean isFirstStep(String viewId) {
    return getSteps().get(0).getHref().contains(viewId);
  }

  /**
   * Returns true if the given viewId is the last step in this wizard.
   *
   * @param viewId the current viewId (view.viewId)
   * @return true if the given view is the last step in this wizard.
   */
  public Boolean isLastStep(String viewId) {
    return getSteps().get(getSteps().size() - 1).getHref().contains(viewId);
  }

  /**
   * Returns to the previous step. If the current step can not be found in the list of available
   * steps, it returns null
   */
  public String previousStep(String viewId) {
    int index = 0;

    List<WizardStep> steps = getSteps();

    for (WizardStep s : steps) {
      if (s.getHref().contains(viewId) && index != 0) {
        return steps.get(index - 1).getHref().get(0) + "?faces-redirect=true";
      }

      ++index;
    }

    return null;
  }

  /** If this methods returns true, only the draft button is shown. */
  public Boolean getDraftButtonOnly(String viewId) {
    return false;
  }

  /**
   * If this methods returns true, the release and draft buttons are shown. Otherwise they are not
   * shown.
   */
  public Boolean getReleaseDraftButtons() {
    return true;
  }

  public abstract String getDesignation();

  /** Override this in extending classes if the button "next" in the wizard should be disabled. */
  public Boolean disableNextButton(String viewId) {
    return false;
  }
}
