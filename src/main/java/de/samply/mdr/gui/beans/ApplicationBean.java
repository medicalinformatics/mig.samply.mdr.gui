/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.gui.MdrConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.jar.Manifest;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 * The application wide bean that is used to get the maven version of this application.
 *
 * @author paul
 */
@ManagedBean
@ApplicationScoped
public class ApplicationBean implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String EXTERNAL_IMAGES_FOLDER = "/usr/local/tomcat/images";

  /** The version of this application. It is read out only once. */
  private String version = null;

  private Manifest manifest = null;

  /** TODO: add javadoc. */
  @PostConstruct
  public void init() {
    try {
      InputStream is =
          FacesContext.getCurrentInstance()
              .getExternalContext()
              .getResourceAsStream("/META-INF/MANIFEST.MF");
      manifest = new Manifest();
      manifest.read(is);
    } catch (IOException e) {
      e.printStackTrace();
      // TODO: add logging
    }
  }

  /** TODO: add javadoc. */
  public String getBuildDate() {
    try {
      return manifest.getMainAttributes().getValue("builddate");
    } catch (NullPointerException e) {
      return "unknown";
    }
  }

  /** TODO: add javadoc. */
  public String getBuildCommitHash() {
    try {
      return manifest.getMainAttributes().getValue("SCM-Version");
    } catch (NullPointerException e) {
      return "unknown";
    }
  }

  /** TODO: add javadoc. */
  public String getBuildCommitBranch() {
    try {
      return manifest.getMainAttributes().getValue("SCM-Branch");
    } catch (NullPointerException e) {
      return "unknown";
    }
  }

  /** TODO: add javadoc. */
  public String getVersionFromManifest() {
    try {
      return manifest.getMainAttributes().getValue("Implementation-Version");
    } catch (NullPointerException e) {
      return "unknown";
    }
  }

  /** Returns the maven version of this application. */
  public String getVersion() {
    if (version == null) {
      version = getClass().getPackage().getImplementationVersion();
      if (version == null) {
        version = getVersionFromManifest();
      }
    }
    return version;
  }

  /**
   * Get the inputstream of an image file located in the specified external image path.
   */
  public InputStream getImage(String filename) {
    try {
      return new FileInputStream(new File(EXTERNAL_IMAGES_FOLDER, filename));
    } catch (FileNotFoundException e) {
      return null;
    }
  }

  /** Returns the current database version. */
  public String getDbVersion() {
    return "" + MdrConfig.getDbVersion();
  }

  /** Returns the JSF version of the currently used JSF library. */
  public String getJsfVersion() {
    return FacesContext.class.getPackage().getImplementationVersion();
  }

  /** Returns the JSF vendor of the currently used JSF library. */
  public String getJsfTitle() {
    return FacesContext.class.getPackage().getImplementationTitle();
  }

  public Manifest getManifest() {
    return manifest;
  }
}
