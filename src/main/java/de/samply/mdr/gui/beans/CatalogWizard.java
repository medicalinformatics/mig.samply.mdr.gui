/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.SlotDto;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.Code.SubCode;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Element;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.Slot;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.jooq.DSLContext;
import org.xml.sax.SAXException;

/** This wizard is used to create **and change existing catalogs**. */
@ManagedBean
@SessionScoped
public class CatalogWizard extends ElementWizard {

  private static final long serialVersionUID = 4470054228581383940L;

  /** The steps of this wizard. */
  private List<WizardStep> steps;

  /** The uploaded file, must be a valid catalog XML file. */
  private transient Part file;

  /** The deserialized uploaded catalog. */
  private Catalog catalog;

  /**
   * The catalog from the MDR, that will be changed by this wizard. Null if this wizard creates a
   * new catalog.
   */
  private MdrElement catalogElement = null;

  /** TODO: add javadoc. */
  public CatalogWizard() {
    steps = new ArrayList<>();
    steps.add(new WizardStep(JsfUtil.getString("upload"), 1, "/user/catalogupload.xhtml"));
    steps.add(new WizardStep(JsfUtil.getString("verification"), 2, "/user/catalogverify.xhtml"));
  }

  /** TODO: add javadoc. */
  public String initialize(String urn) {
    // reset();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      /*
       * Because getChange is called before, just get the catalog from the MDR before we call
       * template
       */
      catalogElement = Mapper.convert(
          IdentifiedDao.getElement(
              ctx, getUserBean().getUserId(), urn), getUserBean().getSelectedLanguage());
      catalogElement = template(ctx, urn);
    }
    return steps.get(0).getHref().get(0);
  }

  /** Saves the catalog in the database. */
  public String finish() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Namespace namespace = ElementDao.getNamespace(ctx, getNamespace());

      de.samply.mdr.dal.dto.Catalog catalog = new de.samply.mdr.dal.dto.Catalog();
      ElementDao.saveElement(ctx, getUserBean().getUserId(), catalog);

      ScopedIdentifier catalogIdentifier = this.finish(ctx, catalog.getId(), namespace.getId());

      HashMap<String, ScopedIdentifier> index = new HashMap<>();
      HashMap<String, Code> rootIndex = new HashMap<>();

      if (catalogElement != null) {
        /*
         * If we change an existing catalog, get all codes from the database and store them in a
         * hashmap
         */
        HashMap<String, IdentifiedElement> existingIdentifiers = new HashMap<>();

        List<IdentifiedElement> allSubMembers =
            IdentifiedDao.getAllSubMembers(ctx, getUserBean().getUserId(), catalogElement.getUrn());
        for (IdentifiedElement e : allSubMembers) {
          de.samply.mdr.dal.dto.Code code = (de.samply.mdr.dal.dto.Code) e.getElement();
          existingIdentifiers.put(code.getCode(), e);
        }

        /*
         * Then create a scoped identifier for every code from the catalog for the code from the
         * hashmap we just created
         */
        for (Code code : this.catalog.getCode()) {
          IdentifiedElement codeElement = existingIdentifiers.get(code.getCode());

          ScopedIdentifier codeIdentifier =
              this.finish(
                  ctx,
                  existingIdentifiers.get(code.getCode()).getElement().getId(),
                  namespace.getId(),
                  convertDefinitions(code),
                  convertSlots(code),
                  Elementtype.CODE,
                  getConceptAssociations());
          index.put(code.getCode(), codeIdentifier);
          rootIndex.put(code.getCode(), code);
        }
      } else {
        Integer maxIdentifier =
            Integer.parseInt(newIdentifier(ctx, namespace.getName(), Elementtype.CODE));

        /*
         * Since this block is only reached if one of these conditions is met:
         * - the catalog does not exist before
         * - the catalog is still in the first draft status
         *
         * We need to make sure, that from this point on only new identifiers will be created.
         */
        setUseAsTemplate(true);

        /*
         * First create a scoped identifier for every code of the catalog and store this scoped
         * identifier in both hashmaps.
         */
        for (Code code : this.catalog.getCode()) {
          de.samply.mdr.dal.dto.Code codeDto = new de.samply.mdr.dal.dto.Code();
          codeDto.setCatalogId(catalog.getId());
          codeDto.setCode(code.getCode());
          codeDto.setValid(code.isIsValid());
          ElementDao.saveElement(ctx, getUserBean().getUserId(), codeDto);

          ScopedIdentifier codeIdentifier =
              this.finish(
                  ctx,
                  codeDto.getId(),
                  namespace.getId(),
                  convertDefinitions(code),
                  convertSlots(code),
                  Elementtype.CODE,
                  getConceptAssociations());

          index.put(code.getCode(), codeIdentifier);
          rootIndex.put(code.getCode(), code);
        }
      }

      /*
       * Use the hashmap and the subCodes from the catalog to create the hierarchy in the database
       * via addSubIdentifier.
       */
      for (Code code : this.catalog.getCode()) {
        int order = 1;
        for (SubCode subCode : code.getSubCode()) {
          if (index.containsKey(subCode.getCode())) {
            ScopedIdentifier superIdentifier = index.get(code.getCode());
            ScopedIdentifier subIdentifier = index.get(subCode.getCode());

            ScopedIdentifierHierarchyDao
                .addSubIdentifier(ctx, superIdentifier.getId(), subIdentifier.getId(), order++);

            /*
             * Remove the code from the rootIndex
             */
            rootIndex.remove(subCode.getCode());
          }
        }
      }

      /*
       * The remaining codes in the root Index hashmap are
       * the "root" codes in the catalog. Add them as children from the catalog.
       */
      for (Entry<String, Code> entry : rootIndex.entrySet()) {
        ScopedIdentifier subIdentifier = index.get(entry.getKey());
        ScopedIdentifierHierarchyDao.addSubIdentifier(
            ctx, catalogIdentifier.getId(), subIdentifier.getId(), entry.getValue().getOrder());
      }

      /*
       * Start the thread that refreshes the materialized views.
       */
      refreshViews();
      ctx.connection(Connection::commit);
      return "/detail?faces-redirect=true&urn=" + catalogIdentifier.toString();
    } catch (ValidationException | DuplicateIdentifierException e) {
      e.printStackTrace();
      return null;
    }
  }

  /** Converts the elements XML definitions into DefinitionDTOs. */
  private List<AdapterDefinition> convertDefinitions(Element element) {
    List<AdapterDefinition> target = new ArrayList<>();

    if (element.getDefinitions() == null) {
      return target;
    }

    for (Definition definition : element.getDefinitions().getDefinition()) {
      AdapterDefinition def = new AdapterDefinition();
      def.setDefinition(definition.getDefinition());
      def.setDesignation(definition.getDesignation());
      def.setLanguage(Language.valueOf(definition.getLang().toUpperCase()));
      target.add(def);
    }

    return target;
  }

  /** Converts the slots from XML slots into SlotDTOs. */
  private List<SlotDto> convertSlots(Element element) {
    List<SlotDto> target = new ArrayList<>();

    if (element.getSlots() == null) {
      return target;
    }

    for (Slot slot : element.getSlots().getSlot()) {
      SlotDto s = new SlotDto();
      s.setName(slot.getKey());
      s.setValue(slot.getValue());
      target.add(s);
    }

    return target;
  }

  /**
   * This method is called right after the upload. It checks if the uploaded catalog is valid (it
   * uses the XSD file to do that).
   */
  public String verify() {
    try {
      JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

      InputStream commonXsd = Catalog.class.getClassLoader().getResourceAsStream("common.xsd");
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = sf.newSchema(new StreamSource(commonXsd));

      Validator validator = schema.newValidator();
      validator.validate(new StreamSource(file.getInputStream()));

      JAXBElement<?> element =
          (JAXBElement<?>) context.createUnmarshaller().unmarshal(file.getInputStream());
      catalog = (Catalog) element.getValue();

      setDefinitions(convertDefinitions(catalog));
      setSlots(convertSlots(catalog));

      /**
       * If this wizard changes a catalog, check if:
       *
       * <p>- the valid codes are the same. So there are only hierarchical changes
       */
      if (catalogElement != null) {
        try (DSLContext ctx = ResourceManager.getDslContext()) {
          List<IdentifiedElement> allSubMembers = IdentifiedDao.getAllSubMembers(
              ctx, getUserBean().getUserId(), catalogElement.getUrn());

          HashSet<String> existingCodes = new HashSet<>();
          HashSet<String> newCodes = new HashSet<>();

          for (IdentifiedElement member : allSubMembers) {
            de.samply.mdr.dal.dto.Code code = (de.samply.mdr.dal.dto.Code) member.getElement();
            if (code.isValid()) {
              existingCodes.add(code.getCode());
            }
          }

          for (Code code : catalog.getCode()) {
            if (code.isIsValid()) {
              newCodes.add(code.getCode());

              if (!existingCodes.contains(code.getCode())) {
                Message.addError("invalidCode", code.getCode());
                return null;
              }
            }
          }

          if (existingCodes.size() != newCodes.size()) {
            Message.addError("invalidCodeCount");
            return null;
          }

        }
      }

      return "/user/catalogverify.xhtml?faces-redirect=true";
    } catch (IOException | JAXBException e) {
      e.printStackTrace();
      Message.addError("invalidCatalog");
      return null;
    } catch (SAXException e) {
      e.printStackTrace();
      Message.addError("invalidCatalogError", e.getMessage());
      return null;
    }
  }

  @Override
  public String reset() {
    super.reset();

    file = null;
    catalog = null;
    catalogElement = null;

    return "/index.xhtml?faces-direct=true";
  }

  @Override
  public String validateSlots() {
    throw new UnsupportedOperationException();
  }

  @Override
  public String validateDefinitions() {
    throw new UnsupportedOperationException();
  }

  @Override
  protected Elementtype getElementType() {
    return Elementtype.CATALOG;
  }

  @Override
  public String template(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      template(ctx, urn);
    }
    return "/user/catalogupload?faces-redirect=true";
  }

  @Override
  public List<WizardStep> getSteps() {
    return steps;
  }

  /* (non-Javadoc)
   * @see de.samply.mdr.gui.beans.ElementWizard#getChange()
   */
  @Override
  public Boolean getChange() {
    return catalogElement != null;
  }

  public Part getFile() {
    return file;
  }

  public void setFile(Part file) {
    this.file = file;
  }

  public Catalog getCatalog() {
    return catalog;
  }

  public void setCatalog(Catalog catalog) {
    this.catalog = catalog;
  }

  public MdrElement getCatalogElement() {
    return catalogElement;
  }

  public void setCatalogElement(MdrElement element) {
    this.catalogElement = element;
  }
}
