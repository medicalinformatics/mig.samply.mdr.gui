/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.rest.UserDto;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.UserDao;
import de.samply.mdr.dal.dao.UserNamespaceGrantsDao;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.JsonFields;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.NamespacePermission.Permission;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.NamespacePermissionDto;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.json.JSONObject;

/**
 * This wizard allows *basic* changes only, for more information see the following. - labels (also
 * the labels for permitted values) - permissions - hidden flag
 *
 * @author paul
 */
@SessionScoped
@ManagedBean
public class ChangeNamespaceWizard extends DataElementGroupWizard {

  private static final long serialVersionUID = 1L;

  private List<WizardStep> steps;

  /** The list of the current namespace permissions. */
  private List<NamespacePermissionDto> permissions;

  /** If true, the namespace will be hidden. */
  private Boolean hidden;

  /** The namespace which is being edited. */
  private Namespace ns;

  /** The selected user identity. */
  private String identity;

  /** The selected permission for the selected user. */
  private Permission permission;

  /** A list of suggestions. */
  private String suggestions;

  /** The search text from the select2 drop down. */
  private String searchText;

  /**
   * The list of users that have been loaded. Without this field the database has to be queried
   * every time.
   */
  private List<UserDto> users;

  /** The required languages. */
  private List<Language> languages;

  /** The required slots. */
  private List<String> constraintSlots;

  /** The selected language that will be added to the list of required languages. */
  private Language language;

  /** The selected slot that will be added to the list of required slots. */
  private String slot;

  @Override
  public String validateSlots() {
    return null;
  }

  @Override
  public String validateDefinitions() {
    setStarted(true);

    if (BeanValidators.validateDefinitions(getDefinitions())) {
      return "changenspermissions?faces-redirect=true";
    } else {
      return null;
    }
  }

  /** In case we need to validate the permissions, do it here. */
  public String validatePermissions() {
    return "changensconstraints?faces-redirect=true";
  }

  /** In case we need to verify the constraints, do it here. */
  public String validateConstraints() {
    return "changensverify?faces-redirect=true";
  }

  @Override
  public Elementtype getElementType() {
    return Elementtype.DATAELEMENT;
  }

  @Override
  public String finish() {
    String definitions = validateDefinitions();

    if (definitions == null) {
      return null;
    }

    if (!getUserBean().isWritableNamespaceByName(ns.getName())) {
      Message.addError("serverError");
      return null;
    }

    JSONObject constraints = new JSONObject();

    /*
    * The constraints are saved in the data field.
    */
    constraints.put(JsonFields.LANGUAGES, languages);
    constraints.put(JsonFields.SLOTS, constraintSlots);

    ns.getData().put(JsonFields.CONSTRAINTS, constraints);

    List<Definition> target = new ArrayList<>();
    for (AdapterDefinition def : getDefinitions()) {
      target.add(Mapper.convert(def));
    }
    ns.setHidden(hidden);

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      NamespaceDao.updateNamespace(ctx, ns, target);
      UserNamespaceGrantsDao.updatePermissions(ctx, Mapper.convertPermissions(permissions, ns), ns);
      ctx.connection(Connection::commit);
    }

    /*
      * Reload the namespaces.
      */
    getUserBean().reloadNamespaces();

    return "/view?faces-redirect=true&namespace=" + ns.getName();
  }

  /**
   * Initializes the wizard. Creates all steps and gets the current permissions from the database.
   *
   * @param namespace the current namespace name
   */
  public String initialize(String namespace) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      DescribedElement desc = NamespaceDao.getNamespace(ctx, getUserBean().getUserId(), namespace);

      ns = (Namespace) desc.getElement();

      steps = new ArrayList<>();
      steps.add(
          new WizardStep(JsfUtil.getString("definitions"), 1, "/user/changensdefinitions.xhtml"));
      steps.add(
          new WizardStep(JsfUtil.getString("permissions"), 2, "/user/changenspermissions.xhtml"));
      steps.add(
          new WizardStep(JsfUtil.getString("constraints"), 3, "/user/changensconstraints.xhtml"));
      steps.add(new WizardStep(JsfUtil.getString("verification"), 4, "/user/changensverify.xhtml"));

      setDefinitions(Mapper.convert(desc.getDefinitions()));

      permissions = Mapper.convert(ctx, UserNamespaceGrantsDao.getPermissions(ctx, ns));

      hidden = ns.getHidden();
      setIdentifier(ns.getName());

      languages = ns.getLanguages().stream().map(Language::valueOf).collect(Collectors.toList());
      constraintSlots = ns.getConstraintsSlots();
    } catch (DataAccessException e) {
      e.printStackTrace();
      reset();
      return null;
    }
    return "/user/changensdefinitions.xhtml?faces-redirect=true";
  }

  /** Adds the current permission. */
  public String add() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      UserDto userDto = null;
      for (UserDto u : users) {
        if (u.getId().equals(identity)) {
          userDto = u;
        }
      }

      /*
       * Something went wrong. Abort.
       */
      if (userDto == null) {
        return null;
      }

      User user = UserDao.getUserByIdentity(ctx, identity);

      /*
       * If the user has not been to the MDR yet, create him.
       */
      if (user == null) {
        user = UserDao.createDefaultUser(
            ctx, identity, userDto.getEmail(), userDto.getName(), userDto.getExternalLabel());
      }

      /*
       * Search for duplicates.
       */
      for (NamespacePermissionDto p : permissions) {
        if (p.getUserId() == user.getId()) {
          return null;
        }
      }

      NamespacePermissionDto dto = new NamespacePermissionDto();
      dto.setEmail(user.getEmail());
      dto.setRealName(user.getRealName());
      dto.setUserId(user.getId());
      dto.setExternalLabel(user.getExternalLabel());
      dto.setPermission(permission);
      permissions.add(dto);

      identity = "";
      permission = Permission.READ;
    }
    return null;
  }

  /** Searches the database for a specific user. Uses the username and email attributes. */
  public String searchEmail() {
    try {
      users = getUserBean().getAuthClient().searchUser(searchText).getUsers();
      suggestions =
          StringUtil.join(
              users,
              ";|;",
              new Builder<UserDto>() {
                @Override
                public String build(UserDto o) {
                  return o.getName()
                      + " <"
                      + o.getEmail()
                      + "> ["
                      + o.getId()
                      + "]"
                      + (StringUtil.isEmpty(o.getExternalLabel())
                          ? ""
                          : " (" + o.getExternalLabel() + ")");
                }
              });
    } catch (InvalidTokenException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public String reset() {
    languages = new ArrayList<>();
    constraintSlots = new ArrayList<>();

    if (ns == null) {
      return "/index?faces-redirect=true";
    } else {
      return "/view?faces-redirect=true&namespace=" + ns.getName();
    }
  }

  /** Adds the current language to the list of required languages. */
  public String addLanguage() {
    if (!languages.contains(language)) {
      languages.add(language);
    }
    return null;
  }

  /** Removes the given language from the list of required languages. */
  public String removeLanguage(Language language) {
    languages.remove(language);
    return null;
  }

  /** Adds the current slot to the list of required slots. */
  public String addSlotConstraint() {
    if (!constraintSlots.contains(slot)) {
      constraintSlots.add(slot);

      slot = "";
    }
    return null;
  }

  /** Not used. */
  @Override
  public String validateMembers() {
    throw new UnsupportedOperationException();
  }

  /** Removes the given slot from the list of required slots. */
  public String removeSlotConstraint(String s) {
    constraintSlots.remove(s);
    return null;
  }

  @Override
  public List<WizardStep> getSteps() {
    return steps;
  }

  @Override
  public Boolean getChange() {
    return true;
  }

  @Override
  public Boolean getReleaseDraftButtons() {
    return false;
  }

  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  public List<NamespacePermissionDto> getPermissions() {
    return permissions;
  }

  public void setPermissions(List<NamespacePermissionDto> permissions) {
    this.permissions = permissions;
  }

  public void remove(NamespacePermissionDto permission) {
    permissions.remove(permission);
  }

  public String getIdentity() {
    return identity;
  }

  public void setIdentity(String email) {
    this.identity = email;
  }

  public Permission getPermission() {
    return permission;
  }

  public void setPermission(Permission permission) {
    this.permission = permission;
  }

  public String getSuggestions() {
    return suggestions;
  }

  public void setSuggestions(String suggestions) {
    this.suggestions = suggestions;
  }

  public String getSearchText() {
    return searchText;
  }

  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }

  public List<UserDto> getUsers() {
    return users;
  }

  public void setUsers(List<UserDto> users) {
    this.users = users;
  }

  public List<Language> getLanguages() {
    return languages;
  }

  public void setLanguages(List<Language> languages) {
    this.languages = languages;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public String getSlot() {
    return slot;
  }

  public void setSlot(String slot) {
    this.slot = slot;
  }

  public List<String> getConstraintSlots() {
    return constraintSlots;
  }

  public void setConstraintSlots(List<String> constraintSlots) {
    this.constraintSlots = constraintSlots;
  }
}
