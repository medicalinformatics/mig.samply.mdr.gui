/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.utils.Importer;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.Tables;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.tables.pojos.UserSourceCredentials;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.ValueDomainDto;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.beans.DataElementWizard.SearchStatus;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.job.concepts.MdmQuery;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * This wizard allows *basic* changes only, for more information see the following.
 *
 * <pre>
 * * definitions and designations (also the labels for permitted values)
 * * members (for groups)
 * * members that identify the same identified item (for records)
 * * slots
 * </pre>
 *
 * @author paul
 */
@SessionScoped
@ManagedBean
public class ChangeWizard extends DataElementGroupWizard {

  private static final long serialVersionUID = 1L;

  private MdrElement element;

  private List<WizardStep> steps;

  private ValueDomainDto valueDomain;

  private SearchStatus searchStatusResponse = SearchStatus.TRUE;

  public SearchStatus getSearchStatus() {
    return searchStatusResponse;
  }

  @Override
  public String validateSlots() {
    if (element.getElement().getElementType() == Elementtype.DATAELEMENT) {
      getPossibleConceptAssociations().clear();
      searchStatusResponse = SearchStatus.TRUE;
      return BeanValidators.validateSlots(getSlots())
          ? "changeconceptAssociation?faces-redirect=true"
          : null;
    } else {
      return BeanValidators.validateSlots(getSlots())
          ? "changeverify?faces-redirect=true"
          : null;
    }
  }

  /**
   * starts searching process of concept associations in external sources, e.g. MDM-Portal
   */
  public void searchConceptAssociation() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<UserSourceCredentials> userSourceCredentials =
          ctx.fetch(ctx.select()
              .from(Tables.USER_SOURCE_CREDENTIALS)
              .where(Tables.USER_SOURCE_CREDENTIALS.USER_ID.eq(getUserBean().getUserId())))
              .into(UserSourceCredentials.class);

      for (UserSourceCredentials usc : userSourceCredentials) {
        if (usc.getSourceId()
            .equals((ctx.fetchOne(Tables.SOURCE, Tables.SOURCE.NAME.eq("MDM")))
                .getId())) {
          MdmQuery mdmQuery = new MdmQuery(getUserBean().getUserId(), usc.getCredential());
          try {
            setPossibleConceptAssociations(
                mdmQuery.getCodes(getDefinitions().get(0).getDesignation(), null));
            searchStatusResponse = SearchStatus.FALSE;
          } catch (Exception e) {
            switch (e.getMessage()) {
              case "401":
                searchStatusResponse = SearchStatus.ERROR_401; // Unauthorized user
                Message.addError("errorUnauthorized");
                break;
              case "404":
                searchStatusResponse = SearchStatus.ERROR_404; // Not Found -> dead links
                Message.addError("errorNotFound");
                break;
              case "500":
                searchStatusResponse = SearchStatus.ERROR_500; // Internal Server Error
                Message.addError("errorInternalServerError");
                break;
              default:
                searchStatusResponse = SearchStatus.ERROR; // error
                Message.addError("errorOccured");
                break;
            }
          }
        }
        //TODO: if other concept sources are added, call up necessary methods here (e.g. SNOMED CT)
      }
    }
  }

  /**
   * Adds concept association from list of possible concept associations by term.
   */
  public void addConceptAssociationByTerm(String term) {
    if (getPossibleConceptAssociations().size() > 0) {
      int i = 0;
      for (ConceptAssociation ca : getPossibleConceptAssociations()) {

        if (ca.getTerm() != null && ca.getTerm().equals(term)) {
          // deletes blank concept association
          if (getConceptAssociations().size() != 0) {
            if (getConceptAssociations().get(0).getTerm().equals("")) {
              getConceptAssociations().remove(0);
            }
          }

          getConceptAssociations().add(ca);
          // deletes given concept association from list of possible concept association
          getPossibleConceptAssociations().remove(i);
          break;
        }
        i++;
      }
    }
  }

  /**
   * TODO: add javadoc.
   */
  public String validateConceptAssociation() {
    return BeanValidators.validateConceptAssociation(getConceptAssociations())
        ? "changeverify?faces-redirect=true"
        : null;
  }

  @Override
  public String validateDefinitions() {
    setStarted(true);
    if (BeanValidators.validateDefinitions(getDefinitions())) {
      switch (element.getElement().getElementType()) {
        case DATAELEMENT:
          if (valueDomain.getDatatype() == DatatypeField.LIST) {
            return "changevalidation?faces-redirect=true";
          } else {
            return "changeslots?faces-redirect=true";
          }

        case DATAELEMENTGROUP:
        case RECORD:
        default:
          return "changeassignment?faces-redirect=true";
      }
    } else {
      return null;
    }
  }

  @Override
  public Elementtype getElementType() {
    if (element != null) {
      return element.getElement().getElementType();
    } else {
      return Elementtype.DATAELEMENT;
    }
  }

  @Override
  public String finish() {
    String definitions = validateDefinitions();
    String identification = validateIdentification();
    String members = validateMembers();

    if (definitions == null || identification == null || members == null) {
      return null;
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Namespace namespace = ElementDao.getNamespace(ctx, getNamespace());

      ScopedIdentifier identifier =
          super.finish(ctx, element.getElement().getId(), namespace.getId());

      if (element.getElement().getElementType() == Elementtype.DATAELEMENT) {
        if (valueDomain.getDatatype() == DatatypeField.LIST) {
          List<AdapterPermissibleValue> values = valueDomain.getPermittedValues();
          for (AdapterPermissibleValue value : values) {
            for (AdapterDefinition definition : value.getDefinitions()) {
              Definition def = Mapper.convert(definition);
              def.setElementId(value.getId());
              def.setScopedIdentifierId(identifier.getId());
              DefinitionDao.saveDefinition(ctx, def);
            }
          }
        }
      } else {
        Importer importer = new Importer();

        int index = 1;
        for (MdrElement element : getElements()) {
          ScopedIdentifier sub =
              importer.importElement(
                  ctx, element.getUrn(), identifier.getNamespace(), getUserBean().getUserId());
          ScopedIdentifierHierarchyDao
              .addSubIdentifier(ctx, identifier.getId(), sub.getId(), index++);
        }
      }
      ctx.connection(Connection::commit);
      return done();
    } catch (DuplicateIdentifierException | ValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Initializes this wizard for the given URN.
   */
  public String initialize(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      element = template(ctx, urn);

      steps = new ArrayList<>();
      steps.add(
          new WizardStep(JsfUtil.getString("definitions"), 1, "/user/changedefinitions.xhtml"));

      if (element.getElement().getElementType() == Elementtype.DATAELEMENT) {
        valueDomain =
            Mapper.createValueDomain(
                ctx, getUserBean().getUserId(), element.getElement(),
                getUserBean().getSelectedLanguage());

        if (valueDomain.getDatatype() == DatatypeField.LIST) {
          steps.add(
              new WizardStep(JsfUtil.getString("validation"), 2, "/user/changevalidation.xhtml"));
        }
      } else if (element.getElement().getElementType() == Elementtype.DATAELEMENTGROUP) {
        setElements(
            Mapper.convertElements(
                IdentifiedDao.getSubMembers(ctx, getUserBean().getUserId(), urn),
                getUserBean().getSelectedLanguage(),
                true));
        steps.add(
            new WizardStep(JsfUtil.getString("membersShort"), 2, "/user/changeassignment.xhtml"));
      } else {
        setElements(
            Mapper.convertElements(
                IdentifiedDao.getEntries(ctx, getUserBean().getUserId(), urn),
                getUserBean().getSelectedLanguage(),
                false));
        steps.add(new WizardStep(JsfUtil.getString("entries"), 2, "/user/changeassignment.xhtml"));
      }

      steps.add(new WizardStep(JsfUtil.getString("slots"), 3, "/user/changeslots.xhtml"));

      if (element.getElement().getElementType() == Elementtype.DATAELEMENT) {
        steps.add(
            new WizardStep(
                JsfUtil.getString("conceptAssociation"),
                4,
                "/user/changeconceptAssociation.xhtml"));
        steps.add(new WizardStep(JsfUtil.getString("verification"), 5, "/user/changeverify.xhtml"));

      } else {
        steps.add(new WizardStep(JsfUtil.getString("verification"), 4, "/user/changeverify.xhtml"));
      }

    } catch (DataAccessException e) {
      reset();
      return null;
    }
    return "/user/changedefinitions.xhtml?faces-redirect=true";
  }

  /**
   * TODO: add javadoc.
   */
  public String validateValidation() {
    return BeanValidators.validateValidation(valueDomain)
        ? "changeslots?faces-redirect=true"
        : null;
  }

  /* (non-Javadoc)
   * @see de.samply.mdr.gui.beans.DataElementGroupWizard#elementValid(de.samply.mdr.gui.MdrElement)
   */
  @Override
  protected boolean elementValid(MdrElement foundElement) {
    /*
     * Records allows dataelements only.
     */
    if (getElementType() == Elementtype.RECORD) {
      return foundElement.getElement().getElementType() == Elementtype.DATAELEMENT;
    } else {
      return super.elementValid(foundElement);
    }
  }

  /**
   * If the element we are changing is a record, check its dataelements for identity.
   */
  @Override
  public String validateMembers() {
    if (getElementType() == Elementtype.RECORD) {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        List<IdentifiedElement> entries =
            IdentifiedDao.getEntries(ctx, getUserBean().getUserId(), element.getUrn());

        if (getElements().size() != entries.size()) {
          Message.addError("invalidMemberCount");
          return null;
        }

        for (int i = 0; i < entries.size(); ++i) {
          IdentifiedElement element =
              IdentifiedDao.getElement(
                  ctx, getUserBean().getUserId(), getElements().get(i).getUrn());
          if (element.getId() != entries.get(i).getId()) {
            Message.addError("invalidElement", element.getScoped().toString());
            return null;
          }
        }
        return "changeslots?faces-redirect=true";
      }
    } else {

      return "changeslots?faces-redirect=true";
    }
  }

  @Override
  public String reset() {
    if (element != null) {
      return "/detail?faces-redirect=true&urn=" + element.getUrn();
    } else {
      return null;
    }
  }

  @Override
  public List<WizardStep> getSteps() {
    return steps;
  }

  @Override
  public Boolean getChange() {
    return true;
  }

  public ValueDomainDto getValueDomain() {
    return valueDomain;
  }

  public void setValueDomain(ValueDomainDto valueDomain) {
    this.valueDomain = valueDomain;
  }
}
