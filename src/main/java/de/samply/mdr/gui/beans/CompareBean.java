/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * Compares at least two groups. Gets all elements in those groups first and checks if the URNs
 * describe the same elements.
 *
 * @author paul
 */
@ManagedBean
@ViewScoped
public class CompareBean implements Serializable {

  private static final long serialVersionUID = 1L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** The list of groups that should be compared. */
  private List<MdrElement> list;

  /** The list of elements in those groups. */
  private List<MdrElement> elements;

  /** A set of element IDs that have already been added to the list of elements. */
  private HashSet<Integer> addedElements;

  /**
   * The hashmap with the group as key and the set of element ids that are in this group as value..
   */
  private HashMap<MdrElement, HashSet<Integer>> rootMap;

  /** TODO: add javadoc. */
  @PostConstruct
  public void initialize() {
    list = userBean.getCompare();

    elements = new ArrayList<>();
    addedElements = new HashSet<>();
    rootMap = new HashMap<>();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (MdrElement group : list) {
        rootMap.put(group, new HashSet<>());
        addSubElements(ctx, rootMap.get(group), group);
      }
    }
  }

  /** searches the group for all sub elements and adds them to the proper list / set. */
  private void addSubElements(DSLContext ctx, HashSet<Integer> rootSet, MdrElement group) {
    for (MdrElement element :
        Mapper.convertElements(
            IdentifiedDao.getAllSubMembers(ctx, userBean.getUserId(), group.getUrn()),
            userBean.getSelectedLanguage(),
            false)) {
      rootSet.add(element.getElement().getId());
      if (!addedElements.contains(element.getElement().getId())) {
        elements.add(element);
        addedElements.add(element.getElement().getId());
      }
    }
  }

  /** Returns the bootstrap status (the color for the table row) for the given element. */
  public String status(MdrElement element) {
    int count = 0;

    for (MdrElement group : list) {
      if (contains(element, group)) {
        count++;
      }
    }

    if (count == 0) {
      return "danger";
    } else if (count == 1) {
      return "danger";
    } else if (count == list.size()) {
      return "success";
    } else {
      return "warning";
    }
  }

  public Boolean contains(MdrElement element, MdrElement group) {
    return rootMap.get(group).contains(element.getElement().getId());
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public HashSet<Integer> getAddedElements() {
    return addedElements;
  }

  public void setAddedElements(HashSet<Integer> addedElements) {
    this.addedElements = addedElements;
  }

  public HashMap<MdrElement, HashSet<Integer>> getRootMap() {
    return rootMap;
  }

  public void setRootMap(HashMap<MdrElement, HashSet<Integer>> rootMap) {
    this.rootMap = rootMap;
  }

  public List<MdrElement> getElements() {
    return elements;
  }

  public void setElements(List<MdrElement> elements) {
    this.elements = elements;
  }

  public List<MdrElement> getList() {
    return list;
  }

  public void setList(List<MdrElement> list) {
    this.list = list;
  }
}
