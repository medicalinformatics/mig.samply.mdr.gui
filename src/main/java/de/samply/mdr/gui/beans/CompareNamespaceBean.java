/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui.beans;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierRelationDao;
import de.samply.mdr.dal.dto.ComparisonMode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.tables.pojos.ScopedIdentifierRelation;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.lib.Constants;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

@ManagedBean
@SessionScoped
public class CompareNamespaceBean {

  private static final long serialVersionUID = 6009006918132244972L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  private String identifierRelations;

  private Gson gson;

  @PostConstruct
  public void init() {
    gson = new Gson();
    loadLinks();
  }

  /**
   * TODO.
   */
  public List<MdrNamespace> getNamespaces() {
    Set<MdrNamespace> namespaces = new HashSet<>();
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      // own namespaces
      namespaces.addAll(userBean.getWritableNamespaces());
      // other namespaces
      namespaces.addAll(Mapper
          .convertNamespaces(NamespaceDao.getReadableNamespaces(ctx, userBean.getUserId()),
              userBean.getSelectedLanguage()));
    } catch (DataAccessException e) {
      e.printStackTrace();
    }

    List<MdrNamespace> retValue = new ArrayList<>(namespaces);
    retValue.sort(MdrNamespace.nameComparator);
    return retValue;
  }

  /**
   * Update the element relations in the database.
   */
  public void updateLinks() {
    JsonParser jsonParser = new JsonParser();
    JsonArray jsonArray = jsonParser.parse(identifierRelations).getAsJsonArray();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ScopedIdentifierRelationDao.deleteAllScopedIdentifierRelations(ctx, userBean.getUserId());
      for (JsonElement relation : jsonArray) {
        ScopedIdentifierRelationDao.createScopedIdentifierRelation(ctx, userBean.getUserId(),
            relation.getAsJsonObject().get("urnLeft").getAsString(),
            relation.getAsJsonObject().get("urnRight").getAsString(),
            relation.getAsJsonObject().get("relation").getAsString());
      }
    } catch (DataAccessException e) {
      e.printStackTrace();
    }
  }

  private void loadLinks() {
    JsonArray jsonArray = new JsonArray();
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<ScopedIdentifierRelation> relations = ScopedIdentifierRelationDao
          .getAllRelations(ctx, userBean.getUserId());

      for (ScopedIdentifierRelation relation : relations) {
        ScopedIdentifier leftIdentifier = ScopedIdentifierDao
            .getScopedIdentifier(ctx, relation.getLeftIdentifierId());
        ScopedIdentifier rightIdentifier = ScopedIdentifierDao
            .getScopedIdentifier(ctx, relation.getRightIdentifierId());

        IdentifiedElement leftElement = IdentifiedDao
            .getElement(ctx, userBean.getUserId(), leftIdentifier.toString());
        IdentifiedElement rightElement = IdentifiedDao
            .getElement(ctx, userBean.getUserId(), rightIdentifier.toString());

        MdrElement leftMdrElement = Mapper.convert(leftElement, userBean.getSelectedLanguage());
        MdrElement rightMdrElement = Mapper.convert(rightElement, userBean.getSelectedLanguage());

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("urnLeft", leftIdentifier.toString());
        jsonObject.addProperty("nameLeft", leftMdrElement.getDesignation());
        jsonObject.addProperty("urnRight", rightIdentifier.toString());
        jsonObject.addProperty("nameRight", rightMdrElement.getDesignation());
        jsonObject.addProperty("relation", relation.getRelation().getLiteral());
        jsonArray.add(jsonObject);
      }

    } catch (DataAccessException e) {
      e.printStackTrace();
    }
    identifierRelations = gson.toJson(jsonArray);
  }

  public List<Language> getLanguages() {
    return Arrays.asList(Constants.Language.values());
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public ComparisonMode[] getComparisonTypes() {
    return ComparisonMode.values();
  }

  public String getIdentifierRelations() {
    return identifierRelations;
  }

  public void setIdentifierRelations(String identifierRelations) {
    this.identifierRelations = identifierRelations;
  }
}
