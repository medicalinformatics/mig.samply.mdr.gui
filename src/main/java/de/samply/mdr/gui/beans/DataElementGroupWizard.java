/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.utils.Importer;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * The wizard used to create a new data element group (or use an existing data element group as a
 * template) or change a data element group in the first draft status.
 *
 * @author paul
 */
@ManagedBean
@SessionScoped
public class DataElementGroupWizard extends ElementWizard {

  private static final long serialVersionUID = 1L;

  /** The list of elements that are currently in the group. */
  private List<MdrElement> elements = new ArrayList<>();

  /** The list of URNs that have been dropped in the left panel. */
  private String items = "";

  public DataElementGroupWizard() {}

  @Override
  public String reset() {
    elements.clear();
    items = "";

    setStarted(false);

    return super.reset();
  }

  /**
   * Refreshes the array of elements. Called when the user dropped a new element on the left panel.
   */
  public void refreshItems() {
    String[] items = this.items.split(",");
    List<MdrElement> old = elements;
    elements = new ArrayList<>();

    for (String item : items) {
      if (item.trim().length() == 0) {
        continue;
      }

      MdrElement foundElement = findElement(item, old);
      if (elementValid(foundElement)) {
        elements.add(foundElement);
      }
    }
  }

  /** Checks, if the element is a valid member of this group. */
  protected boolean elementValid(MdrElement foundElement) {
    /** Groups can contain everything. */
    return true;
  }

  @Override
  public String template(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      template(ctx, urn);
      elements =
          Mapper.convertElements(
              IdentifiedDao.getSubMembers(ctx, getUserBean().getUserId(), urn),
              getUserBean().getSelectedLanguage(),
              true);
    } catch (DataAccessException e) {
      reset();
      return null;
    }

    return "/user/groupdefinitions?faces-redirect=true";
  }

  /** Finds the TODO. */
  private MdrElement findElement(String item, List<MdrElement> old) {
    for (MdrElement element : old) {
      if (element.getUrn().equals(item)) {
        return element;
      }
    }

    for (MdrElement element : getUserBean().getStarred()) {
      if (element.getUrn().equals(item)) {
        return element;
      }
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return Mapper.convert(
          IdentifiedDao.getElement(ctx, getUserBean().getUserId(), item),
          getUserBean().getSelectedLanguage());
    }
  }

  /** Validates the slots. */
  @Override
  public String validateSlots() {
    return BeanValidators.validateSlots(getSlots()) ? "groupverify?faces-redirect=true" : null;
  }

  /** Validates the definitions. */
  @Override
  public String validateDefinitions() {
    setStarted(true);
    return BeanValidators.validateDefinitions(getDefinitions())
        ? "groupassignment?faces-redirect=true"
        : null;
  }

  /** Called when the "Finish" button is clicked. */
  public String finish() {
    String definitions = validateDefinitions();
    String slots = validateSlots();
    String members = validateMembers();

    if (definitions == null || slots == null || members == null) {
      return null;
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Namespace namespace = ElementDao.getNamespace(ctx, getNamespace());

      DataElementGroup group = new DataElementGroup();
      ElementDao.saveElement(ctx, getUserBean().getUserId(), group);

      ScopedIdentifier superIdentifier = super.finish(ctx, group.getId(), namespace.getId());

      /*
        * In case the user has dropped elements from another namespace, use the importer which will
        * return the URN itself if it is already in the required namespace.
        */
      Importer importer = new Importer();

      for (MdrElement element : elements) {
        ScopedIdentifier sub =
            importer.importElement(
                ctx, element.getUrn(), superIdentifier.getNamespace(), getUserBean().getUserId());
        ScopedIdentifierHierarchyDao.addSubIdentifier(ctx, superIdentifier.getId(), sub.getId());
      }
      ctx.connection(Connection::commit);

      refreshViews();
      return done();
    } catch (DuplicateIdentifierException | ValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  /** Validates the members of this group. */
  public String validateMembers() {
    return "groupslots?faces-redirect=true";
  }

  public String getItems() {
    return items;
  }

  public void setItems(String items) {
    this.items = items;
  }

  public List<MdrElement> getElements() {
    return elements;
  }

  public void setElements(List<MdrElement> elements) {
    this.elements = elements;
  }

  @Override
  protected Elementtype getElementType() {
    return Elementtype.DATAELEMENTGROUP;
  }

  @Override
  public List<WizardStep> getSteps() {
    return Arrays.asList(
        new WizardStep(JsfUtil.getString("definitions"), 1, "/user/groupdefinitions.xhtml"),
        new WizardStep(JsfUtil.getString("membersShort"), 2, "/user/groupassignment.xhtml"),
        new WizardStep(JsfUtil.getString("slots"), 3, "/user/groupslots.xhtml"),
        new WizardStep(JsfUtil.getString("verification"), 4, "/user/groupverify.xhtml"));
  }

  @Override
  public Boolean getDraftButtonOnly(String viewId) {
    return viewId.endsWith("groupverify.xhtml") && !allMembersReleasable();
  }

  /**
   * Go through all group members and check if they have another validation type than "TBD".
   *
   * @return true if no "TBD" members are present, false if at least one member is "TBD"
   */
  protected boolean allMembersReleasable() {
    List<ScopedIdentifier> unreleasableElements;
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      unreleasableElements = ScopedIdentifierDao.getUnreleasableElementIdentifiers(ctx);
      for (MdrElement element : elements) {
        if (unreleasableElements.contains(element.getElement().getScoped())) {
          return false;
        }
      }
    }
    return true;
  }
}
