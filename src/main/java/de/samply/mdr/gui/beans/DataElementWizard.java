/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.HierarchyDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.PermissibleCodeDao;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.Tables;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.tables.pojos.UserSourceCredentials;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.ValueDomainDto;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.job.concepts.MdmQuery;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterElement;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.AdapterValueDomain;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.omnifaces.util.Faces;

/**
 * The wizard used to create a new data element (or use an existing data element as a template) or
 * change a data element in the first draft status.
 *
 * @author paul
 */
@ManagedBean
@SessionScoped
public class DataElementWizard extends ElementWizard {

  private static final long serialVersionUID = -7038334163625057667L;
  private static final int DEFAULT_LIMIT = 20;
  private static final int DEFAULT_THRESHOLD = 50;
  public SearchStatus searchStatusResponse = SearchStatus.TRUE; // true
  /**
   * The data elements value domain.
   */
  private ValueDomainDto valueDomain;
  /**
   * The list of available catalogs.
   */
  private List<MdrElement> availableCatalogs;
  /**
   * The currently selected code. This is only necessary for dataelements with a catalog value
   * domain.
   */
  private String selectedCode;
  /**
   * The external adapter element used as a template for this data element wizard.
   */
  private AdapterElement externalElement = null;
  /**
   * The element id of the selected recommendation. -1 if no recommendation is selected.
   */
  private String selectedUrn = "";
  /**
   * The string that was last searched for in the recommendation wizard step.
   */
  private String searchedString = "";
  /**
   * The threshold that is used when fetching recommendations in percent (between 0 and 100).
   */
  private int threshold = DEFAULT_THRESHOLD;
  /**
   * The limit that is used when fetching recommendations (0 = unlimited).
   */
  private int limit = DEFAULT_LIMIT;

  public SearchStatus getSearchStatus() {
    return searchStatusResponse;
  }

  /**
   * Adds another permitted value to the value domain. This is called when the user presses the "add
   * another permitted value" button.
   */
  public String addPermittedValue() {
    AdapterPermissibleValue value = new AdapterPermissibleValue();
    for (AdapterDefinition definition : getDefinitions()) {
      AdapterDefinition def = new AdapterDefinition();
      def.setLanguage(definition.getLanguage());
      value.getDefinitions().add(def);
    }
    valueDomain.getPermittedValues().add(value);
    return null;
  }

  /**
   * Removes the given permissible value.
   */
  public void remove(AdapterPermissibleValue value) {
    valueDomain.getPermittedValues().remove(value);
  }

  @Override
  public String template(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      template(ctx, urn);
      valueDomain = Mapper.createValueDomain(
          ctx, getUserBean().getUserId(), getTemplateElement().getElement(),
          getUserBean().getSelectedLanguage());
    } catch (DataAccessException e) {
      reset();
      return null;
    }

    return "/user/elementdefinitions?faces-redirect=true";
  }

  /**
   * TODO: add javadoc.
   */
  public String initialize(AdapterElement externalTemplate) {
    reset();

    externalElement = externalTemplate;

    AdapterValueDomain externalVD = externalTemplate.createValueDomain();

    valueDomain.setPermittedValues(externalVD.getPermittedValues());

    valueDomain.setDatatype(externalVD.getDatatype());

    valueDomain.setFormat(externalVD.getFormat());
    valueDomain.setUseRegex(externalVD.getUseRegex());
    valueDomain.setUseRangeFrom(externalVD.getUseRangeFrom());
    valueDomain.setUseRangeTo(externalVD.getUseRangeTo());
    valueDomain.setRangeFrom(externalVD.getRangeFrom());
    valueDomain.setRangeTo(externalVD.getRangeTo());

    valueDomain.setDateRepresentation(externalVD.getDateRepresentation());
    valueDomain.setTimeRepresentation(externalVD.getTimeRepresentation());
    valueDomain.setUnitOfMeasure(externalVD.getUnitOfMeasure());
    valueDomain.setUseMaxLength(externalVD.getUseMaxLength());
    valueDomain.setMaxLength(externalVD.getMaxLength());
    valueDomain.setWithinRange(externalVD.getWithinRange());
    valueDomain.setWithDays(externalVD.getWithDays());
    valueDomain.setWithSeconds(externalVD.getWithSeconds());

    setDefinitions(externalTemplate.getDefinitions());

    return "/user/elementdefinitions?faces-redirect=true";
  }

  /**
   * Validates the value domain.
   */
  public String validateValidation() {
    boolean valid = BeanValidators.validateValidation(valueDomain);
    if (!valid) {
      return null;
    } else if (valueDomain.getDatatype() == DatatypeField.CATALOG) {

      if (valueDomain.getCatalogRoot() == null
          || !valueDomain.getCatalogRoot().getElement().getUrn().equals(valueDomain.getCatalog())) {
        try (DSLContext ctx = ResourceManager.getDslContext()) {
          IdentifiedElement catalog =
              IdentifiedDao.getElement(ctx, getUserBean().getUserId(), valueDomain.getCatalog());

          valueDomain.setCatalogRoot(
              new TreeNode(Mapper.convert(catalog, getUserBean().getSelectedLanguage())));

          HashMap<Integer, TreeNode> index = new HashMap<>();
          index.put(catalog.getScoped().getId(), valueDomain.getCatalogRoot());

          HashMap<String, TreeNode> urnIndex = new HashMap<>();
          urnIndex.put(valueDomain.getCatalog(), valueDomain.getCatalogRoot());

          for (IdentifiedElement element :
              IdentifiedDao.getAllSubMembers(
                  ctx, getUserBean().getUserId(), valueDomain.getCatalog())) {
            index.put(
                element.getScoped().getId(),
                new TreeNode(Mapper.convert(element, getUserBean().getSelectedLanguage())));
            urnIndex.put(element.getScoped().toString(), index.get(element.getScoped().getId()));
          }

          valueDomain.setSubCodes(Mapper.convertElements(
              IdentifiedDao.getEntries(ctx, getUserBean().getUserId(), valueDomain.getCatalog()),
              getUserBean().getSelectedLanguage(),
              false));

          for (HierarchyNode node : HierarchyDao.getHierarchyNodes(ctx, valueDomain.getCatalog())) {
            index.get(node.getSuperId()).getChildren().add(index.get(node.getSubId()));
            index.get(node.getSubId()).getParents().add(index.get(node.getSuperId()));
          }

          valueDomain.setUrnIndex(urnIndex);
          valueDomain.setIndex(index);
        }
      }

      return "elementcatalog?faces-redirect=true";
    } else {
      return "elementslots?faces-redirect=true";
    }
  }

  @Override
  public String reset() {
    externalElement = null;
    valueDomain = new ValueDomainDto();
    selectedUrn = "";
    searchedString = "";
    limit = DEFAULT_LIMIT;
    threshold = DEFAULT_THRESHOLD;
    searchStatusResponse = SearchStatus.TRUE;

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      availableCatalogs =
          Mapper.convertElements(
              IdentifiedDao.getCatalogs(ctx, getUserBean().getUserId()),
              getUserBean().getSelectedLanguage(),
              true);
    }

    return super.reset();
  }

  /**
   * Called when the user finished the wizard. This method must save the data element in the
   * database, if everything is valid.
   */
  public String finish() {
    String definitions = validateDefinitions();
    String slots = validateSlots();
    String validation = validateValidation();
    String conceptAssociation = validateConceptAssociation();

    if (definitions == null
        || slots == null
        || validation == null
        || conceptAssociation == null) {
      return null;
    }

    ValueDomain domain = Mapper.convert(valueDomain);

    /*
     * Determine the maxLength of all available permitted values.
     */
    if (domain instanceof EnumeratedValueDomain) {
      int maxLength = 1;
      for (AdapterPermissibleValue dto : valueDomain.getPermittedValues()) {
        if (maxLength < dto.getValue().length()) {
          maxLength = dto.getValue().length();
        }
      }
      domain.setMaxCharacters(maxLength);
    }

    /*
     * Determine the maxLength value for all currently available codes.
     */
    if (domain instanceof CatalogValueDomain) {
      int maxLength = 1;
      for (TreeNode node : valueDomain.getIndex().values()) {
        if (node.getElement().getElement().getElementType() == Elementtype.CODE) {
          Code c = (Code) node.getElement().getElement().getElement();
          int length = c.getCode().length();
          if (node.isSelected() && length > maxLength) {
            maxLength = length;
          }
        }
      }
      domain.setMaxCharacters(maxLength);
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      /*
       * Save the value domain first.
       */
      ElementDao.saveElement(ctx, getUserBean().getUserId(), domain);

      DataElement element = new DataElement();
      element.setValueDomainId(domain.getId());

      if (externalElement != null) {
        element.setExternalId(externalElement.getIdentification());
      }

      ElementDao.saveElement(ctx, getUserBean().getUserId(), element);

      DescribedElement namespaceElement =
          NamespaceDao.getNamespace(ctx, getUserBean().getUserId(), getNamespace());
      ScopedIdentifier identifier =
          super.finish(ctx, element.getId(), namespaceElement.getElement().getId());

      Namespace namespace = (Namespace) namespaceElement.getElement();

      if (domain instanceof EnumeratedValueDomain) {
        /*
         * For every value we have to check the namespace constraints.
         */
        boolean valid = true;

        for (AdapterPermissibleValue v : valueDomain.getPermittedValues()) {
          PermissibleValue value = new PermissibleValue();
          value.setValueDomainId(domain.getId());
          value.setPermittedValue(v.getValue());

          ElementDao.saveElement(ctx, getUserBean().getUserId(), value);

          valid = valid && BeanValidators.validateValueConstraints(namespace.getLanguages(), v);

          for (AdapterDefinition definition : v.getDefinitions()) {
            Definition def = Mapper.convert(definition);
            def.setElementId(value.getId());
            def.setScopedIdentifierId(identifier.getId());
            DefinitionDao.saveDefinition(ctx, def);
          }
        }

        /*
         * If the namespace constraints are not met, throw an exception to show the error
         * messages.
         */
        if (!valid) {
          throw new ValidationException();
        }
      }

      if (domain instanceof CatalogValueDomain) {
        for (TreeNode node : valueDomain.getIndex().values()) {
          if (node.getState() != ChildrenSelectionState.NONE
              && node.getElement().getElement().getElementType() == Elementtype.CODE) {
            PermissibleCode code = new PermissibleCode();
            code.setCatalogValueDomainId(domain.getId());
            code.setCodeScopedIdentifierId(node.getElement().getElement().getScoped().getId());
            PermissibleCodeDao.savePermissibleCode(ctx, code);
          }
        }
      }

      ctx.connection(c -> c.commit());
      return done();
    } catch (ValidationException | DuplicateIdentifierException e) {
      return null;
    }
  }

  /**
   * Selects all nodes.
   */
  public String selectAll() {
    selectAll(valueDomain.getUrnIndex().get(selectedCode));
    valueDomain.updateTreeState();
    return null;
  }

  /**
   * Selects all nodes in the given tree node.
   */
  private void selectAll(TreeNode node) {
    node.setSelected(true);
    node.setState(ChildrenSelectionState.ALL);

    for (TreeNode child : node.getChildren()) {
      selectAll(child);
    }
  }

  /**
   * Deselects all nodes.
   */
  public String deselectAll() {
    deselectAll(valueDomain.getUrnIndex().get(selectedCode));
    valueDomain.updateTreeState();
    return null;
  }

  /**
   * Deselects all codes in the given tree node.
   */
  private void deselectAll(TreeNode node) {
    node.setSelected(false);
    node.setState(ChildrenSelectionState.NONE);

    for (TreeNode child : node.getChildren()) {
      child.setSelected(false);

      if (child.getState() != ChildrenSelectionState.NONE) {
        child.setState(ChildrenSelectionState.NONE);
        deselectAll(child);
      }
    }
  }

  public String validateCatalog() {
    return "elementslots?faces-redirect=true";
  }

  @Override
  public String validateSlots() {
    getPossibleConceptAssociations().clear();
    searchStatusResponse = SearchStatus.TRUE;
    return BeanValidators.validateSlots(getSlots())
        ? "elementconceptAssociation?faces-redirect=true"
        : null;
  }

  /**
   * starts searching process of concept associations in external sources, e.g. MDM-Portal
   */
  public void searchConceptAssociation() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<UserSourceCredentials> userSourceCredentials =
          ctx.fetch(ctx.select()
              .from(Tables.USER_SOURCE_CREDENTIALS)
              .where(Tables.USER_SOURCE_CREDENTIALS.USER_ID.eq(getUserBean().getUserId())))
              .into(UserSourceCredentials.class);
      for (UserSourceCredentials usc : userSourceCredentials) {
        if (usc.getSourceId()
            .equals((ctx.fetchOne(Tables.SOURCE, Tables.SOURCE.NAME.eq("MDM")))
                .getId())) {
          MdmQuery mdmQuery = new MdmQuery(getUserBean().getUserId(), usc.getCredential());
          try {
            setPossibleConceptAssociations(
                mdmQuery.getCodes(getDefinitions().get(0).getDesignation(), null));
            searchStatusResponse = SearchStatus.FALSE;
          } catch (Exception e) {
            switch (e.getMessage()) {
              case "401":
                searchStatusResponse = SearchStatus.ERROR_401; // Unauthorized user
                Message.addError("errorUnauthorized");
                break;
              case "404":
                searchStatusResponse = SearchStatus.ERROR_404; // Not Found -> dead links
                Message.addError("errorNotFound");
                break;
              case "500":
                searchStatusResponse = SearchStatus.ERROR_500; // Internal Server Error
                Message.addError("errorInternalServerError");
                break;
              default:
                searchStatusResponse = SearchStatus.ERROR; // error
                Message.addError("errorOccured");
                break;
            }
          }
        }
        //TODO: if other concept sources are added, call up necessary methods here (e.g. SNOMED CT)
      }
    }
  }

  /**
   * Adds concept association from list of possible concept associations by term.
   */
  public void addConceptAssociationByTerm(String term) {
    if (getPossibleConceptAssociations().size() > 0) {
      int i = 0;
      for (ConceptAssociation ca : getPossibleConceptAssociations()) {

        if (ca.getTerm() != null && ca.getTerm().equals(term)) {
          // deletes blank concept association
          if (getConceptAssociations().size() != 0) {
            if (getConceptAssociations().get(0).getTerm().equals("")) {
              getConceptAssociations().remove(0);
            }
          }

          getConceptAssociations().add(ca);
          // deletes given concept association from list of possible concept association
          getPossibleConceptAssociations().remove(i);
          break;
        }
        i++;
      }
    }
  }

  /**
   * Validates the concept associations.
   *
   * @return String to redirect to elementverify
   */
  public String validateConceptAssociation() {
    return BeanValidators.validateConceptAssociation(getConceptAssociations())
        ? "elementverify?faces-redirect=true"
        : null;
  }

  @Override
  public String validateDefinitions() {
    setStarted(true);
    if (BeanValidators.validateDefinitions(getDefinitions())) {
      if (valueDomain.getPermittedValues().size() == 0) {
        addPermittedValue();
      }
      return "validation?faces-redirect=true";
    } else {
      return null;
    }
  }

  /**
   * TODO: add javadoc.
   */
  public String continueToDefinitions() {
    if (!selectedUrn.equals("")) {
      return template(selectedUrn);
    } else {
      // if no element is selected make sure to reset so we do not have a previous template selected
      final String searchedString = this.searchedString;
      final int limit = this.limit;
      final int threshold = this.threshold;
      final String selectedUrn = this.selectedUrn;

      reset();

      this.searchedString = searchedString;
      this.limit = limit;
      this.threshold = threshold;
      this.selectedUrn = selectedUrn;
    }
    return "elementdefinitions?faces-redirect=true";
  }

  @Override
  public List<WizardStep> getSteps() {
    return Arrays.asList(
        new WizardStep(JsfUtil.getString("recommendations"), 1, "/user/recommendations.xhtml"),
        new WizardStep(JsfUtil.getString("definitions"), 2, "/user/elementdefinitions.xhtml"),
        new WizardStep(
            JsfUtil.getString("validation"),
            3,
            "/user/validation.xhtml",
            "/user/elementcatalog.xhtml"),
        new WizardStep(JsfUtil.getString("slots"), 4, "/user/elementslots.xhtml"),
        new WizardStep(
            JsfUtil.getString("conceptAssociation"), 5, "/user/elementconceptAssociation.xhtml"),
        new WizardStep(JsfUtil.getString("verification"), 6, "/user/elementverify.xhtml"));
  }

  @Override
  protected Elementtype getElementType() {
    return Elementtype.DATAELEMENT;
  }

  public ValueDomainDto getValueDomain() {
    return valueDomain;
  }

  public void setValueDomain(ValueDomainDto valueDomain) {
    this.valueDomain = valueDomain;
  }

  public List<MdrElement> getAvailableCatalogs() {
    return availableCatalogs;
  }

  public void setAvailableCatalogs(List<MdrElement> availableCatalogs) {
    this.availableCatalogs = availableCatalogs;
  }

  public String getSelectedCode() {
    return selectedCode;
  }

  public void setSelectedCode(String selectedCode) {
    this.selectedCode = selectedCode;
  }

  public AdapterElement getExternalElement() {
    return externalElement;
  }

  public void setExternalElement(AdapterElement externalElement) {
    this.externalElement = externalElement;
  }

  public String getSelectedUrn() {
    return selectedUrn;
  }

  /**
   * TODO: add javadoc.
   */
  public void setSelectedUrn() {
    try {
      selectedUrn = Faces.getRequestParameter("urn");
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getSearchedString() {
    return searchedString;
  }

  public void setSearchedString(String searchedString) {
    this.searchedString = searchedString;
  }

  public int getThreshold() {
    return threshold;
  }

  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  @Override
  public Boolean disableNextButton(String viewId) {
    return viewId.endsWith("validation.xhtml")
        && (valueDomain == null || valueDomain.getDatatype() == null);
  }

  @Override
  public Boolean getDraftButtonOnly(String viewId) {
    return viewId.endsWith("elementverify.xhtml")
        && valueDomain != null
        && valueDomain.getDatatype() == DatatypeField.TBD;
  }

  public enum SearchStatus {
    TRUE,
    FALSE,
    ERROR,
    ERROR_401,
    ERROR_404,
    ERROR_500
  }
}
