/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ConceptAssociationDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.DetailedMdrElement;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * Handles the detailed view of an element.
 *
 * @author paul
 */
@ViewScoped
@ManagedBean
public class DetailViewBean implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The URN of the element. */
  private String urn;

  /** If true, the members are draggable. */
  private Boolean draggable;

  /** The fully detailed MDR element. */
  private DetailedMdrElement element;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{dataElementWizard}")
  private DataElementWizard dataElementWizard;

  @ManagedProperty(value = "#{dataElementGroupWizard}")
  private DataElementGroupWizard dataElementGroupWizard;

  @ManagedProperty(value = "#{recordWizard}")
  private RecordWizard recordWizard;

  @ManagedProperty(value = "#{catalogWizard}")
  private CatalogWizard catalogWizard;

  @ManagedProperty(value = "#{changeWizard}")
  private ChangeWizard changeWizard;

  private String importIntoNamespace;

  private Boolean isValid;

  private String node;

  private List<MdrElement> codes;

  private boolean readonly;

  /** A list of scoped identifiers that identify unreleasable elements. */
  private List<ScopedIdentifier> unreleasableElements = Collections.emptyList();

  /** Called by the f:viewAction. */
  public void initialize() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      element =
          Mapper.convert(
              ctx,
              userBean.getUserId(),
              IdentifiedDao.getElement(ctx, userBean.getUserId(), urn),
              Mapper.convertSlots(SlotDao.getSlots(ctx, urn)),
              ScopedIdentifierDao.getVersions(ctx, urn),
              getUserBean().getSelectedLanguage(),
              ConceptAssociationDao.getConceptAssociations(ctx, urn,userBean.getUserId()));
      if (element.getElement().getElementType() == Elementtype.CODE) {
        Code code = (Code) element.getElement().getElement();
        isValid = code.isValid();
      }

      if (node != null) {
        setCodes(
            Mapper.convertElements(
                IdentifiedDao.getEntries(ctx, userBean.getUserId(), node),
                getUserBean().getSelectedLanguage(),
                false));
      }
      unreleasableElements = ScopedIdentifierDao.getUnreleasableElementIdentifiers(ctx);
    }
  }

  /** Resets the corresponding wizard and starts it with the specified element as template. */
  public String template(MdrElement element) {
    ElementWizard wiz = getWizard(element);
    wiz.reset();
    return wiz.template(element.getUrn());
  }

  /** returns the correct wizard for the specified element. */
  private ElementWizard getWizard(MdrElement element) {
    switch (element.getElement().getElementType()) {
      case DATAELEMENT:
        return getDataElementWizard();

      case DATAELEMENTGROUP:
        return getDataElementGroupWizard();

      case RECORD:
        return getRecordWizard();

      case CATALOG:
        return getCatalogWizard();

      default:
        return null;
    }
  }

  /** initializes the correct wizard and redirects the user to the correct web page. */
  public String change(MdrElement element) {
    if (element.firstDraft()) {
      getWizard(element).setUseAsTemplate(false);
      return getWizard(element).template(element.getUrn());
    } else {
      /*
       * Because changing catalogs is treated differently and it uses
       * a different wizard.
       */
      if (element.getElement().getElementType() == Elementtype.CATALOG) {
        getCatalogWizard().setUseAsTemplate(false);
        return getCatalogWizard().initialize(element.getUrn());
      } else {
        getChangeWizard().setUseAsTemplate(false);
        return getChangeWizard().initialize(element.getUrn());
      }
    }
  }

  /**
   * Releases the element. It outdates all other released scoped identifiers for this scoped
   * identifier.
   */
  public String release(MdrElement element) {
    if (element.getDraft() && getUserBean().isWritableNamespaceUrn(element.getUrn())) {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        ctx.connection(c -> c.setAutoCommit(false));
        ScopedIdentifierDao.outdateIdentifiers(ctx, userBean.getUserId(), element.getUrn());
        ScopedIdentifierDao.release(ctx, userBean.getUserId(), element.getElement().getScoped());
        ctx.connection(Connection::commit);
      }
    }

    return "/detail?faces-redirect=true&urn=" + element.getUrn();
  }

  /** Duplicates an element. */
  public String duplicate(MdrElement element) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      ScopedIdentifier duplicate =
          IdentifiedDao.duplicate(
            ctx,
            element.getUrn(),
            element.getElement().getScoped().getNamespace(),
            userBean.getUserId(),
            true);
      ctx.connection(Connection::commit);
      return "/detail?faces-redirect=true&urn=" + duplicate.toString();
    }
  }

  /**
   * Check if an element is sufficiently defined to be released.
   *
   * @param mdrElement the element to check
   * @return true if it can be released, false otherwise
   */
  public Boolean isReleasable(MdrElement mdrElement) {
    return !unreleasableElements.contains(mdrElement.getElement().getScoped());
  }

  public DetailedMdrElement getElement() {
    return element;
  }

  public void setElement(DetailedMdrElement element) {
    this.element = element;
  }

  public DataElementWizard getDataElementWizard() {
    return dataElementWizard;
  }

  public void setDataElementWizard(DataElementWizard dataElementWizard) {
    this.dataElementWizard = dataElementWizard;
  }

  public DataElementGroupWizard getDataElementGroupWizard() {
    return dataElementGroupWizard;
  }

  public void setDataElementGroupWizard(DataElementGroupWizard dataElementGroupWizard) {
    this.dataElementGroupWizard = dataElementGroupWizard;
  }

  public RecordWizard getRecordWizard() {
    return recordWizard;
  }

  public void setRecordWizard(RecordWizard recordWizard) {
    this.recordWizard = recordWizard;
  }

  public Boolean getDraggable() {
    return draggable;
  }

  public void setDraggable(Boolean draggable) {
    this.draggable = draggable;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public String getImportIntoNamespace() {
    return importIntoNamespace;
  }

  public void setImportIntoNamespace(String importIntoNamespace) {
    this.importIntoNamespace = importIntoNamespace;
  }

  public ChangeWizard getChangeWizard() {
    return changeWizard;
  }

  public void setChangeWizard(ChangeWizard changeWizard) {
    this.changeWizard = changeWizard;
  }

  public String getUrn() {
    return urn;
  }

  public void setUrn(String urn) {
    this.urn = urn;
  }

  public Boolean getIsValid() {
    return isValid;
  }

  public void setIsValid(Boolean isValid) {
    this.isValid = isValid;
  }

  public String getNode() {
    return node;
  }

  public void setNode(String node) {
    this.node = node;
  }

  public List<MdrElement> getCodes() {
    return codes;
  }

  public void setCodes(List<MdrElement> codes) {
    this.codes = codes;
  }

  public boolean isReadonly() {
    return readonly;
  }

  public void setReadonly(boolean readonly) {
    this.readonly = readonly;
  }

  public CatalogWizard getCatalogWizard() {
    return catalogWizard;
  }

  public void setCatalogWizard(CatalogWizard catalogWizard) {
    this.catalogWizard = catalogWizard;
  }
}
