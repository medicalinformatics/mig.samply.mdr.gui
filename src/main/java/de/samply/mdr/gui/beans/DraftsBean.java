/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * Loads all drafts.
 *
 * @author paul
 */
@ManagedBean
@ViewScoped
public class DraftsBean implements Serializable {

  private static final long serialVersionUID = 1L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** The list of elements that are currently in the draft status. */
  private List<MdrElement> elements = Collections.emptyList();

  /** the list of elements that have been marked for release. */
  private HashMap<String, MdrElement> marked = new HashMap<>();

  /** The urn of the element that will be marked. */
  private String markItem;

  /** A list of scoped identifiers that identify unreleasable elements. */
  private List<ScopedIdentifier> unreleasableElements = Collections.emptyList();

  private String[] namespacesToDeleteFrom;

  /** Called by the f:viewAction. */
  public void initialize() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      elements = Mapper.convertElements(
          IdentifiedDao.getDrafts(
              ctx, userBean.getUserId()), getUserBean().getSelectedLanguage(), true);
      marked = new HashMap<>();
      unreleasableElements = ScopedIdentifierDao.getUnreleasableElementIdentifiers(ctx);
    }
  }

  /** TODO: add javadoc. */
  public void toggle() {
    if (marked.containsKey(markItem)) {
      marked.remove(markItem);
    } else {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        marked.put(
            markItem,
            Mapper.convert(
                IdentifiedDao.getElement(ctx, userBean.getUserId(), markItem),
                getUserBean().getSelectedLanguage()));
      }
    }
  }

  /** TODO: add javadoc. */
  public String release() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (MdrElement element : marked.values()) {
        ScopedIdentifierDao.release(ctx, userBean.getUserId(), element.getElement().getScoped());
      }
    }

    initialize();
    return null;
  }

  public Boolean isMarked(MdrElement element) {
    return marked.containsKey(element.getUrn());
  }

  public List<MdrElement> getElements() {
    return elements;
  }

  public void setElements(List<MdrElement> elements) {
    this.elements = elements;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public String getMarkItem() {
    return markItem;
  }

  public void setMarkItem(String markUrn) {
    this.markItem = markUrn;
  }

  public HashMap<String, MdrElement> getMarked() {
    return marked;
  }

  public void setMarked(HashMap<String, MdrElement> marked) {
    this.marked = marked;
  }

  public String[] getNamespacesToDeleteFrom() {
    return namespacesToDeleteFrom;
  }

  public void setNamespacesToDeleteFrom(String[] namespacesToDeleteFrom) {
    this.namespacesToDeleteFrom = namespacesToDeleteFrom;
  }

  /**
   * Check if an element is sufficiently defined to be released.
   *
   * @param mdrElement the element to check
   * @return true if it can be released, false otherwise
   */
  public Boolean isReleasable(MdrElement mdrElement) {
    return !unreleasableElements.contains(mdrElement.getElement().getScoped());
  }

  /**
   * Convert namespace id string array to namespace id integer list and call userbean method.
   * @return
   */
  public String deleteDrafts() {
    List<Integer> ids = new ArrayList<>();
    for (String namespace : namespacesToDeleteFrom) {
      ids.add(Integer.parseInt(namespace));
    }
    return userBean.deleteDrafts(ids);
  }
}
