/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.utils.Importer;
import de.samply.mdr.dal.dao.utils.Importer.Import;
import de.samply.mdr.dal.dao.utils.Importer.ImportStrategy;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.gui.RefreshThread;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.job.ImportToStagingJob;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Message;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Element;
import de.samply.mdr.xsd.Export;
import de.samply.mdr.xsd.Namespace;
import de.samply.mdr.xsd.ObjectFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.jooq.DSLContext;
import org.json.JSONObject;
import org.xml.sax.SAXException;

/** This wizard is used to import an XML file (serialized Export class from samply.xsd). */
@ManagedBean
@SessionScoped
public class ImportWizard extends AbstractWizard {

  private static final long serialVersionUID = -3812096496732558754L;

  @ManagedProperty(value = "#{threadExecutor}")
  private ThreadExecutorBean threadExecutorBean;

  /** The steps of this wizard. */
  private List<WizardStep> steps;

  /** The uploaded file, must be a valid catalog XML file. */
  private transient Part file;

  /** The deserialized uploaded export object. */
  private Export export;

  /** The current ImportStrategies. */
  private List<ImportStrategy> strategy = Collections.emptyList();

  /** If true, the importer will try to keep the identifier from the XML. */
  private Boolean keepIdentifier = false;

  /** The list of target namespaces. Those namespaces must be writable. */
  private List<MdrNamespace> targetNamespaces = Collections.emptyList();

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  private List<Import> imports = Collections.emptyList();

  /** TODO: add javadoc. */
  public ImportWizard() {
    steps = new ArrayList<>();
    steps.add(new WizardStep(JsfUtil.getString("uploadFile"), 1, "/user/import.xhtml"));
    steps.add(new WizardStep(JsfUtil.getString("strategy"), 2, "/user/importstrategy.xhtml"));
    steps.add(new WizardStep(JsfUtil.getString("verification"), 3, "/user/importverify.xhtml"));
  }

  public String initialize() {
    return steps.get(0).getHref().get(0) + "?faces-redirect=true";
  }

  /** Saves the catalog in the database. */
  public String finish() {
    return null;
  }

  /** TODO: add javadoc. */
  public String upload() {
    String fileName = JsfUtil.getFileName(file);

    if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
      return uploadExcel();
    } else {
      return uploadExport();
    }
  }

  /** TODO: add javadoc. */
  public String uploadExcel() {
    try {
      File importFile = JsfUtil.savePartToTmpFile("import_", file);
      threadExecutorBean.spawnJob(
          new ImportToStagingJob(
              importFile, userBean.getUserId(), userBean.getSelectedNamespace().getId()));
      Message.addInfo("importJobSpawned");
      return "/user/importtostaging.xhtml?faces-redirect=true";
    } catch (IllegalArgumentException | IOException e) {
      Message.addError("invalidImportFile");
      return null;
    }
  }

  /**
   * This method is called right after the upload. It checks if the uploaded export is valid (it
   * uses the XSD file to do that).
   */
  public String uploadExport() {
    try {
      JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

      InputStream commonXsd = Catalog.class.getClassLoader().getResourceAsStream("common.xsd");
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = sf.newSchema(new StreamSource(commonXsd));

      Validator validator = schema.newValidator();
      validator.validate(new StreamSource(file.getInputStream()));

      Object element = context.createUnmarshaller().unmarshal(file.getInputStream());

      /** If the uploaded file is not a serialized Export, throw an error. */
      if (!(element instanceof Export)) {
        Message.addError("invalidImportFile");
        return null;
      }

      export = (Export) element;

      strategy = new ArrayList<>();

      for (JAXBElement<? extends Element> jaxb : export.getElement()) {
        if (jaxb.getValue() instanceof de.samply.mdr.xsd.Namespace) {
          de.samply.mdr.xsd.Namespace namespace = (Namespace) jaxb.getValue();
          ImportStrategy is = new ImportStrategy();
          is.setFrom(namespace.getName());

          strategy.add(is);
        }
      }

      targetNamespaces = new ArrayList<>(userBean.getWritableNamespaces());

      return "/user/importstrategy.xhtml?faces-redirect=true";
    } catch (IOException | JAXBException e) {
      e.printStackTrace();
      Message.addError("invalidCatalog");
      return null;
    } catch (SAXException e) {
      e.printStackTrace();
      Message.addError("invalidCatalogError", e.getMessage());
      return null;
    }
  }

  /**
   * Verifies the import by doing it on the database but not committing the actual import. Sets the
   * imports field, so that it can be shown in the GUI.
   */
  public String verify() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Importer importer = new Importer();
      importer.importExport(ctx, export, strategy, keepIdentifier, userBean.getUserId());
      imports = importer.getImports();
      // DO NOT COMMIT HERE, THIS IS ONLY A PREVIEW!
      return "/user/importverify?faces-redirect=true";
    }
  }

  /** Committing the catalog. */
  public String commit() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Importer importer = new Importer();
      importer.importExport(ctx, export, strategy, keepIdentifier, userBean.getUserId());
      ctx.connection(Connection::commit);

      /*
       * Start the "refresh materialized view" thread
       */
      new RefreshThread().start();
    }

    return "/index?faces-redirect=true";
  }

  /** TODO: add javadoc. */
  public MdrNamespace createNewTargetNamespace(String name, String label, int id) {
    de.samply.mdr.dal.dto.Namespace namespace = new de.samply.mdr.dal.dto.Namespace();
    namespace.setCreatedBy(0);
    namespace.setData(new JSONObject());
    namespace.setElementType(Elementtype.NAMESPACE);
    namespace.setHidden(false);
    namespace.setId(0);
    namespace.setName(name);
    namespace.setUuid(UUID.randomUUID());

    Definition def = new Definition();
    def.setDefinition(label);
    def.setDesignation(label);
    def.setLanguage("en");
    List<Definition> definitions = new ArrayList<>();
    definitions.add(def);

    DescribedElement newNamespace = new DescribedElement();
    newNamespace.setDefinitions(definitions);
    newNamespace.setElement(namespace);
    return new MdrNamespace(newNamespace, userBean.getSelectedLanguage());
  }

  /** TODO: add javadoc. */
  public String reset() {
    file = null;
    export = null;

    return "/index.xhtml?faces-direct=true";
  }

  @Override
  public Boolean getReleaseDraftButtons() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public List<WizardStep> getSteps() {
    return steps;
  }

  public Part getFile() {
    return file;
  }

  public void setFile(Part file) {
    this.file = file;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  @Override
  public String getDesignation() {
    return "Upload file";
  }

  public Export getExport() {
    return export;
  }

  public void setExport(Export export) {
    this.export = export;
  }

  public List<ImportStrategy> getStrategy() {
    return strategy;
  }

  public void setStrategy(List<ImportStrategy> strategy) {
    this.strategy = strategy;
  }

  public List<MdrNamespace> getTargetNamespaces() {
    return targetNamespaces;
  }

  public void setTargetNamespaces(List<MdrNamespace> targetNamespaces) {
    this.targetNamespaces = targetNamespaces;
  }

  public List<Import> getImports() {
    return imports;
  }

  public void setImports(List<Import> imports) {
    this.imports = imports;
  }

  public Boolean getKeepIdentifier() {
    return keepIdentifier;
  }

  public void setKeepIdentifier(Boolean keepIdentifier) {
    this.keepIdentifier = keepIdentifier;
  }

  public ThreadExecutorBean getThreadExecutorBean() {
    return threadExecutorBean;
  }

  public void setThreadExecutorBean(ThreadExecutorBean threadExecutorBean) {
    this.threadExecutorBean = threadExecutorBean;
  }
}
