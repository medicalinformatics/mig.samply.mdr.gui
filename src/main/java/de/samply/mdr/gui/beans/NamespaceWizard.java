/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.Message;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;

/**
 * The wizard used to create a new namespace.
 *
 * @author paul
 */
@ManagedBean
@SessionScoped
public class NamespaceWizard extends ElementWizard {

  private static final long serialVersionUID = 1497295675591385802L;

  private Boolean hidden;

  private String name;

  @Override
  public String validateSlots() {
    return null;
  }

  @Override
  public String validateDefinitions() {
    name = createNewName();
    setStarted(true);
    return BeanValidators.validateDefinitions(getDefinitions())
        ? "namespaceverify?faces-redirect=true"
        : null;
  }

  /**
   * Creates a new identifiable name for this namespace. Tries to use the first designation as
   * template.
   */
  private String createNewName() {
    Pattern asciiPattern = Pattern.compile("^[a-z0-9-]+$");
    AdapterDefinition definition = getDefinitions().get(0);
    String input = definition.getDesignation().toLowerCase();

    Matcher matcher = asciiPattern.matcher(input);

    if (matcher.find()) {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        Namespace namespace = ElementDao.getNamespace(ctx, input);

        if (namespace == null) {
          return input;
        }

        int i = 0;
        while (namespace != null) {
          ++i;
          namespace = ElementDao.getNamespace(ctx, input + i);
        }

        return input + i;
      }
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      input = "mdr";
      Namespace namespace = ElementDao.getNamespace(ctx, input);

      if (namespace == null) {
        return input;
      }

      int i = 0;
      while (namespace != null) {
        ++i;
        namespace = ElementDao.getNamespace(ctx, input + i);
      }

      return input + i;
    }
  }

  @Override
  protected Elementtype getElementType() {
    return null;
  }

  @Override
  public List<WizardStep> getSteps() {
    return Arrays.asList(
        new WizardStep(JsfUtil.getString("definitions"), 1, "/user/namespacedefinitions.xhtml"),
        new WizardStep(JsfUtil.getString("verification"), 2, "/user/namespaceverify.xhtml"));
  }

  public Boolean getHidden() {
    return hidden;
  }

  public void setHidden(Boolean hidden) {
    this.hidden = hidden;
  }

  @Override
  public String reset() {
    super.reset();
    hidden = false;
    return "/index?faces-redirect=true";
  }

  /** TODO: add javadoc. */
  public String finish() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      // If the namespace already exists. Show an error message for now
      if (ElementDao.getNamespace(ctx, name) != null) {
        Message.addError("namespaceAlreadyExists");
        return null;
      }

      Namespace namespace = new Namespace();
      namespace.setCreatedBy(getUserBean().getUserId());
      namespace.setName(name);
      namespace.setHidden(hidden);

      ctx.connection(c -> c.setAutoCommit(false));
      ElementDao.saveElement(ctx, getUserBean().getUserId(), namespace);
      for (AdapterDefinition def : getDefinitions()) {
        Definition d = Mapper.convert(def);
        d.setElementId(namespace.getId());
        DefinitionDao.saveNamespaceDefinition(ctx, d);
      }
      ctx.connection(Connection::commit);
      getUserBean().reloadNamespaces();
    }

    reset();
    return "/index?faces-redirect=true";
  }

  @Override
  public Boolean getReleaseDraftButtons() {
    return false;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String template(String urn) {
    return null;
  }
}
