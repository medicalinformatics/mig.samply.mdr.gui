/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.utils.Importer;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.WizardStep;
import de.samply.mdr.gui.exceptions.DuplicateIdentifierException;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.util.JsfUtil;
import de.samply.mdr.util.Mapper;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

/**
 * The wizard used to create a new record (or use an existing record as a template) or change a
 * record in the first draft status.
 *
 * @author paul
 */
@ManagedBean
@SessionScoped
public class RecordWizard extends DataElementGroupWizard {

  private static final long serialVersionUID = 5319909503627713132L;

  @Override
  public String validateMembers() {
    return "/user/recordslots?faces-redirect=true";
  }

  @Override
  public String validateSlots() {
    return BeanValidators.validateSlots(getSlots())
        ? "/user/recordverify?faces-redirect=true"
        : null;
  }

  @Override
  public String validateDefinitions() {
    setStarted(true);
    return BeanValidators.validateDefinitions(getDefinitions())
        ? "/user/recordassignment?faces-redirect=true"
        : null;
  }

  @Override
  public String template(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      template(ctx, urn);
      setElements(
          Mapper.convertElements(
              IdentifiedDao.getEntries(ctx, getUserBean().getUserId(), urn),
              getUserBean().getSelectedLanguage(),
              false));
    } catch (DataAccessException e) {
      reset();
      return null;
    }

    return "/user/recorddefinitions?faces-redirect=true";
  }

  @Override
  protected boolean elementValid(MdrElement foundElement) {
    /** Records can contain dataelements only. */
    return foundElement.getElement().getElementType() == Elementtype.DATAELEMENT;
  }

  @Override
  public String finish() {
    String definitions = validateDefinitions();
    String slots = validateSlots();
    String members = validateMembers();

    if (definitions == null || slots == null || members == null) {
      return null;
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      Namespace namespace = ElementDao.getNamespace(ctx, getNamespace());

      Record record = new Record();
      ElementDao.saveElement(ctx, getUserBean().getUserId(), record);

      ScopedIdentifier superIdentifier = super.finish(ctx, record.getId(), namespace.getId());

      Importer importer = new Importer();

      int order = 1;
      for (MdrElement element : getElements()) {
        ScopedIdentifier sub =
            importer.importElement(
                ctx, element.getUrn(), namespace.getName(), getUserBean().getUserId());
        ScopedIdentifierHierarchyDao
            .addSubIdentifier(ctx, superIdentifier.getId(), sub.getId(), order++);
      }
      ctx.connection(Connection::commit);

      refreshViews();
      return done();
    } catch (DuplicateIdentifierException | ValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  protected Elementtype getElementType() {
    return Elementtype.RECORD;
  }

  @Override
  public List<WizardStep> getSteps() {
    return Arrays.asList(
        new WizardStep(JsfUtil.getString("definitions"), 1, "/user/recorddefinitions.xhtml"),
        new WizardStep(JsfUtil.getString("entries"), 2, "/user/recordassignment.xhtml"),
        new WizardStep(JsfUtil.getString("slots"), 3, "/user/recordslots.xhtml"),
        new WizardStep(JsfUtil.getString("verification"), 4, "/user/recordverify.xhtml"));
  }

  @Override
  public Boolean getDraftButtonOnly(String viewId) {
    return viewId.endsWith("recordverify.xhtml") && !allMembersReleasable();
  }
}
