/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierRelationDao;
import de.samply.mdr.dal.dao.ScopedIdentifierRelationDao.RelationDirection;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.tables.pojos.ScopedIdentifierRelation;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrElementRelation;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/** Searches for other representations of the given URN. */
@ViewScoped
@ManagedBean
public class RepresentationsBean implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The URN from the dataelement. */
  private String urn;

  /**
   * The current element.
   */
  private MdrElement element;

  /**
   * The data elements that are just other representations of the dataelement described by the URN.
   */
  private List<MdrElement> elements;

  /**
   * The data elements that are related to the dataelement described by the URN.
   * This includes the elements that have the selected element as target.
   */
  private List<MdrElementRelation> relatedElementsInbound;

  /**
   * The data elements that are related to the dataelement described by the URN.
   * This includes the elements that have the selected element as source.
   */
  private List<MdrElementRelation> relatedElementsOutbound;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** TODO: add javadoc. */
  public void initialize() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      relatedElementsInbound = new ArrayList<>();
      relatedElementsOutbound = new ArrayList<>();

      element = Mapper.convert(
          IdentifiedDao.getElement(ctx, userBean.getUserId(), urn),
          userBean.getSelectedLanguage()
      );

      elements =
          Mapper.convertElements(
              IdentifiedDao.getSimilarElements(ctx, userBean.getUserId(), urn),
              userBean.getSelectedLanguage(),
              true);

      elements.removeIf(el -> (el.getUrn().equals(element.getUrn())));

      List<ScopedIdentifierRelation> scopedIdentifierRelationsOutbound = ScopedIdentifierRelationDao
          .getRelations(ctx, userBean.getUserId(), urn, RelationDirection.SOURCE);

      for (ScopedIdentifierRelation relation : scopedIdentifierRelationsOutbound) {
        ScopedIdentifier rightIdentifier = ScopedIdentifierDao
            .getScopedIdentifier(ctx, relation.getRightIdentifierId());
        IdentifiedElement rightElement = IdentifiedDao
            .getElement(ctx, userBean.getUserId(), rightIdentifier.toString());

        MdrElement rightMdrElement = Mapper.convert(rightElement, userBean.getSelectedLanguage());
        MdrElementRelation mdrElementRelation = new MdrElementRelation(rightMdrElement,
            relation.getRelation(), RelationDirection.SOURCE);
        relatedElementsOutbound.add(mdrElementRelation);
      }

      List<ScopedIdentifierRelation> scopedIdentifierRelationsInbound = ScopedIdentifierRelationDao
          .getRelations(ctx, userBean.getUserId(), urn, RelationDirection.TARGET);

      for (ScopedIdentifierRelation relation : scopedIdentifierRelationsInbound) {
        ScopedIdentifier leftIdentifier = ScopedIdentifierDao
            .getScopedIdentifier(ctx, relation.getLeftIdentifierId());

        IdentifiedElement leftElement = IdentifiedDao
            .getElement(ctx, userBean.getUserId(), leftIdentifier.toString());

        MdrElement leftMdrElement = Mapper.convert(leftElement, userBean.getSelectedLanguage());
        MdrElementRelation mdrElementRelation = new MdrElementRelation(leftMdrElement,
            relation.getRelation(), RelationDirection.TARGET);
        relatedElementsInbound.add(mdrElementRelation);
      }
    }
  }

  public String getUrn() {
    return urn;
  }

  public void setUrn(String urn) {
    this.urn = urn;
  }

  public MdrElement getElement() {
    return element;
  }

  public void setElement(MdrElement element) {
    this.element = element;
  }

  public List<MdrElement> getElements() {
    return elements;
  }

  public void setElements(List<MdrElement> elements) {
    this.elements = elements;
  }

  public List<MdrElementRelation> getRelatedElementsInbound() {
    return relatedElementsInbound;
  }

  public void setRelatedElementsInbound(
      List<MdrElementRelation> relatedElementsInbound) {
    this.relatedElementsInbound = relatedElementsInbound;
  }

  public List<MdrElementRelation> getRelatedElementsOutbound() {
    return relatedElementsOutbound;
  }

  public void setRelatedElementsOutbound(
      List<MdrElementRelation> relatedElementsOutbound) {
    this.relatedElementsOutbound = relatedElementsOutbound;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }
}
