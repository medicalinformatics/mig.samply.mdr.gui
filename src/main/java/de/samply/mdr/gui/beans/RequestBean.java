/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/** The request scoped bean that checks if the server name contains the text "osse". */
@ManagedBean(name = "requestBean")
@RequestScoped
public class RequestBean implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final String OSSE_TITLE = "OSSE";
  private static final String SAMPLY_TITLE = "Samply";

  /**
   * Returns the server name from the request.
   *
   * @return a {@link java.lang.String} object.
   */
  public String getServerName() {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
  }

  /**
   * Returns the title the web page should show. If the server contains the string "osse", it shows
   * "OSSE", otherwise "Samply" is shown.
   *
   * @return a {@link java.lang.String} object.
   */
  public String getTitle() {
    if (isOsse()) {
      return OSSE_TITLE;
    } else {
      return SAMPLY_TITLE;
    }
  }

  /**
   * If true, this request was made for an OSSE domain.
   *
   * @return a {@link java.lang.Boolean} object.
   */
  public Boolean isOsse() {
    return getServerName().toLowerCase().contains("osse-register")
        || System.getProperty("de.samply.osseDevelopment") != null;
  }

  /**
   * If true, this server is running in a productive environment.
   *
   * @return a {@link java.lang.Boolean} object.
   */
  public Boolean isProduction() {
    return System.getProperty("de.samply.development", "false").equalsIgnoreCase("false");
  }
}
