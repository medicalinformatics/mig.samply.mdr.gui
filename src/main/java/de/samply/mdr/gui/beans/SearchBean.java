/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.cadsr.CadsrClient;
import de.samply.cadsr.CadsrSearch;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.lib.Constants.Source;
import de.samply.mdr.util.Mapper;
import de.samply.string.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.ClientBuilder;
import org.jooq.DSLContext;

/**
 * The search bean searches for elements that contain a specific text in their definition or
 * designation.
 *
 * @author paul
 */
@ManagedBean
@ViewScoped
public class SearchBean implements Serializable {

  private static final long serialVersionUID = 1L;

  /** The search test. Definitions and designations will be searched for this string. */
  private String searchText;

  /** The currently selected element types that will be searched for. */
  private Elementtype[] selectedTypes =
      new Elementtype[] {Elementtype.DATAELEMENT, Elementtype.DATAELEMENTGROUP, Elementtype.RECORD};

  /** The currently selected statuses that will be searched for. */
  private Status[] selectedStatuses = new Status[] {Status.RELEASED};

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** A list of all available namespaces with results, excluding the writable namespaces. */
  private List<MdrNamespace> namespaces;

  /** The currently selected source. */
  private Source source = Source.LOCALMDR;

  /** A list of all writable namespaces with results. */
  private List<MdrNamespace> myNamespaces;

  /** A list of all namespaces combined. */
  private List<MdrNamespace> allNamespaces;

  /** The list of all available results. */
  private List<MdrElement> results = new ArrayList<>();

  /** The current search in the CaDSR. */
  private CadsrSearch cadsr = new CadsrSearch();

  /** The list of all currently selected elements. It is a subset of results. */
  private List<MdrElement> activeElements;

  /** The currently selected namespace. */
  private String selectedNamespace = null;

  /** The type of this search. */
  private SearchType searchType = SearchType.ELEMENTS;

  /** Searches for elements and updated the results array. */
  public String updateResults() {
    /*
     * If the search text is a valid urn, show him the details page immediately.
     */
    if (ScopedIdentifier.isUrn(searchText)) {
      return "/detail?faces-redirect=true&urn=" + searchText;
    } else if (searchType == SearchType.ELEMENTS) {
      /*
       * Search for elements with appropriate filters.
       */
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        results =
            Mapper.convertElements(
                IdentifiedDao
                    .findElements(
                        ctx,
                        userBean.getUserId(),
                        searchText,
                        null,
                        selectedTypes,
                        selectedStatuses,
                        new HashMap<>()),
                getUserBean().getSelectedLanguage(),
                true);

        selectedNamespace = null;

        updateElements();

        /*
         * Load all namespaces. This is inefficient, but currently there is no other way to do it.
         */
        HashSet<String> loaded = new HashSet<>();

        namespaces = new ArrayList<>();
        myNamespaces = new ArrayList<>();
        allNamespaces = new ArrayList<>();

        /*
         * TODO: Those queries are inefficient...
         */
        List<MdrNamespace> writable = getUserBean().getWritableNamespaces();

        /*
         * For each element, check if the namespace has been loaded already, and if it hasn't,
         * load it.
         */
        for (MdrElement element : results) {
          String namespace = element.getElement().getScoped().getNamespace();
          if (!loaded.contains(namespace)) {
            MdrNamespace ns = Mapper.convertNamespace(
                NamespaceDao.getNamespace(ctx, userBean.getUserId(), namespace),
                userBean.getSelectedLanguage());

            if (writable.contains(ns)) {
              myNamespaces.add(ns);
            } else {
              namespaces.add(ns);
            }
            allNamespaces.add(ns);
            loaded.add(namespace);
          }
        }
      }
    } else if (searchType == SearchType.NAMESPACES) {
      /*
       * Search for namespaces.
       */
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        namespaces = Mapper.convertNamespaces(
            NamespaceDao.findNamespaces(ctx, userBean.getUserId(), searchText),
            userBean.getSelectedLanguage());
        activeElements = Collections.emptyList();
      }
    }

    Collections.sort(namespaces, MdrNamespace.nameComparator);
    return null;
  }

  /** Updates the list of the currently selected elements. */
  public String updateElements() {
    if (searchType == SearchType.ELEMENTS) {
      /*
       * If the user selected "all namespaces", just show the results.
       */
      if (StringUtil.isEmpty(selectedNamespace)) {
        activeElements = results;
      } else {
        /*
         * Otherwise search for elements in the result set, that are in the selected namespace.
         */
        try (DSLContext ctx = ResourceManager.getDslContext()) {
          MdrNamespace activeNamespace = Mapper.convertNamespace(
              NamespaceDao.getNamespace(ctx, userBean.getUserId(), selectedNamespace),
              userBean.getSelectedLanguage());

          activeElements = new ArrayList<>();

          for (MdrElement element : results) {
            if (Objects.equals(element.getElement().getScoped().getNamespaceId(),
                activeNamespace.getId())) {
              activeElements.add(element);
            }
          }
        }
      }
    } else if (searchType == SearchType.NAMESPACES) {
      try (DSLContext ctx = ResourceManager.getDslContext()) {
        activeElements =
            Mapper.convertElements(
                IdentifiedDao.getRootElements(ctx, userBean.getUserId(), selectedNamespace),
                userBean.getSelectedLanguage(),
                true);
      }
    }
    return null;
  }

  /** TODO: add javadoc. */
  public String updateCadsrResults() {
    CadsrClient client = new CadsrClient(ClientBuilder.newClient());
    try {
      cadsr = client.search(searchText);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /** TODO: add javadoc. */
  public String loadNext() {
    CadsrClient client = new CadsrClient(ClientBuilder.newClient());
    try {
      client.loadNext(cadsr);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public String getSearchText() {
    return searchText;
  }

  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }

  public List<MdrElement> getResults() {
    return results;
  }

  public void setResults(List<MdrElement> results) {
    this.results = results;
  }

  public Status[] getStatuses() {
    return Status.values();
  }

  public Elementtype[] getSelectedTypes() {
    return selectedTypes;
  }

  public void setSelectedTypes(Elementtype[] selectedTypes) {
    this.selectedTypes = selectedTypes;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public Status[] getSelectedStatuses() {
    return selectedStatuses;
  }

  public void setSelectedStatuses(Status[] selectedStatuses) {
    this.selectedStatuses = selectedStatuses;
  }

  public Source getSource() {
    return source;
  }

  public void setSource(Source source) {
    this.source = source;
  }

  public CadsrSearch getCadsr() {
    return cadsr;
  }

  public void setCadsr(CadsrSearch cadsr) {
    this.cadsr = cadsr;
  }

  public List<MdrElement> getActiveElements() {
    return activeElements;
  }

  public void setActiveElements(List<MdrElement> activeElements) {
    this.activeElements = activeElements;
  }

  public List<MdrNamespace> getNamespaces() {
    return namespaces;
  }

  public void setNamespaces(List<MdrNamespace> namespaces) {
    this.namespaces = namespaces;
  }

  public SearchType getSearchType() {
    return searchType;
  }

  public void setSearchType(SearchType searchType) {
    this.searchType = searchType;
  }

  public SearchType[] getSearchTypes() {
    return SearchType.values();
  }

  /** Returns an array of *searchable* element types. */
  public Elementtype[] getTypes() {
    return new Elementtype[] {
      Elementtype.DATAELEMENT, Elementtype.DATAELEMENTGROUP, Elementtype.RECORD, Elementtype.CATALOG
    };
  }

  public String getSelectedNamespace() {
    return selectedNamespace;
  }

  public void setSelectedNamespace(String selectedNamespace) {
    this.selectedNamespace = selectedNamespace;
  }

  public List<MdrNamespace> getMyNamespaces() {
    return myNamespaces;
  }

  public void setMyNamespaces(List<MdrNamespace> myNamespaces) {
    this.myNamespaces = myNamespaces;
  }

  public List<MdrNamespace> getAllNamespaces() {
    return allNamespaces;
  }

  public void setAllNamespaces(List<MdrNamespace> allNamespaces) {
    this.allNamespaces = allNamespaces;
  }

  public enum SearchType {
    ELEMENTS,
    NAMESPACES
  }
}
