/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui.beans;

import static de.samply.mdr.dal.jooq.Tables.USER_SOURCE_CREDENTIALS;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.jooq.Tables;
import de.samply.mdr.dal.jooq.tables.pojos.Source;
import de.samply.mdr.dal.jooq.tables.pojos.UserSourceCredentials;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.jooq.DSLContext;


@ManagedBean
@SessionScoped
public class SettingsBean {

  //TODO: for multiple Sources
  //private List<Source.Credential> conceptCredentials = new ArrayList<>();
  //private List<Source> source = new ArrayList<>();

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;
  private List<SourceCredential> credentials = new ArrayList<>();
  private List<Source> sources = new ArrayList<>();

  /**
   *  Initializes the settings page and loads sources and credentials from db.
   */
  public void initialize() {
    credentials.clear();
    sources.clear();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      sources = ctx.fetch(
          ctx.select().from(Tables.SOURCE).where(Tables.SOURCE.NAME.notContains("Samply MDR")))
          .into(Source.class);

      List<UserSourceCredentials> userSourceCredentials =
          ctx.fetch(ctx.select()
              .from(Tables.USER_SOURCE_CREDENTIALS)
              .where(Tables.USER_SOURCE_CREDENTIALS.USER_ID.eq(userBean.getUserId())))
              .into(UserSourceCredentials.class);

      mapCredentialsToSources(userSourceCredentials);

    }
  }

  /**
   *  maps the credentials to the sources.
   */
  public List<SourceCredential> mapCredentialsToSources(
      List<UserSourceCredentials> userSourceCredentials) {
    for (Source s : sources) {
      SourceCredential sourceCredential = new SourceCredential();
      sourceCredential.setSourceName(s.getName());
      if (userSourceCredentials != null && !userSourceCredentials.isEmpty()) {
        for (UserSourceCredentials usc : userSourceCredentials) {
          if (usc.getSourceId().equals(s.getId())) {
            sourceCredential.setCredential(usc.getCredential());
          }
        }
      } else {
        sourceCredential.setCredential(null);
      }
      credentials.add(sourceCredential);
    }
    return credentials;
  }

  /**
   *  saves credentials to the database.
   */
  public void saveSourceCredential() {
    List<UserSourceCredentials> userSourceCredentialsList = new ArrayList<>();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (SourceCredential sc : credentials) {
        UserSourceCredentials userSourceCredentials = new UserSourceCredentials();
        userSourceCredentials.setCredential(sc.getCredential());
        for (Source s : sources) {
          if (s.getName().equals(sc.getSourceName())) {
            userSourceCredentials.setSourceId(s.getId());
          }
        }

        //TODO: set userId to 0 if credential is an instance credential
        userSourceCredentials.setUserId(userBean.getUserId());

        ctx.insertInto(USER_SOURCE_CREDENTIALS, USER_SOURCE_CREDENTIALS.SOURCE_ID,
            USER_SOURCE_CREDENTIALS.CREDENTIAL, USER_SOURCE_CREDENTIALS.USER_ID)
            .values(userSourceCredentials.getSourceId(), userSourceCredentials.getCredential(),
                userSourceCredentials.getUserId()).onDuplicateKeyUpdate()
            .set(USER_SOURCE_CREDENTIALS.CREDENTIAL, userSourceCredentials.getCredential())
            .execute();
      }

    }
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public List<Source> getSources() {
    return sources;
  }

  public void setSources(List<Source> sources) {
    this.sources = sources;
  }

  public List<SourceCredential> getCredentials() {
    return credentials;
  }

  public void setCredentials(List<SourceCredential> credentials) {
    this.credentials = credentials;
  }

  public class SourceCredential {

    String sourceName;
    String credential;

    public String getSourceName() {
      return sourceName;
    }

    public void setSourceName(String sourceName) {
      this.sourceName = sourceName;
    }

    public String getCredential() {
      return credential;
    }

    public void setCredential(String credential) {
      this.credential = credential;
    }
  }
}