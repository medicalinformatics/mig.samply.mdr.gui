/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui.beans;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ImportDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.staging.model.adapter.ValidationTypeAdapter;
import de.samply.mdr.xsd.staging.DefinitionType;
import de.samply.mdr.xsd.staging.StagedElementType;
import de.samply.mdr.xsd.staging.ValidationType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

@ManagedBean
@ViewScoped
public class StagingBean implements Serializable {

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{threadExecutor}")
  private ThreadExecutorBean threadExecutorBean;

  private Gson gson;

  private List<String> namespaces;

  /** the list of elements that have been marked for import. */
  private List<String> selectedItems = new ArrayList<>();

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public ThreadExecutorBean getThreadExecutorBean() {
    return threadExecutorBean;
  }

  public void setThreadExecutorBean(ThreadExecutorBean threadExecutorBean) {
    this.threadExecutorBean = threadExecutorBean;
  }

  public List<String> getNamespaces() {
    return namespaces;
  }

  public void setNamespaces(List<String> namespaces) {
    this.namespaces = namespaces;
  }

  public List<String> getSelectedItems() {
    return selectedItems;
  }

  public void setSelectedItems(List<String> selectedItems) {
    this.selectedItems = selectedItems;
  }

  public Gson getGson() {
    return gson;
  }

  /** TODO: add javadoc. */
  @PostConstruct
  public void initialize() {
    selectedItems = new ArrayList<>();
    initGson();
    loadNamespaces();
  }

  private void initGson() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeHierarchyAdapter(ValidationType.class, new ValidationTypeAdapter());
    gsonBuilder.disableHtmlEscaping();
    gson = gsonBuilder.create();
  }

  private void loadNamespaces() {
    namespaces = new ArrayList<>();
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<Integer> namespaceIds = ImportDao.getNamespaceIdsForUser(ctx, userBean.getUserId());
      for (int id : namespaceIds) {
        DescribedElement namespace = NamespaceDao.getNamespaceById(ctx, userBean.getUserId(), id);
        namespaces.add(((Namespace) namespace.getElement()).getName());
      }
    }
  }

  public boolean isEmpty() {
    return namespaces == null || namespaces.isEmpty();
  }

  /** TODO: deal with prioritized languages (like in MdrElement.java) */
  public String getDefinition(Staging stagedElement) {
    try {
      StagedElementType dataelement =
          gson.fromJson(stagedElement.getData(), StagedElementType.class);
      for (DefinitionType definition : dataelement.getDefinitions().getDefinition()) {
        if (stagedElement.getDesignation().equalsIgnoreCase(definition.getDesignation())) {
          return definition.getDefinition();
        }
      }
      return dataelement.getDefinitions().getDefinition().get(0).getDefinition();
    } catch (Exception e) {
      return "";
    }
  }

  public StagedElementType convertDataElement(Staging stagedElement) {
    return gson.fromJson(stagedElement.getData(), StagedElementType.class);
  }

  private List<Integer> convertIdToInteger() {
    List<Integer> ids = new ArrayList<>();
    for (String idString : selectedItems) {
      try {
        ids.add(Integer.parseInt(idString));
      } catch (NumberFormatException e) {
        // If namespaces are contained in the id list...simply skip them
        continue;
      }
    }
    return ids;
  }
}
