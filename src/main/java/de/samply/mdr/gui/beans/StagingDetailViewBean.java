/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui.beans;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ImportDao;
import de.samply.mdr.dal.dao.StagingDao;
import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.job.StagingToElementJob;
import de.samply.mdr.staging.utils.ImportToStagingConverter;
import de.samply.mdr.util.Mapper;
import de.samply.mdr.util.StagingUtil;
import de.samply.mdr.xsd.staging.DefinitionType;
import de.samply.mdr.xsd.staging.StagedElementType;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * Handles the detailed view of a staged element.
 *
 * @author michael
 */
@ViewScoped
@ManagedBean
public class StagingDetailViewBean implements Serializable {

  /** The db id of the staged element. */
  private String id;

  /** The full staged element. */
  private Staging stagedElement;

  private StagedElementType dataelement;

  private Import selectedImport;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{stagingBean}")
  private StagingBean stagingBean;

  @ManagedProperty(value = "#{dataElementWizard}")
  private DataElementWizard dataElementWizard;

  private String importIntoNamespace;

  private Boolean isValid;

  private String node;

  private String ancestorsString;

  private List<JsonObject> ancestors;

  private MdrElement createdElement;

  /** TODO: add javadoc. */
  public static String getIdFromJsonObject(JsonObject jsonObject) {
    try {
      return jsonObject.getAsJsonPrimitive("id").getAsString();
    } catch (Exception e) {
      return "error";
    }
  }

  /** TODO: add javadoc. */
  public static String getTextFromJsonObject(JsonObject jsonObject) {
    try {
      return jsonObject.getAsJsonPrimitive("text").getAsString();
    } catch (Exception e) {
      return "error";
    }
  }

  /** Called by the f:viewAction. */
  public void initialize() {
    JsonArray entries;
    ancestors = new ArrayList<>();

    try {
      entries = (JsonArray) new JsonParser().parse(URLDecoder.decode(ancestorsString, "UTF-8"));
      for (JsonElement entry : entries) {
        ancestors.add(entry.getAsJsonObject());
      }
      Collections.reverse(ancestors);
    } catch (UnsupportedEncodingException e) {
      ancestors = new ArrayList<>();
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      if (id.startsWith("im_")) {
        dataelement = null;
        int importId = Integer.parseInt(this.id.substring(3));
        selectedImport = ImportDao.getImportById(ctx, importId);
      } else {
        selectedImport = null;
        stagedElement = StagingDao.getStagedElementById(ctx, Integer.valueOf(id));
        switch (stagedElement.getElementType()) {
          case DATAELEMENTGROUP:
          case DATAELEMENT:
          case RECORD:
            dataelement =
                stagingBean.getGson().fromJson(stagedElement.getData(), StagedElementType.class);
            break;
          case CATALOG:
          case CODE:
          case NAMESPACE:
            // Not supported at the moment
            break;
          case ENUMERATED_VALUE_DOMAIN:
          case DESCRIBED_VALUE_DOMAIN:
          case CATALOG_VALUE_DOMAIN:
          case PERMISSIBLE_VALUE:
          default:
            // Not going to be supported
            break;
        }
        if (stagedElement.getElementId() != null && stagedElement.getElementId() > 0) {
          createdElement = Mapper.convert(
              IdentifiedDao.getElementById(ctx, userBean.getUserId(), stagedElement.getElementId()),
              userBean.getSelectedLanguage());
        }
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  /**
   * Delete the selected import.
   *
   * @return redirect url to the same page
   */
  public String delete() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ImportDao.deleteImport(ctx, selectedImport);
    }

    return "/user/staging.xhtml?faces-redirect=true";
  }

  /**
   * Convert all elements of the selected import to drafts.
   *
   * @return redirect url to conversion information page
   */
  public String toDraft() {
    List<Staging> elements;
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      elements = StagingDao.getStagedElementsByImportId(ctx, selectedImport.getId());
    }
    List<Integer> ids = new ArrayList<>();
    for (Staging element : elements) {
      ids.add(element.getId());
    }

    stagingBean
        .getThreadExecutorBean()
        .spawnJob(new StagingToElementJob(ids, userBean.getUserId()));

    selectedImport.setConvertedAt(new Timestamp(new java.util.Date().getTime()));
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      ImportDao.updateImport(ctx, selectedImport);
      ctx.connection(Connection::commit);
    }

    return "/user/stagingtodraft.xhtml?faces-redirect=true";
  }

  public String getSelectedImportDisplayname() {
    return StagingUtil.getImportDisplayname(selectedImport);
  }

  public Staging getStagedElement() {
    return stagedElement;
  }

  public void setStagedElement(Staging stagedElement) {
    this.stagedElement = stagedElement;
  }

  public StagedElementType getDataelement() {
    return dataelement;
  }

  public void setDataelement(StagedElementType dataelement) {
    this.dataelement = dataelement;
  }

  public DataElementWizard getDataElementWizard() {
    return dataElementWizard;
  }

  public void setDataElementWizard(DataElementWizard dataElementWizard) {
    this.dataElementWizard = dataElementWizard;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public StagingBean getStagingBean() {
    return stagingBean;
  }

  public void setStagingBean(StagingBean stagingBean) {
    this.stagingBean = stagingBean;
  }

  public String getImportIntoNamespace() {
    return importIntoNamespace;
  }

  public void setImportIntoNamespace(String importIntoNamespace) {
    this.importIntoNamespace = importIntoNamespace;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Boolean getIsValid() {
    return isValid;
  }

  public void setIsValid(Boolean isValid) {
    this.isValid = isValid;
  }

  public String getNode() {
    return node;
  }

  public void setNode(String node) {
    this.node = node;
  }

  public String getAncestorsString() {
    return ancestorsString;
  }

  public void setAncestorsString(String ancestorsString) {
    this.ancestorsString = ancestorsString;
  }

  public List<JsonObject> getAncestors() {
    return ancestors;
  }

  public void setAncestors(List<JsonObject> ancestors) {
    this.ancestors = ancestors;
  }

  public DefinitionType getPrioritizedDefinition() {
    return ImportToStagingConverter.getPrioritizedDefinition(
        dataelement, getUserBean().getSelectedLanguage());
  }

  public MdrElement getCreatedElement() {
    return createdElement;
  }

  public void setCreatedElement(MdrElement createdElement) {
    this.createdElement = createdElement;
  }

  public Import getSelectedImport() {
    return selectedImport;
  }

  public void setSelectedImport(Import selectedImport) {
    this.selectedImport = selectedImport;
  }
}
