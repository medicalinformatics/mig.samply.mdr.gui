/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.gui.MdrElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TreeNodes are used for catalogs. They have a state for itself, the state of all children, and a
 * reference to the parents and children of this node.
 */
public class TreeNode implements Serializable {

  private static final long serialVersionUID = 1211478552020330315L;
  /** The MDR element (Code) that this node describes. */
  private final MdrElement element;

  /** The parents of this node. */
  private final List<TreeNode> parents = new ArrayList<>();
  /** The children of this node. */
  private final List<TreeNode> children = new ArrayList<>();
  /** If true, this node itself is selected as valid code. */
  private boolean isSelected = true;
  /** The state of this node. */
  private TreeNode.ChildrenSelectionState state = ChildrenSelectionState.ALL;

  public TreeNode(MdrElement element) {
    this.element = element;
  }

  public MdrElement getElement() {
    return element;
  }

  public List<TreeNode> getChildren() {
    return children;
  }

  public List<TreeNode> getParents() {
    return parents;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean isSelected) {
    this.isSelected = isSelected;
  }

  public TreeNode.ChildrenSelectionState getState() {
    return state;
  }

  public void setState(TreeNode.ChildrenSelectionState state) {
    this.state = state;
  }

  /**
   * TODO: add javadoc.
   *
   * <pre>
   * ALL: the code itself and *all* children are selected
   * SOME: the code itself or at least one child is selected
   * NONE: the code itself nor any child is selected
   * </pre>
   */
  public enum ChildrenSelectionState {
    ALL,
    SOME,
    NONE
  }
}
