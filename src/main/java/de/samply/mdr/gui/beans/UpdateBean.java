/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.utils.Updater;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.util.Mapper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/** A bean that shows the update process of a single group. */
@ManagedBean
@ViewScoped
public class UpdateBean implements Serializable {

  private static final long serialVersionUID = 1L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  /** The URN that will be updated with this bean. */
  private String urn;

  /** The replacement map. */
  private Map<MdrElement, MdrElement> replacements;

  /** The list of updated elements. */
  private List<MdrElement> keys;

  /** The element that will be updated with this bean. */
  private MdrElement element;

  /**
   * Called by an ajax request. It *initializes* the maps, but does not commit the SQL connection,
   * so this process does not update the database in any way. The maps are used to show the user
   * exactly what will be updated by this process.
   */
  public String initialize() {
    if (!userBean.isWritableNamespaceUrn(urn)) {
      return "/detail?faces-redirect=true&urn=" + urn;
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      element = Mapper.convert(
          IdentifiedDao.getElement(ctx, userBean.getUserId(), urn), userBean.getSelectedLanguage());

      Updater updater = new Updater(urn, userBean.getUserId());
      updater.update(ctx);

      this.replacements = new HashMap<>();

      Map<IdentifiedElement, IdentifiedElement> replacements = updater.getRootReplacements();

      for (Entry<IdentifiedElement, IdentifiedElement> entry : replacements.entrySet()) {
        if (entry.getValue() == null) {
          this.replacements.put(
              Mapper.convert(entry.getKey(), userBean.getSelectedLanguage()), null);
        } else {
          this.replacements.put(
              Mapper.convert(entry.getKey(), userBean.getSelectedLanguage()),
              Mapper.convert(entry.getValue(), userBean.getSelectedLanguage()));
        }
      }
      keys = new ArrayList<>();
      keys.addAll(this.replacements.keySet());
      // DO NOT COMMIT HERE, THIS IS JUST A PREVIEW!
    }
    return null;
  }

  /** Executes the real update and redirects the user to the updated element. detail page. */
  public String update() {
    if (!userBean.isWritableNamespaceUrn(urn)) {
      return "/detail?faces-redirect=true&urn=" + urn;
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      Updater updater = new Updater(urn, userBean.getUserId());
      IdentifiedElement newElement = updater.update(ctx);
      return "/detail?faces-redirect=true&urn=" + newElement.getScoped().toString();
    }
  }

  /** Returns the status of the given element. */
  public String status(MdrElement element) {
    if (!keys.contains(element)) {
      /*
       * FYI: this doesnt make sense...
       */
      return "warning";
    } else if (isDeleted(element)) {
      return "danger";
    } else {
      return "success";
    }
  }

  /** Returns true if the element will be removed from the group. */
  public Boolean isDeleted(MdrElement element) {
    return keys.contains(element) && replacements.get(element) == null;
  }

  public String getUrn() {
    return urn;
  }

  public void setUrn(String urn) {
    this.urn = urn;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public Map<MdrElement, MdrElement> getReplacements() {
    return replacements;
  }

  public void setReplacements(Map<MdrElement, MdrElement> replacements) {
    this.replacements = replacements;
  }

  public List<MdrElement> getKeys() {
    return keys;
  }

  public void setKeys(List<MdrElement> keys) {
    this.keys = keys;
  }

  public MdrElement getElement() {
    return element;
  }

  public void setElement(MdrElement element) {
    this.element = element;
  }
}
