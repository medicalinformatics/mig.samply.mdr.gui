/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.auth.client.AuthClient;
import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.KeycloakAuthClient;
import de.samply.auth.client.KeycloakUsertype;
import de.samply.auth.client.SamplyAuthClient;
import de.samply.auth.client.jwt.JwtAccessToken;
import de.samply.auth.client.jwt.JwtIdToken;
import de.samply.auth.client.jwt.JwtRefreshToken;
import de.samply.auth.client.jwt.JwtVocabulary;
import de.samply.auth.rest.Scope;
import de.samply.auth.rest.Usertype;
import de.samply.auth.utils.OAuth2ClientConfig;
import de.samply.common.config.OAuth2Client;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ConceptAssociationDao;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dao.UserDao;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.gui.ClientUtil;
import de.samply.mdr.gui.DetailedMdrElement;
import de.samply.mdr.gui.MdrConfig;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import de.samply.string.util.StringUtil;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.jooq.DSLContext;
import org.jooq.exception.TooManyRowsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This session scoped bean stores all user information.
 *
 * <pre>
 * - his id in the database
 * - his username and email
 * - the language he selected, currently always german
 * - the namespaces that he can read and write
 * - the elements that he starred or wants to compare
 * </pre>
 *
 * @author paul
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

  private static final long serialVersionUID = 1L;
  private static final Logger logger = LoggerFactory.getLogger(UserBean.class);

  private static final String ROLE_NAME_ADMIN = "admin";
  private static final String ROLE_NAME_CREATE_NAMESPACE = "createNamespace";
  private static final String ROLE_NAME_CREATE_CATALOG = "createCatalog";
  private static final String ROLE_NAME_IMPORT_EXPORT = "importExport";
  private static final String ROLE_NAME_LINK_ELEMENTS = "linkElements";

  /** Compares two namespaces by their names. */
  private static final Comparator<MdrNamespace> namespaceComparator =
      new Comparator<MdrNamespace>() {
        @Override
        public int compare(MdrNamespace o1, MdrNamespace o2) {
          return o1.getName().compareToIgnoreCase(o2.getName());
        }
      };

  /** The current username (email). Null if the login is not valid */
  private String username = null;

  /** The current real name. Null if the login is not valid. */
  private String realName = null;

  /**
   * The current user identity (usually a URL that identifies the user). Null if the login is not
   * valid.
   */
  private String userIdentity = null;

  /** If the login is valid this value is true. False otherwise. */
  private Boolean loginValid = false;

  /** The *mapped* user ID in the database. */
  private int userId = 0;

  /** The currently selected language. */
  private Language selectedLanguage = Language.DE;

  /** The currently starred MDR elements. */
  private LinkedList<MdrElement> starred = new LinkedList<>();

  /**
   * The currently selected elements, that the user wants to compare. Usually this is a list of
   * groups.
   */
  private LinkedList<MdrElement> compare = new LinkedList<>();

  /** The currently for export selected elements. */
  private ArrayList<MdrElement> export = new ArrayList<>();

  /** The currently for export selected namespaces. */
  private ArrayList<String> exportNamespace = new ArrayList<>();

  /** The list of namespaces the user has admin rights on. */
  private List<MdrNamespace> adminRightNamespaces = new ArrayList<>();

  /** The list of currently writable namespaces. */
  private List<MdrNamespace> writableNamespaces = new ArrayList<>();

  /** The list of currently readable namespaces. */
  private List<MdrNamespace> readableNamespaces = new ArrayList<>();

  /**
   * The list of currently readable namespaces, that are not explicitly readable (e.g. they are
   * public)
   */
  private List<MdrNamespace> otherReadableNamespaces;

  /** The URN of the item that is about to get starred. */
  private String starItem;

  /** If true the current user is able to create new namespaces. */
  private Boolean canCreateNamespace;

  /** If the the current user is able to create new namespaces. */
  private Boolean canCreateCatalogs;

  /** If true the user can use the export and import. */
  private Boolean canExportImport;

  /** If true the user can create relations between elements. */
  private Boolean canLinkElements;

  /** If true, the user is allowed to access the admin area. */
  private Boolean hasAdminPrivilege = false;

  /** The currently selected namespace for this user. */
  private MdrNamespace selectedNamespace = null;

  /**
   * The state of this session. This is a random string for OAuth2 used to prevent cross site
   * forgery attacks.
   */
  private String state;

  /** The Jwts from OAuth2. */
  private JwtAccessToken accessToken;

  /** The Jwt Open ID token. */
  private JwtIdToken idToken;

  /** The Jwt refresh token. */
  private JwtRefreshToken refreshToken;

  /** Initializes the bean: loads all accessible namespaces. */
  public void init() {
    state = new BigInteger(64, new SecureRandom()).toString(32);

    reloadNamespaces();

    try {
      selectedLanguage =
          Language.valueOf(
              FacesContext.getCurrentInstance()
                  .getExternalContext()
                  .getRequestLocale()
                  .getLanguage()
                  .toUpperCase());
    } catch (Exception e) {
      selectedLanguage = Language.EN;
    }
  }

  /** Returns true, if this element is currently starred. */
  public Boolean isStarred(MdrElement element) {
    return starred.contains(element);
  }

  /** Toggles the starred element. */
  public String toggle(MdrElement element) {
    if (starred.contains(element)) {
      starred.remove(element);
    } else {
      starred.addFirst(element);
    }
    return null;
  }

  /** Toggles the star on the "starItem". */
  public String toggle() {
    Iterator<MdrElement> iterator = starred.iterator();

    while (iterator.hasNext()) {
      MdrElement element = iterator.next();
      if (element.getUrn().equals(starItem)) {
        iterator.remove();
        return null;
      }
    }

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      starred.addFirst(
          Mapper.convert(IdentifiedDao.getElement(ctx, userId, starItem), getSelectedLanguage()));
    }

    return null;
  }

  /** Stars the given element. */
  public void star(MdrElement element) {
    /*
     * Make sure that all other elements with the same URN are unstarred
     * This happens when drafts are saved
     */
    Iterator<MdrElement> iterator = starred.iterator();
    while (iterator.hasNext()) {
      if (iterator.next().getUrn().equals(element.getUrn())) {
        iterator.remove();
      }
    }

    starred.addFirst(element);
  }

  /**
   * Gets the fully described element from the database. With slots, its members, all available
   * versions
   */
  public DetailedMdrElement getDetailMdrElement(String urn) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return Mapper.convert(
          ctx,
          userId,
          IdentifiedDao.getElement(ctx, userId, urn),
          Mapper.convertSlots(SlotDao.getSlots(ctx, urn)),
          ScopedIdentifierDao.getVersions(ctx, urn),
          getSelectedLanguage(),
          ConceptAssociationDao.getConceptAssociations(ctx, urn,userId));
    }
  }

  /** Outdates or deletes the given element.
   *  Depending on the status of the element. Drafts are deleted, released elements are outdated.
   */
  public String deleteElement(MdrElement element) {
    if (!isWritableNamespaceUrn(element.getUrn())) {
      return null;
    }

    String redirectUrn = "detail.xhtml?urn=" + element.getUrn() + "&faces-redirect=true";

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      if (element.getStatus() == Status.DRAFT) {
        if (element.firstDraft()) {
          // If no released version exists - redirect to the drafts page
          redirectUrn = "user/mydrafts.xhtml?faces-redirect=true";
        } else {
          // If an older released version exists, redirect to that page instead
          ScopedIdentifier previousIdentifier = ScopedIdentifier.fromUrn(element.getUrn());
          int previousVersion = Integer.parseInt(previousIdentifier.getVersion());
          previousIdentifier.setVersion(String.valueOf(previousVersion - 1));
          redirectUrn = "detail.xhtml?urn=" + previousIdentifier + "&faces-redirect=true";
        }
        ScopedIdentifierDao.deleteDraftIdentifier(ctx, userId, element.getUrn());
      } else {
        ScopedIdentifierDao.outdateIdentifier(ctx, userId, element.getUrn());
      }
    }
    return redirectUrn;
  }

  /**
   * TODO: add javadoc.
   */
  public String deleteDrafts(List<Integer> namespaceIds) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ScopedIdentifierDao.deleteOwnDraftIdentifiersInNamespace(ctx, userId, namespaceIds);
    }
    return "mydrafts.xhtml?faces-redirect=true";
  }

  /** Lets the user login and sets all necessary fields. */
  public void login(AuthClient client) throws InvalidTokenException, InvalidKeyException {
    accessToken = client.getAccessToken();
    idToken = client.getIdToken();
    refreshToken = client.getRefreshToken();

    /*
     * Make sure that if the access token contains a state parameter, that it matches the state
     * variable. If it does not, abort.
     */
    if (!StringUtil.isEmpty(accessToken.getState()) && !state.equals(accessToken.getState())) {
      accessToken = null;
      idToken = null;
      refreshToken = null;
      return;
    }
    loginValid = true;
    userIdentity = client.getIdToken().getSubject();
    realName = client.getIdToken().getName();
    username = client.getIdToken().getEmail();
    try {
      List<String> resourceRoles =
          client.getAccessToken().getResourceRoles().get(client.getConfig().getClientId());
      hasAdminPrivilege = resourceRoles.contains(ROLE_NAME_ADMIN);
      canCreateNamespace = resourceRoles.contains(ROLE_NAME_CREATE_NAMESPACE);
      canCreateCatalogs = resourceRoles.contains(ROLE_NAME_CREATE_CATALOG);
      canExportImport = resourceRoles.contains(ROLE_NAME_IMPORT_EXPORT);
      canLinkElements = resourceRoles.contains(ROLE_NAME_LINK_ELEMENTS);
    } catch (Exception e) {
      hasAdminPrivilege = false;
      canCreateNamespace = false;
      canCreateCatalogs = false;
      canExportImport = false;
      canLinkElements = false;
    }
    starred = new LinkedList<>();
    compare = new LinkedList<>();
    createOrGetUser(client.getIdToken());
  }

  /** If the "userIdentity" does not exist in the database, create it. */
  private void createOrGetUser(JwtIdToken idToken) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      User user = UserDao.getUserByIdentity(ctx, userIdentity);
      if (user == null) {
        if (rewriteUserIds()) {
          try {
            user = UserDao.getUserByEmail(ctx, idToken.getEmail());
          } catch (TooManyRowsException e) {
            logger.error("Duplicate email in mdrUser table: ", idToken.getEmail());
            user = null;
          }
        }

        if (user == null) {
          user = UserDao.createDefaultUser(ctx, idToken.getSubject(), idToken.getEmail(),
              idToken.getName(), idToken.getExternalLabel());
          userId = user.getId();
        } else {
          user.setUsername(idToken.getSubject());
          user.setRealName(idToken.getName());
          user.setExternalLabel(idToken.getExternalLabel());
          UserDao.updateUser(ctx, user);
        }
      } else {
        /*
         * Update his email.
         */
        userId = user.getId();

        user.setEmail(idToken.getEmail());
        user.setRealName(idToken.getName());
        user.setExternalLabel(idToken.getExternalLabel());

        UserDao.updateUser(ctx, user);
      }

      // In maintenance mode, do not require the namespaces since at least one update changes the
      // permissions tables
      if (!MdrConfig.getMaintenanceMode()) {
        reloadNamespaces();
      }

      @SuppressWarnings("unchecked")
      List<String> descriptions =
          (List<String>) idToken.getClaimsSet().getClaim(JwtVocabulary.DESCRIPTION);
      String usertype = idToken.getClaimsSet().getStringClaim(JwtVocabulary.USERTYPE);

      /*
       * If the user has a registry, he has a public key known in Samply Auth. If he does,
       * the array of descriptions is not empty. In this case, automatically create a
       * namespace with the "osse-" prefix.
       * TODO: This might be problematic, since Samply Auth 1.4 the user can add public keys and
       *  then he gets a new namespace in the MDR.
       */
      if (usertype != null
          && ((getAuthClient().getConfig().isUseSamplyAuth()
          && (idToken.getUsertype() == Usertype.OSSE_REGISTRY
          || idToken.getUsertype() == Usertype.BRIDGEHEAD))
          || usertype.equals(KeycloakUsertype.OSSE_REGISTRY)
          || usertype.equals(KeycloakUsertype.BRIDGEHEAD))
          && descriptions.size() > 0
          && writableNamespaces.size() == 0) {
        int i = 1;
        String input = "osse-";
        Namespace namespace = ElementDao.getNamespace(ctx, input + i);

        if (namespace != null) {
          while (namespace != null) {
            ++i;
            namespace = ElementDao.getNamespace(ctx, input + i);
          }
        }

        input = input + i;

        Namespace ns = new Namespace();
        ns.setName(input);
        ns.setHidden(false);
        ns.setCreatedBy(user.getId());

        ElementDao.saveElement(ctx, userId, ns);

        Definition def = new Definition();
        def.setLanguage("en");
        def.setDefinition(descriptions.get(0));
        def.setDesignation(descriptions.get(0));
        def.setElementId(ns.getId());

        DefinitionDao.saveNamespaceDefinition(ctx, def);

        /*
         * Reload the namespaces once again...
         */
        if (!MdrConfig.getMaintenanceMode()) {
          reloadNamespaces();
        }
      }

      if (writableNamespaces != null && writableNamespaces.size() > 0) {
        selectedNamespace = writableNamespaces.get(0);
      }
      ctx.connection(Connection::commit);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    try {
      selectedLanguage =
          Language.valueOf(
              FacesContext.getCurrentInstance()
                  .getExternalContext()
                  .getRequestLocale()
                  .getLanguage()
                  .toUpperCase());
    } catch (Exception e) {
      selectedLanguage = Language.EN;
    }
  }

  /**
   * This step uses the readable and writable namespaces to separate them into two distinct groups.
   * Also it sorts them.
   */
  private void updateOtherNamespaces() {

    HashSet<String> writable = new HashSet<>();
    otherReadableNamespaces = new ArrayList<>();

    for (MdrNamespace ns : writableNamespaces) {
      writable.add(ns.getName());
    }

    for (MdrNamespace ns : readableNamespaces) {
      if (!writable.contains(ns.getName())) {
        otherReadableNamespaces.add(ns);
      }
    }

    Collections.sort(otherReadableNamespaces, namespaceComparator);
    Collections.sort(writableNamespaces, namespaceComparator);
  }

  /** Reloads the readable and writable namespaces and updates the internal lists. */
  public void reloadNamespaces() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      readableNamespaces = Mapper.convertNamespaces(
          NamespaceDao.getReadableNamespaces(ctx, userId), selectedLanguage);
      writableNamespaces = Mapper.convertNamespaces(
          NamespaceDao.getWritableNamespaces(ctx, userId), selectedLanguage);
      adminRightNamespaces = Mapper.convertNamespaces(
          NamespaceDao.getAdministratableNamespaces(ctx, userId), selectedLanguage);

      updateOtherNamespaces();

      if (writableNamespaces.size() > 0 && selectedNamespace == null) {
        selectedNamespace = writableNamespaces.get(0);
      }
    }
  }

  /** Executes a logout. */
  public void logout() throws IOException {
    username = null;
    realName = null;
    loginValid = false;
    userIdentity = null;
    userId = 0;
    starred = new LinkedList<>();
    compare = new LinkedList<>();

    canCreateNamespace = false;
    canCreateCatalogs = false;
    canExportImport = false;
    canLinkElements = false;

    accessToken = null;
    refreshToken = null;
    idToken = null;

    hasAdminPrivilege = false;

    reloadNamespaces();

    OAuth2Client config = MdrConfig.getOauth2();
    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

    context.redirect(
        OAuth2ClientConfig.getLogoutUrl(
            config,
            context.getRequestScheme(),
            context.getRequestServerName(),
            context.getRequestServerPort(),
            context.getRequestContextPath(),
            "/"));
  }

  /** Checks if the given urn is in the list of writable namespaces. */
  public Boolean isWritableNamespaceUrn(String urn) {
    ScopedIdentifier scoped = ScopedIdentifier.fromUrn(urn);
    return isWritableNamespaceByName(scoped.getNamespace());
  }

  /** Checks if the given namespace is writable. */
  public Boolean isWritableNamespace(MdrNamespace namespace) {
    if (namespace == null) {
      return false;
    } else {
      return isWritableNamespaceByName(namespace.getName());
    }
  }

  /** Checks if the given namespace is writable. */
  public Boolean isWritableNamespaceByName(String namespace) {
    if (hasAdminPrivilege) {
      return true;
    }
    for (MdrNamespace ns : getAdminRightNamespaces()) {
      if (ns.getName().equals(namespace)) {
        return true;
      }
    }
    return false;
  }

  /** Returns the URL for Samply.Auth */
  public String getAuthenticationUrl() throws UnsupportedEncodingException {
    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
    HttpServletRequest req = (HttpServletRequest) context.getRequest();

    StringBuffer requestUrl = new StringBuffer(context.getRequestServletPath());
    if (req.getQueryString() != null) {
      requestUrl.append("?").append(req.getQueryString());
    }

    String scheme = req.getScheme();
    String headerScheme = req.getHeader("X-Forwarded-Proto");
    if (headerScheme != null && !headerScheme.equalsIgnoreCase(scheme)) {
      scheme = headerScheme;
    }

    return OAuth2ClientConfig.getRedirectUrl(
        MdrConfig.getOauth2(),
        scheme,
        context.getRequestServerName(),
        context.getRequestServerPort(),
        context.getRequestContextPath(),
        requestUrl.toString(),
        null,
        state,
        Scope.OPENID, Scope.MDR);
  }

  /** Returns a list of namespaces, that the given element can be imported to. */
  public List<MdrNamespace> getImportableNamespaces(MdrElement element) {
    List<MdrNamespace> target = new ArrayList<>();
    for (MdrNamespace ns : writableNamespaces) {
      if (!ns.getName().equals(element.getElement().getScoped().getNamespace())) {
        target.add(ns);
      }
    }
    return target;
  }

  /** Adds the given element to the list of elements that the user wants to compare. */
  public String markToCompare(MdrElement element) {
    if (!compare.contains(element)) {
      compare.addFirst(element);
      return null;
    } else {
      return "/compare?faces-redirect=true";
    }
  }

  /** TODO: add javadoc. */
  public String markToExport(MdrElement element) {
    if (!export.contains(element)) {
      export.add(element);
    }
    return null;
  }

  /** TODO: add javadoc. */
  public String markToExport(String namespace) {
    if (!exportNamespace.contains(namespace)) {
      exportNamespace.add(namespace);
    }
    return null;
  }

  public String clearCompare() {
    compare.clear();
    return "/index?faces-redirect=true";
  }

  /** TODO: add javadoc. */
  public String clearExport() {
    export.clear();
    exportNamespace.clear();
    return "/index?faces-redirect=true";
  }

  /** Returns valid auth client with the current access tokens. */
  public AuthClient getAuthClient() {
    OAuth2Client config = MdrConfig.getOauth2();
    if (config.isUseSamplyAuth()) {
      return new SamplyAuthClient(
          accessToken, idToken, refreshToken, config, ClientUtil.getClient(), state);
    } else {
      return new KeycloakAuthClient(
          accessToken, idToken, refreshToken, config, ClientUtil.getClient(), state);
    }
  }

  public Boolean isExported(MdrElement element) {
    return export.contains(element);
  }

  public Boolean isExportedNamespace(String namespace) {
    return exportNamespace.contains(namespace);
  }

  public void selectNamespace(MdrNamespace namespace) {
    this.selectedNamespace = namespace;
  }

  public Boolean isSelected(MdrNamespace namespace) {
    return this.selectedNamespace != null
        && this.selectedNamespace.getName().equals(namespace.getName());
  }

  public Boolean isCompared(MdrElement element) {
    return compare.contains(element);
  }

  public List<MdrNamespace> getWritableNamespaces() {
    return writableNamespaces;
  }

  public void setWritableNamespaces(List<MdrNamespace> writableNamespaces) {
    this.writableNamespaces = writableNamespaces;
  }

  public List<MdrNamespace> getReadableNamespaces() {
    return readableNamespaces;
  }

  public void setReadableNamespaces(List<MdrNamespace> readableNamespaces) {
    this.readableNamespaces = readableNamespaces;
  }

  public List<MdrNamespace> getAdminRightNamespaces() {
    return adminRightNamespaces;
  }

  public void setAdminRightNamespaces(List<MdrNamespace> adminRightNamespaces) {
    this.adminRightNamespaces = adminRightNamespaces;
  }

  public Boolean getLoginValid() {
    return loginValid;
  }

  public void setLoginValid(Boolean loginValid) {
    this.loginValid = loginValid;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public Language getSelectedLanguage() {
    return selectedLanguage;
  }

  public void setSelectedLanguage(Language selectedLanguage) {
    this.selectedLanguage = selectedLanguage;
  }

  public String getStarItem() {
    return starItem;
  }

  public void setStarItem(String starItem) {
    this.starItem = starItem;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Collection<MdrElement> getStarred() {
    return starred;
  }

  public LinkedList<MdrElement> getCompare() {
    return compare;
  }

  public void setCompare(LinkedList<MdrElement> compare) {
    this.compare = compare;
  }

  public Boolean getCanCreateNamespace() {
    return canCreateNamespace;
  }

  public void setCanCreateNamespace(Boolean canCreateNamespace) {
    this.canCreateNamespace = canCreateNamespace;
  }

  public Boolean getCanLinkElements() {
    return canLinkElements;
  }

  public void setCanLinkElements(Boolean canLinkElements) {
    this.canLinkElements = canLinkElements;
  }

  public List<MdrNamespace> getOtherReadableNamespaces() {
    return otherReadableNamespaces;
  }

  public void setOtherReadableNamespaces(List<MdrNamespace> otherReadableNamespaces) {
    this.otherReadableNamespaces = otherReadableNamespaces;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public MdrNamespace getSelectedNamespace() {
    return selectedNamespace;
  }

  public void setSelectedNamespace(MdrNamespace selectedNamespace) {
    this.selectedNamespace = selectedNamespace;
  }

  public JwtAccessToken getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(JwtAccessToken accessToken) {
    this.accessToken = accessToken;
  }

  public JwtIdToken getIdToken() {
    return idToken;
  }

  public void setIdToken(JwtIdToken idtoken) {
    this.idToken = idtoken;
  }

  public JwtRefreshToken getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(JwtRefreshToken refreshToken) {
    this.refreshToken = refreshToken;
  }

  public Boolean getCanCreateCatalogs() {
    return canCreateCatalogs;
  }

  public void setCanCreateCatalogs(Boolean canCreateCatalogs) {
    this.canCreateCatalogs = canCreateCatalogs;
  }

  public ArrayList<MdrElement> getExport() {
    return export;
  }

  public void setExport(ArrayList<MdrElement> export) {
    this.export = export;
  }

  public ArrayList<String> getExportNamespace() {
    return exportNamespace;
  }

  public void setExportNamespace(ArrayList<String> exportNamespace) {
    this.exportNamespace = exportNamespace;
  }

  public Boolean getCanExportImport() {
    return canExportImport;
  }

  public void setCanExportImport(Boolean canExportImport) {
    this.canExportImport = canExportImport;
  }

  public Boolean getHasAdminPrivilege() {
    return hasAdminPrivilege;
  }

  public String getState() {
    return state;
  }

  /**
   * Get the REST URL of the rest api for tree comparison.
   */
  public String getRestUrl() {
    // TODO: maybe read all config params from env? Something has to be done here.
    String restUrl = System.getProperty("REST_URL", "");
    if (restUrl.isEmpty()) {
      restUrl = System.getenv("REST_URL");
    }
    return restUrl;
  }

  /**
   * Check if user ids shall be rewritten.
   */
  private boolean rewriteUserIds() {
    String rewriteString = System.getProperty("REWRITE_KEYCLOAK_IDS", "");
    if (rewriteString.isEmpty()) {
      rewriteString = System.getenv("REWRITE_KEYCLOAK_IDS");
    }
    return Boolean.parseBoolean(rewriteString);
  }
}
