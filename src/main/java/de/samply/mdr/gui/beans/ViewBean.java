/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.beans;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.util.Mapper;
import de.samply.string.util.StringUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.jooq.DSLContext;

/**
 * The bean that handles the view page.
 *
 * @author paul
 */
@ManagedBean
@ViewScoped
public class ViewBean implements Serializable {

  private static final long serialVersionUID = 7492773861851792281L;

  @ManagedProperty(value = "#{userBean}")
  private UserBean userBean;

  @ManagedProperty(value = "#{changeNamespaceWizard}")
  private ChangeNamespaceWizard wizard;

  /** The namespaces that are relevant for the elements that have been found. */
  private List<MdrNamespace> namespaces = Collections.emptyList();

  /** The list of the namespaces that the user has write access to. */
  private List<MdrNamespace> myNamespaces = Collections.emptyList();

  /** The elements that are found for the selected namespace. */
  private List<MdrElement> activeElements = Collections.emptyList();

  /** The currently selected namespace as MdrNamespace. */
  private MdrNamespace mdrNamespace = null;

  /** The selected namespace. */
  private String selectedNamespace = "";

  /**
   * Called to initialize all fields. Called by the the viewAction in xhtml files that require this
   * bean.
   */
  public void initialize() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      namespaces = Mapper.convertNamespaces(
          NamespaceDao.getReadableNamespaces(
              ctx, getUserBean().getUserId()), userBean.getSelectedLanguage());
      Collections.sort(namespaces, MdrNamespace.nameComparator);

      myNamespaces = getUserBean().getWritableNamespaces();
      Collections.sort(myNamespaces, MdrNamespace.nameComparator);

      for (MdrNamespace ns : myNamespaces) {
        namespaces.remove(ns);
      }

      if (!StringUtil.isEmpty(selectedNamespace)) {
        updateElements();
      }
    }
  }

  /** TODO: add javadoc. */
  public void updateElements() {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      activeElements =
          Mapper.convertElements(
              IdentifiedDao.getRootElements(ctx, userBean.getUserId(), selectedNamespace),
              userBean.getSelectedLanguage(),
              true);
    }
  }

  /** Checks if this namespace is owned by the current user. */
  public Boolean isOwnedNamespace() {
    return (mdrNamespace != null && mdrNamespace.getCreatedBy() == userBean.getUserId());
  }

  public List<MdrElement> getActiveElements() {
    return activeElements;
  }

  public void setActiveElements(List<MdrElement> elements) {
    this.activeElements = elements;
  }

  public UserBean getUserBean() {
    return userBean;
  }

  public void setUserBean(UserBean userBean) {
    this.userBean = userBean;
  }

  public ChangeNamespaceWizard getWizard() {
    return wizard;
  }

  public void setWizard(ChangeNamespaceWizard wizard) {
    this.wizard = wizard;
  }

  public List<MdrNamespace> getNamespaces() {
    return namespaces;
  }

  public void setNamespaces(List<MdrNamespace> allNamespaces) {
    this.namespaces = allNamespaces;
  }

  public String getSelectedNamespace() {
    return selectedNamespace;
  }

  public void setSelectedNamespace(String selectedNamespace) {
    this.selectedNamespace = selectedNamespace;
  }

  public List<MdrNamespace> getMyNamespaces() {
    return myNamespaces;
  }

  public void setMyNamespaces(List<MdrNamespace> myNamespaces) {
    this.myNamespaces = myNamespaces;
  }
}
