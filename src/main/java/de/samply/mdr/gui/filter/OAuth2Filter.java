/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 * <p>
 * Additional permission under GNU GPL version 3 section 7:
 * <p>
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.gui.filter;

import com.nimbusds.jose.JWSAlgorithm;
import de.samply.auth.client.AuthClient;
import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.KeycloakAuthClient;
import de.samply.auth.client.SamplyAuthClient;
import de.samply.auth.client.jwt.JwtAccessToken;
import de.samply.auth.client.jwt.JwtIdToken;
import de.samply.auth.client.jwt.JwtRefreshToken;
import de.samply.common.config.OAuth2Client;
import de.samply.mdr.gui.ClientUtil;
import de.samply.mdr.gui.MdrConfig;
import de.samply.mdr.gui.beans.UserBean;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This web filter handles the code from the central authentication server. It checks if there is a
 * code query parameter and if there is, it tries to get an access token using the given code.
 */
@WebFilter(filterName = "OAuth2Filter")
public class OAuth2Filter implements Filter {

  private static final Logger logger = LogManager.getLogger(OAuth2Filter.class);

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    /*
     * This is crucial, because the default encoding in tomcat is ISO-8859-1 and the HttpRequest
     * tries to parse
     * the parameters using this default encoding.
     */
    httpRequest.setCharacterEncoding(StandardCharsets.UTF_8.displayName());

    HttpSession session = httpRequest.getSession(true);

    UserBean userBean = (UserBean) session.getAttribute("userBean");

    /*
     * Create a new session.
     */
    if (userBean == null) {
      userBean = new UserBean();
      session.setAttribute("userBean", userBean);
    }

    /*
     * Ignore if the user already has a valid login.
     */
    if (!userBean.getLoginValid()
        && (httpRequest.getParameter("code") != null
            || httpRequest.getParameter("accessToken") != null)) {
      try {
        OAuth2Client config = MdrConfig.getOauth2();
        AuthClient client;
        if (httpRequest.getParameter("code") != null) {
          String code = httpRequest.getParameter("code");
          client =
              config.isUseSamplyAuth()
                  ? new SamplyAuthClient(code, config, ClientUtil.getClient(), userBean.getState())
                  : new KeycloakAuthClient(
                      code, config, ClientUtil.getClient(), userBean.getState());
          logger.debug("Code as parameter found, trying a login with the given code");
        } else {
          boolean externalValidation = false;
          try {
            List<String> supportedSigningAlgs = AuthClient
                .getDiscovery(ClientUtil.getClient(), config).getSupportedSigningAlgs();
            externalValidation = supportedSigningAlgs.contains(JWSAlgorithm.HS256.getName());
          } catch (Exception e) {
            logger.warn("Could not get supported signing algorithms", e);
          }
          JwtAccessToken accessToken =
              new JwtAccessToken(config, httpRequest.getParameter("accessToken"),
                  externalValidation);
          JwtRefreshToken refreshToken =
              new JwtRefreshToken(config, httpRequest.getParameter("refreshToken"),
                  externalValidation);
          JwtIdToken idToken = new JwtIdToken(config, httpRequest.getParameter("idToken"),
              externalValidation);
          if (!externalValidation && (!accessToken.isValid() || !idToken.isValid() || !refreshToken
              .isValid())) {
            throw new InvalidTokenException();
          }
          logger.debug("Tokens as parameters found, trying a login with the given tokens");
          client =
              new KeycloakAuthClient(
                  accessToken,
                  idToken,
                  refreshToken,
                  config,
                  ClientUtil.getClient(),
                  userBean.getState());
          if (externalValidation && (
              !((KeycloakAuthClient) client).checkTokenValidity(accessToken, true)
                  || !((KeycloakAuthClient) client).checkTokenValidity(accessToken, true)
                  || !((KeycloakAuthClient) client).checkTokenValidity(accessToken, true))) {
            throw new InvalidTokenException();
          }
        }
        String redirectUrl = httpRequest.getRequestURL().toString();
        String forwardedProtocolHeader = httpRequest.getHeader("X-Forwarded-Proto");
        if (forwardedProtocolHeader != null
            && !forwardedProtocolHeader.equalsIgnoreCase(httpRequest.getScheme())) {
          redirectUrl = redirectUrl.replaceFirst(httpRequest.getScheme(), forwardedProtocolHeader);
        }

        logger.info("Setting redirect URL to " + redirectUrl);
        client.setRedirectUrl(redirectUrl); // for oauth2 other than samply auth

        userBean.login(client);
      } catch (InvalidTokenException | InvalidKeyException e) {
        logger.error("The token was invalid, aborting");
      } catch (NotFoundException e) {
        /*
         * In case the login was not valid, just ignore it
         * and reload the namespaces for the anonymous user
         */
        logger.warn(
            "A code was received, but the central authentication returned with a 404. "
                + "Ignoring the code and continuing");
        userBean.init();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    /*
     * In case someone is still using the old way with "/samplyLogin.xhtml",
     * redirect him back to the index page.
     */
    if (httpRequest.getRequestURI().endsWith("/samplyLogin.xhtml")) {
      httpResponse.sendRedirect(httpRequest.getContextPath() + "/index.xhtml");
      return;
    }

    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {}
}
