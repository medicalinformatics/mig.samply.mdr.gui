/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.gui.validator;

import de.samply.mdr.util.Message;
import java.math.BigDecimal;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.omnifaces.util.Messages;

@FacesValidator("stringMaxLengthValidator")
public class StringMaxLengthValidator implements Validator {

  @Override
  public void validate(FacesContext context, UIComponent component, Object value)
      throws ValidatorException {
    try {
      if (new BigDecimal(value.toString()).signum() < 1) {
        throw new ValidatorException(
            Messages.create(Message.getString("invalidMaxLength"))
                .detail(Message.getString("invalidMaxLengthDetail"))
                .error()
                .get());
      }
    } catch (NumberFormatException ex) {
      throw new ValidatorException(
          Messages.create(Message.getString("invalidMaxLength"))
              .detail(Message.getString("invalidMaxLengthDetail"))
              .error()
              .get());
    }
  }
}
