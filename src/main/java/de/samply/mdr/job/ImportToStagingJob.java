/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.job;

import de.samply.mdr.staging.FileReaderExcel;
import de.samply.mdr.util.StagingUtil;
import java.io.File;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportToStagingJob implements Callable {

  private static final Logger logger = LoggerFactory.getLogger(ImportToStagingJob.class);
  int userId;
  int fallbackNamespaceId;
  private File importFile;

  /**
   * TODO: add javadoc.
   */
  public ImportToStagingJob(File importFile, int userId, int fallbackNamespaceId) {
    this.importFile = importFile;
    this.userId = userId;
    this.fallbackNamespaceId = fallbackNamespaceId;
  }

  @Override
  public Object call() throws Exception {
    logger.info("Starting import thread");
    FileReaderExcel fileReaderExcel = new FileReaderExcel(importFile);
    StagingUtil stagingUtil = new StagingUtil();
    String namespaceName;
    try {
      namespaceName = fileReaderExcel.getInfo().getNamespaceName();
    } catch (NullPointerException npe) {
      namespaceName = null;
    }
    stagingUtil.stageImport(
        fileReaderExcel.getInfo(),
        fileReaderExcel.getDataelements(),
        userId,
        namespaceName,
        fallbackNamespaceId);
    logger.info("Import thread done.");
    return null;
  }
}
