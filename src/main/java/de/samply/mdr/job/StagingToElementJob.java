/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.job;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.StagingDao;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.util.StagingUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StagingToElementJob implements Callable {

  private static final Logger logger = LoggerFactory.getLogger(StagingToElementJob.class);
  int userId;
  private Collection<Integer> stagedElementIds;
  private boolean releaseImmediately;

  /**
   * TODO: add javadoc.
   */
  public StagingToElementJob(Collection<Integer> stagedElementIds, int userId) {
    this.stagedElementIds = stagedElementIds;
    this.userId = userId;
    releaseImmediately = false;
  }

  /**
   * TODO: add javadoc.
   */
  public StagingToElementJob(
      Collection<Integer> stagedElementIds, int userId, boolean releaseImmediately) {
    this.stagedElementIds = stagedElementIds;
    this.userId = userId;
    this.releaseImmediately = releaseImmediately;
  }

  @Override
  public Object call() throws Exception {
    if (stagedElementIds == null || stagedElementIds.size() < 1) {
      return null;
    }
    if (releaseImmediately) {
      logger.info("Starting to store and release elements");
    } else {
      logger.info("Starting to store elements as drafts");
    }
    Collection<Staging> stagedElements;
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      stagedElements = StagingDao.getStagedElementsById(ctx, stagedElementIds);
      // Check if all parent elements are part of the collection. If they are missing, add them
      Collection<Integer> missingAncestorIds = new ArrayList<>();
      for (Staging stagedElement : stagedElements) {
        Integer parentId = stagedElement.getParentId();
        if (parentId != null && parentId > 0 && !stagedElementIds.contains(parentId)) {
          missingAncestorIds.add(parentId);
        }
      }
      if (!missingAncestorIds.isEmpty()) {
        stagedElements.addAll(StagingDao.getStagedElementsById(ctx, missingAncestorIds));
      }
    } catch (Exception e) {
      logger.error("An error occurred during conversion of staged elements: " + e.getMessage());
      return null;
    }
    StagingUtil stagingUtil = new StagingUtil();
    stagingUtil.persistStagedElements(stagedElements, userId, releaseImmediately);
    logger.info("Converting staged elements finished.");
    return null;
  }
}
