/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.job.concepts;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.jooq.Tables;
import de.samply.mdr.dal.jooq.tables.pojos.Source;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpException;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.jooq.DSLContext;

/**
 *  TODO: add javadoc.
 */
public class MdmQuery {

  private String credential;
  private Source source;

  private String umls = "UMLS";
  private String version = "v1";

  /**
   *  TODO: add javadoc.
   */
  public MdmQuery(Integer userId, String credential) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      List<Source> sources = ctx.fetch(ctx.select().from(Tables.SOURCE)).into(Source.class);
      for (Source s : sources) {
        if (s.getName().equals("MDM")) {
          source = s;
        }
      }
      this.credential = credential;
    }
  }

  /**
   * TODO: add javadoc.
   */
  public List<ConceptAssociation> getCodes(String designation, String datatype) throws Exception {

    JsonArray jsonArray = getClientResponseJsonArray("items/codes", designation, datatype);

    List<ConceptAssociation> conceptAssociationsList = new ArrayList<>();

    // resulting codes from rest-call
    String[] codes = new String[jsonArray.size()];
    for (int i = 0; i < jsonArray.size(); i++) {
      JsonObject jsonObject =
          jsonArray
              .get(i)
              .getAsJsonObject()
              .get("coding")
              .getAsJsonArray()
              .get(0)
              .getAsJsonObject();
      codes[i] = jsonObject.get("umls").toString();
    }

    // get unique codes from codes[] with meaning/description of the code
    String[] unique = new HashSet<>(Arrays.asList(codes)).toArray(new String[0]);
    String[][] conceptCodes = new String[unique.length][2];
    for (int i = 0; i < unique.length; i++) {
      conceptCodes[i][0] = unique[i];
      for (int j = 0; j < jsonArray.size(); j++) {
        if (jsonArray
            .get(j)
            .getAsJsonObject()
            .get("coding")
            .getAsJsonArray()
            .get(0)
            .getAsJsonObject()
            .get("umls")
            .toString()
            .equals(unique[i])) {
          conceptCodes[i][1] =
              jsonArray
                  .get(j)
                  .getAsJsonObject()
                  .get("coding")
                  .getAsJsonArray()
                  .get(0)
                  .getAsJsonObject()
                  .get("meaning")
                  .toString();
        }
      }
    }

    // for every unique code[] a concept association is created and added to the list
    // conceptAssociationsList
    for (String[] conceptCode : conceptCodes) {
      ConceptAssociation conceptAssociation = new ConceptAssociation();
      conceptAssociation.setSourceId(source.getId());
      conceptAssociation
          .setTerm(conceptCode[0].substring(1, (conceptCode[0].length() - 1)));
      conceptAssociation
          .setVersion(version);//TODO: get version from query (UMLS-version or MDM-Version)?
      conceptAssociation
          .setText(conceptCode[1].substring(1, (conceptCode[1].length() - 1)));
      conceptAssociation.setSystem(umls); //TODO: get concept from query

      conceptAssociationsList.add(conceptAssociation);
    }

    return conceptAssociationsList;
  }

  /*  private JsonObject getClientResponseJsonObject(String specifiedUrl, String designation,
      String datatype) {
    return new Gson()
        .fromJson(getClientResponse(specifiedUrl, designation, datatype).readEntity(String.class),
            JsonObject.class);
  }*/

  private JsonArray getClientResponseJsonArray(
      String specifiedUrl, String designation, String datatype) throws Exception {
    return new Gson()
        .fromJson(
            getClientResponse(specifiedUrl, designation, datatype).readEntity(String.class),
            JsonArray.class);
  }

  private Response getClientResponse(String specifiedUrl, String designation, String datatype)
      throws Exception {
    Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
    WebTarget webTarget;
    if (datatype == null) {
      webTarget =
          client
              .target(source.getBaseurl())
              .path(specifiedUrl)
              .queryParam("apiKey", credential)
              .queryParam("query", designation);
    } else {
      webTarget =
          client
              .target(source.getBaseurl())
              .path(specifiedUrl)
              .queryParam("apiKey", credential)
              .queryParam("query", "name%3A\"" + designation + "\"AND\"" + datatype + "\"");
    }
    Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
    Response response = invocationBuilder.get();

    if (response.getStatus() != 200) {
      throw new HttpException("" + response.getStatus());
    }
    return response;
  }
}
