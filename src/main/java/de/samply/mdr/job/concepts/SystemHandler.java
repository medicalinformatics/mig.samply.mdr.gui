/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.job.concepts;

import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.lib.Constants.Language;
import java.util.ArrayList;
import java.util.List;

public class SystemHandler {

  private String apikey;
  private String baseUrl;

  private String source;
  private String system;

  /**
   * TODO: add javadoc.
   */
  public List<ConceptAssociation> getCodes(String designation, String datatype, Language language) {
    List<ConceptAssociation> conceptAssociationsList = new ArrayList<>();

    if (language == Language.DE) {
      apikey = "";
      baseUrl = "https://medical-data-models.org/MDR/api/v1/";
      source = "MDM-Portal";
      system = "UMLS";
      // MdmQuery mdmQuery = new MdmQuery(apikey,baseUrl);
      // return mdmQuery.getCodesFromMDM(designation,datatype);
      // conceptAssociationsList.addAll(new MdmQuery(apikey, baseUrl).getCodesFromMDM(designation,
      // datatype));

    } else if (language == Language.EN) {
      apikey = "";
      baseUrl = "";
      source = "UMLS";
      conceptAssociationsList.addAll(new UmlsQuery().getCodesFromUmls(designation, datatype));
    }

    return conceptAssociationsList;
  }
}
