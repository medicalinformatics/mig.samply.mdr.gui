/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import org.apache.commons.io.FilenameUtils;

/** A simple JSF Util. */
public class JsfUtil {

  /** TODO: add javadoc. */
  public static String getString(String name) {
    FacesContext context = FacesContext.getCurrentInstance();
    ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
    return bundle.getString(name);
  }

  /**
   * Save a file part to a temporary file.
   *
   * @param prefix the prefix of the temp file
   * @param part the file part to save
   * @return the resulting temp file
   */
  public static File savePartToTmpFile(String prefix, Part part) throws IOException {
    File file = Files.createTempFile(prefix, getFileName(part)).toFile();
    try (InputStream input = part.getInputStream()) {
      Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
    return file;
  }

  /**
   * Gets the file name of a file part.
   *
   * @param filePart the file part
   * @return the file name
   */
  public static String getFileName(Part filePart) {
    String header = filePart.getHeader("content-disposition");
    for (String headerPart : header.split(";")) {
      if (headerPart.trim().startsWith("filename")) {
        String filepath =
            headerPart.substring(headerPart.indexOf('=') + 1).trim().replace("\"", "");
        // in case of IE, the part name is the full path of the file
        return FilenameUtils.getName(filepath);
      }
    }
    return null;
  }
}
