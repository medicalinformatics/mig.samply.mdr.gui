/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package  de.samply.mdr.util;

import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.HierarchyDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.UserDao;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.NamespacePermission;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.enums.Validationtype;
import de.samply.mdr.gui.DetailedMdrElement;
import de.samply.mdr.gui.MdrElement;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.gui.NamespacePermissionDto;
import de.samply.mdr.gui.SlotDto;
import de.samply.mdr.gui.ValueDomainDto;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.gui.beans.TreeNode.ChildrenSelectionState;
import de.samply.mdr.gui.exceptions.ElementNotFoundException;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.Constants.DatatypeField;
import de.samply.mdr.lib.Constants.DateRepresentation;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.lib.Constants.TimeRepresentation;
import de.samply.string.util.StringUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jooq.DSLContext;

/**
 * A mapper that converts the objects from DAOs to objects used in the GUI.
 *
 * @author paul
 */
public class Mapper {

  private static final Comparator<MdrElement> elementComparator =
      new Comparator<MdrElement>() {
        @Override
        public int compare(MdrElement o1, MdrElement o2) {
          if (o1.getElement().getElementType() == o2.getElement().getElementType()) {
            return o1.getDesignation().compareToIgnoreCase(o2.getDesignation());
          } else {
            return o1.getElement().getElementType().compareTo(o2.getElement().getElementType());
          }
        }
      };

  /**
   * Creates a value domain DTO from the given value domain. Uses the MDR connection to get all
   * definitions for the given scoped identifier.
   */
  public static ValueDomainDto convert(
      DSLContext ctx, int userId, ValueDomain domain, ScopedIdentifier identifier,
      Language priority) {
    ValueDomainDto valueDomain = new ValueDomainDto();
    Pattern rangePattern = Pattern.compile("^(?:(.*)<=)?x(?:<=(.*))?$");

    if (domain instanceof EnumeratedValueDomain) {
      /*
       * In case of an enumerated value domain, get all permissible values
       */
      valueDomain.setDatatype(DatatypeField.LIST);
      valueDomain.getPermittedValues().clear();

      for (Element element : ElementDao.getPermissibleValues(ctx, domain.getId())) {
        PermissibleValue value = (PermissibleValue) element;
        AdapterPermissibleValue v = new AdapterPermissibleValue();
        v.setValue(value.getPermittedValue());
        v.setDefinitions(
            convert(DefinitionDao.getDefinitions(ctx, value.getId(), identifier.getId())));
        v.setId(value.getId());
        valueDomain.getPermittedValues().add(v);
      }
    } else if (domain instanceof CatalogValueDomain) {
      /*
       * In case of a catalog value domain, first get all catalog codes and
       * then all permissible values.
       */
      valueDomain.setDatatype(DatatypeField.CATALOG);

      IdentifiedElement catalog =
          IdentifiedDao.getElement(
              ctx, userId, ((CatalogValueDomain) domain).getCatalogScopedIdentifierId());

      ScopedIdentifier scopedIdentifier = catalog.getScoped();
      valueDomain.setCatalog(scopedIdentifier.toString());

      valueDomain.setCatalogRoot(new TreeNode(Mapper.convert(catalog, priority)));

      HashMap<Integer, TreeNode> index = new HashMap<>();
      index.put(catalog.getScoped().getId(), valueDomain.getCatalogRoot());

      HashMap<String, TreeNode> urnIndex = new HashMap<>();
      urnIndex.put(valueDomain.getCatalog(), valueDomain.getCatalogRoot());

      /*
       * First put them all in an index.
       */
      for (IdentifiedElement element :
          IdentifiedDao.getAllSubMembers(ctx, userId, valueDomain.getCatalog())) {
        TreeNode node = new TreeNode(Mapper.convert(element, priority));

        index.put(element.getScoped().getId(), node);
        urnIndex.put(element.getScoped().toString(), index.get(element.getScoped().getId()));

        node.setSelected(false);
        node.setState(ChildrenSelectionState.NONE);
      }

      valueDomain.setSubCodes(
          Mapper.convertElements(
              IdentifiedDao.getEntries(ctx, userId, valueDomain.getCatalog()), priority, false));

      /*
       * Create the tree using the nodes.
       */
      for (HierarchyNode node : HierarchyDao.getHierarchyNodes(ctx, valueDomain.getCatalog())) {
        index.get(node.getSuperId()).getChildren().add(index.get(node.getSubId()));
        index.get(node.getSubId()).getParents().add(index.get(node.getSuperId()));
      }

      valueDomain.setUrnIndex(urnIndex);
      valueDomain.setIndex(index);

      for (IdentifiedElement element : IdentifiedDao.getPermissibleCodes(ctx, userId, domain)) {
        urnIndex.get(element.getScoped().toString()).setSelected(true);
      }

      valueDomain.updateTreeState();
    } else if (domain instanceof DescribedValueDomain) {
      DescribedValueDomain described = (DescribedValueDomain) domain;
      Matcher matcher;

      switch (described.getValidationType()) {
        case BOOLEAN:
          valueDomain.setDatatype(DatatypeField.BOOLEAN);
          break;

        case DATE:
          valueDomain.setDatatype(DatatypeField.DATE);
          valueDomain.setWithDays(described.getFormat().contains("DD"));
          valueDomain.setDateRepresentation(
              DateRepresentation.parseValidationData(described.getValidationData()));
          break;

        case DATETIME:
          valueDomain.setDatatype(DatatypeField.DATETIME);
          valueDomain.setWithDays(described.getFormat().contains("DD"));
          valueDomain.setWithSeconds(described.getFormat().contains("ss"));
          valueDomain.setDateRepresentation(
              DateRepresentation.parseValidationData(described.getValidationData()));
          valueDomain.setTimeRepresentation(
              TimeRepresentation.parseValidationData(described.getValidationData()));
          break;

        case TIME:
          valueDomain.setDatatype(DatatypeField.TIME);
          valueDomain.setWithSeconds(described.getFormat().contains("ss"));
          valueDomain.setTimeRepresentation(
              TimeRepresentation.parseValidationData(described.getValidationData()));
          break;

        case NONE:
          if (described.getMaxCharacters() > 0) {
            valueDomain.setUseMaxLength(true);
            valueDomain.setMaxLength(described.getMaxCharacters());
          }

          switch (described.getDatatype()) {
            case "DATE":
              valueDomain.setDatatype(DatatypeField.DATE);
              valueDomain.setWithDays(true);
              break;

            case "DATETIME":
              valueDomain.setDatatype(DatatypeField.DATETIME);
              valueDomain.setWithDays(true);
              valueDomain.setWithSeconds(true);
              break;

            case "NUMBER":
            case "INTEGER":
            case "INT":
              valueDomain.setDatatype(DatatypeField.INTEGER);
              valueDomain.setWithinRange(false);
              break;

            default:
              valueDomain.setDatatype(DatatypeField.STRING);
              valueDomain.setUseRegex(false);
              break;
          }

          break;

        case REGEX:
          valueDomain.setDatatype(DatatypeField.STRING);
          valueDomain.setUseRegex(true);
          valueDomain.setFormat(described.getValidationData());

          if (described.getMaxCharacters() > 0) {
            valueDomain.setUseMaxLength(true);
            valueDomain.setMaxLength(described.getMaxCharacters());
          }
          break;

        case FLOAT:
          valueDomain.setDatatype(DatatypeField.FLOAT);
          valueDomain.setWithinRange(false);
          valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
          break;

        case FLOATRANGE:
          valueDomain.setDatatype(DatatypeField.FLOAT);
          valueDomain.setWithinRange(true);
          valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
          matcher = rangePattern.matcher(described.getValidationData());

          if (matcher.find()) {
            if (matcher.group(1) != null) {
              valueDomain.setRangeFrom(Float.parseFloat(matcher.group(1)));
              valueDomain.setUseRangeFrom(true);
            } else {
              valueDomain.setUseRangeFrom(false);
            }

            if (matcher.group(2) != null) {
              valueDomain.setRangeTo(Float.parseFloat(matcher.group(2)));
              valueDomain.setUseRangeTo(true);
            } else {
              valueDomain.setUseRangeTo(false);
            }
          } else {
            valueDomain.setUseRangeFrom(false);
            valueDomain.setUseRangeTo(false);
            valueDomain.setWithinRange(false);
          }
          break;

        case INTEGER:
          valueDomain.setDatatype(DatatypeField.INTEGER);
          valueDomain.setWithinRange(false);
          valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
          break;

        case INTEGERRANGE:
          valueDomain.setDatatype(DatatypeField.INTEGER);
          valueDomain.setWithinRange(true);
          valueDomain.setUnitOfMeasure(described.getUnitOfMeasure());
          matcher = rangePattern.matcher(described.getValidationData());

          if (matcher.find()) {
            if (matcher.group(1) != null) {
              valueDomain.setRangeFrom(Integer.parseInt(matcher.group(1)));
              valueDomain.setUseRangeFrom(true);
            } else {
              valueDomain.setUseRangeFrom(false);
            }

            if (matcher.group(2) != null) {
              valueDomain.setRangeTo(Integer.parseInt(matcher.group(2)));
              valueDomain.setUseRangeTo(true);
            } else {
              valueDomain.setUseRangeTo(false);
            }
          } else {
            valueDomain.setUseRangeFrom(false);
            valueDomain.setUseRangeTo(false);
            valueDomain.setWithinRange(false);
          }
          break;

        case TBD:
          valueDomain.setDatatype(DatatypeField.TBD);
          break;

        default:
          break;
      }
    }

    return valueDomain;
  }

  /** Convert a DefinitionDTO object into a storable Definition. */
  public static Definition convert(AdapterDefinition definition) {
    Definition def = new Definition();
    def.setDefinition(definition.getDefinition());
    def.setDesignation(definition.getDesignation());
    def.setLanguage(definition.getLanguage().getName());
    return def;
  }

  /** Convert a value domain DTO into a storable value domain object. */
  public static ValueDomain convert(ValueDomainDto valueDomain) {
    ValueDomain domain;

    if (valueDomain.getDatatype() == DatatypeField.LIST) {
      domain = new EnumeratedValueDomain();
      domain.setDatatype("enumerated");
      domain.setFormat("enumerated");
      domain.setUnitOfMeasure(null);
    } else if (valueDomain.getDatatype() == DatatypeField.CATALOG) {
      CatalogValueDomain catalogValueDomain = new CatalogValueDomain();
      catalogValueDomain.setCatalogScopedIdentifierId(
          valueDomain.getCatalogRoot().getElement().getElement().getScoped().getId());
      domain = catalogValueDomain;
      domain.setDatatype("catalog");
      domain.setFormat("catalog");
      domain.setUnitOfMeasure(null);

    } else {
      DescribedValueDomain described = new DescribedValueDomain();

      switch (valueDomain.getDatatype()) {
        case BOOLEAN:
          described.setDatatype("BOOLEAN");
          described.setDescription("(true|false|yes|no|f|t)");
          described.setFormat(described.getDescription());
          described.setValidationType(Validationtype.BOOLEAN);
          described.setValidationData(described.getFormat());
          described.setMaxCharacters(5);
          described.setUnitOfMeasure(null);
          break;

        case FLOAT:
        case INTEGER:
          described.setDatatype(valueDomain.getDatatype().toString());
          described.setMaxCharacters(valueDomain.getRangeTo().toString().length());
          if (valueDomain.getWithinRange()) {
            described.setValidationType(
                Validationtype.valueOf(valueDomain.getDatatype().toString() + "RANGE"));
            described.setValidationData(
                (valueDomain.getUseRangeFrom() ? valueDomain.getRangeFrom() + "<=" : "")
                    + "x"
                    + (valueDomain.getUseRangeTo() ? "<=" + valueDomain.getRangeTo() : ""));
            described.setFormat(described.getValidationData());
          } else {
            described.setValidationType(
                Validationtype.valueOf(valueDomain.getDatatype().toString()));
            described.setValidationData("x");
            described.setFormat(described.getValidationData());
          }
          described.setUnitOfMeasure(valueDomain.getUnitOfMeasure());
          described.setDescription(described.getFormat());
          break;

        case STRING:
          described.setDescription(valueDomain.getFormat());
          described.setFormat(valueDomain.getFormat());
          described.setDatatype("STRING");
          described.setUnitOfMeasure(null);

          if (valueDomain.getUseMaxLength()) {
            described.setMaxCharacters(valueDomain.getMaxLength());
          } else {
            described.setMaxCharacters(0);
          }

          if (valueDomain.getUseRegex()) {
            described.setValidationType(Validationtype.REGEX);
            described.setValidationData(valueDomain.getFormat());
          } else {
            described.setValidationType(Validationtype.NONE);
            described.setValidationData(null);
          }

          break;

        case DATE:
          /*
           * If the date is in the local date, always set with days.
           */
          if (valueDomain.getDateRepresentation() == DateRepresentation.LOCAL_DATE) {
            valueDomain.setWithDays(true);
          }
          described.setFormat(getDateFormat(valueDomain));
          described.setDatatype("DATE");
          described.setUnitOfMeasure(null);
          described.setMaxCharacters(described.getFormat().length());
          described.setDescription(described.getFormat());
          described.setValidationType(Validationtype.DATE);
          described.setValidationData(getValidationData(valueDomain));
          break;

        case DATETIME:
          /*
           * Datetime without days does *not* make sense
           */
          valueDomain.setWithDays(true);

          described.setFormat(getDateFormat(valueDomain) + " " + getTimeFormat(valueDomain));
          described.setDatatype("DATETIME");
          described.setUnitOfMeasure(null);
          described.setMaxCharacters(described.getFormat().length());
          described.setDescription(described.getFormat());
          described.setValidationType(Validationtype.DATETIME);
          described.setValidationData(getValidationData(valueDomain));
          break;

        case TIME:
          described.setFormat(getTimeFormat(valueDomain));
          described.setDatatype("TIME");
          described.setUnitOfMeasure(null);
          described.setMaxCharacters(described.getFormat().length());
          described.setDescription(described.getFormat());
          described.setValidationType(Validationtype.TIME);
          described.setValidationData(getValidationData(valueDomain));
          break;

        case TBD:
          described.setDatatype(Validationtype.TBD.name());
          described.setDescription(Validationtype.TBD.name());
          described.setFormat(described.getDescription());
          described.setValidationType(Validationtype.TBD);
          described.setUnitOfMeasure(null);
          described.setMaxCharacters(0);
          break;

        default:
          break;
      }

      domain = described;
    }

    return domain;
  }

  /** Converts a list of definitions into their DTO counterparts. */
  public static List<AdapterDefinition> convert(List<Definition> definitions) {
    List<AdapterDefinition> target = new ArrayList<>();
    for (Definition definition : definitions) {
      AdapterDefinition def = new AdapterDefinition();
      def.setDefinition(definition.getDefinition());
      def.setDesignation(definition.getDesignation());
      def.setLanguage(definition.getLanguage() != null
          ? Language.valueOf(definition.getLanguage().toUpperCase())
          : null);
      target.add(def);
    }
    return target;
  }

  /** TODO: add javadoc. */
  public static DetailedMdrElement convert(
      DSLContext ctx,
      int userId,
      IdentifiedElement element,
      List<SlotDto> slots,
      List<ScopedIdentifier> versions,
      Language priority,
      List<ConceptAssociation> conceptAssociations) {

    if (element == null) {
      throw new ElementNotFoundException();
    }

    DetailedMdrElement detailed = new DetailedMdrElement(element, priority);
    detailed.setSlots(slots);
    detailed.setVersions(versions);
    detailed.setConceptAssociations(conceptAssociations);

    String urn = element.getScoped().toString();

    switch (element.getElementType()) {
      case DATAELEMENT:
        detailed.setValueDomain(createValueDomain(ctx, userId, detailed.getElement(), priority));
        break;
      case DATAELEMENTGROUP:
        detailed.setMembers(convertElements(
            IdentifiedDao.getSubMembers(ctx, userId, urn), priority, true));
        break;
      case RECORD:
        detailed.setMembers(convertElements(
            IdentifiedDao.getEntries(ctx, userId, urn), priority, false));
        break;
      case CATALOG:
      case CODE:
        detailed.setMembers(convertElements(
            IdentifiedDao.getEntries(ctx, userId, urn), priority, false));
        break;
      default:
        break;
    }

    return detailed;
  }

  /** TODO: add javadoc. */
  public static MdrElement convert(IdentifiedElement element, Language priority) {
    if (element == null) {
      throw new RuntimeException("ELEMENT NOT FOUND!");
    }
    return new MdrElement(element, priority);
  }

  /** TODO: add javadoc. */
  public static List<NamespacePermissionDto> convert(
      DSLContext ctx, List<NamespacePermission> permissions) {
    List<NamespacePermissionDto> target = new ArrayList<>();
    for (NamespacePermission p : permissions) {
      NamespacePermissionDto dto = new NamespacePermissionDto();
      User user = UserDao.getUser(ctx, p.getUserId());
      dto.setEmail(user.getEmail());
      dto.setRealName(user.getRealName());
      dto.setUserId(p.getUserId());
      dto.setExternalLabel(user.getExternalLabel());
      dto.setPermission(p.getPermission());
      target.add(dto);
    }

    return target;
  }

  /** Converts the validation data for the given value domain. */
  private static String getValidationData(ValueDomainDto valueDomain) {
    if (valueDomain.getDatatype() == DatatypeField.DATE) {
      return valueDomain.getDateRepresentation().toString()
          + (valueDomain.getWithDays() ? "_WITH_DAYS" : "");
    }

    /*
     * In case of the date format it does not make sense to disable the days.
     */
    if (valueDomain.getDatatype() == DatatypeField.DATETIME) {
      return valueDomain.getDateRepresentation().toString()
          + "_WITH_DAYS"
          + ";"
          + valueDomain.getTimeRepresentation().toString()
          + (valueDomain.getWithSeconds() ? "_WITH_SECONDS" : "");
    }

    if (valueDomain.getDatatype() == DatatypeField.TIME) {
      return valueDomain.getTimeRepresentation().toString()
          + (valueDomain.getWithSeconds() ? "_WITH_SECONDS" : "");
    }

    return null;
  }

  private static String getTimeFormat(ValueDomainDto valueDomain) {
    List<String> parts = new ArrayList<>();
    parts.add("hh");
    parts.add("mm");

    if (valueDomain.getWithSeconds()) {
      parts.add("ss");
    }

    return StringUtil.join(parts, ":");
  }

  /** Returns the selected representation format as String. */
  private static String getDateFormat(ValueDomainDto valueDomain) {
    switch (valueDomain.getDateRepresentation()) {
      case DIN_5008:
        if (valueDomain.getWithDays()) {
          return "DD.MM.YYYY";
        } else {
          return "MM.YYYY";
        }

      case LOCAL_DATE:
        return "YYYY-MM-DD";

      default:
      case ISO_8601:
        if (valueDomain.getWithDays()) {
          return "YYYY-MM-DD";
        } else {
          return "YYYY-MM";
        }
    }
  }

  /** Creates a ValueDomainDto for the given identified element. */
  public static ValueDomainDto createValueDomain(
      DSLContext ctx, int userId, IdentifiedElement element, Language priority)  {
    DataElement de = (DataElement) element.getElement();
    return convert(
        ctx,
        userId,
        (ValueDomain) ElementDao.getElement(ctx, de.getValueDomainId()),
        element.getScoped(),
        priority);
  }

  /** Convert the given slots into SlotDTOs. */
  public static List<SlotDto> convertSlots(List<Slot> slots) {
    List<SlotDto> target = new ArrayList<>();

    for (de.samply.mdr.dal.dto.Slot s : slots) {
      SlotDto slot = new SlotDto();
      slot.setName(s.getKey());
      slot.setValue(s.getValue());
      target.add(slot);
    }

    return target;
  }

  /** Convert the list of namespaces to MdrNamespaces with the given language. */
  public static List<MdrNamespace> convertNamespaces(
      List<DescribedElement> namespaces, Language priority) {
    List<MdrNamespace> target = new ArrayList<>();

    for (DescribedElement n : namespaces) {
      target.add(convertNamespace(n, priority));
    }

    return target;
  }

  /** Converts a namespace into an MdrNamespace. */
  public static MdrNamespace convertNamespace(DescribedElement n, Language priority) {
    return new MdrNamespace(n, priority);
  }

  /** TODO: add javadoc. */
  public static List<MdrElement> convertElements(
      List<IdentifiedElement> elements, Language priority, Boolean sort) {
    List<MdrElement> target = new ArrayList<>();
    for (IdentifiedElement e : elements) {
      target.add(Mapper.convert(e, priority));
    }

    if (sort) {
      Collections.sort(target, elementComparator);
    }
    return target;
  }

  /** TODO: add javadoc. */
  public static List<NamespacePermission> convertPermissions(
      List<NamespacePermissionDto> permissions, Namespace ns) {
    ArrayList<NamespacePermission> target = new ArrayList<>();
    for (NamespacePermissionDto dto : permissions) {
      NamespacePermission permission = new NamespacePermission();
      permission.setPermission(dto.getPermission());
      permission.setUserId(dto.getUserId());
      permission.setNamespaceId(ns.getId());
      target.add(permission);
    }
    return target;
  }
}
