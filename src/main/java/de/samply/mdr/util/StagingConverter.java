/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.util;

import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.HierarchyDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.jooq.Tables;
import de.samply.mdr.dal.jooq.enums.RelationType;
import de.samply.mdr.gui.SlotDto;
import de.samply.mdr.gui.ValueDomainDto;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.Constants;
import de.samply.mdr.xsd.staging.BooleanValidation;
import de.samply.mdr.xsd.staging.CalendarValidation;
import de.samply.mdr.xsd.staging.CatalogValidation;
import de.samply.mdr.xsd.staging.ConceptAssociationType;
import de.samply.mdr.xsd.staging.ConceptAssociations;
import de.samply.mdr.xsd.staging.DateFormatString;
import de.samply.mdr.xsd.staging.DefinitionType;
import de.samply.mdr.xsd.staging.Definitions;
import de.samply.mdr.xsd.staging.FloatValidation;
import de.samply.mdr.xsd.staging.IntegerValidation;
import de.samply.mdr.xsd.staging.PermittedValueType;
import de.samply.mdr.xsd.staging.PermittedValuesValidation;
import de.samply.mdr.xsd.staging.SlotType;
import de.samply.mdr.xsd.staging.Slots;
import de.samply.mdr.xsd.staging.StringValidation;
import de.samply.mdr.xsd.staging.TbdValidation;
import de.samply.mdr.xsd.staging.ValidationType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.jooq.DSLContext;

/**
 * Converters to display Staged Elements. StageElements are received via REST api or excel import
 * and have a different layout. So some minor modifications have to be made to display them.
 */
public class StagingConverter {

  /**
   * Convert the slots from the xsd staging schema to the internal slot representations.
   *
   * @param slots xsd format slots
   * @return list of slotDtos in the internal representation
   */
  public static List<SlotDto> convertSlots(Slots slots) {
    List<SlotDto> slotDtos = new ArrayList<>();

    if (slots != null) {
      for (SlotType slot : slots.getSlot()) {
        SlotDto slotDto = new SlotDto();
        slotDto.setName(slot.getKey());
        slotDto.setValue(slot.getValue());
        slotDtos.add(slotDto);
      }
    }

    return slotDtos;
  }

  /**
   * Convert the conceptAssociation from the xsd staging schema to the internal conceptAssociation
   * representations.
   *
   * @param conceptAssociations xsd format slots
   * @return list of slotDtos in the internal representation
   */
  public static List<ConceptAssociation> convertConceptAssociations(
      ConceptAssociations conceptAssociations) {
    List<ConceptAssociation> conceptAssociationDtos = new ArrayList<>();

    if (conceptAssociations != null) {
      for (ConceptAssociationType conceptAssociation : conceptAssociations
          .getConceptAssociation()) {
        ConceptAssociation conceptAssociationDto = new ConceptAssociation();
        conceptAssociationDto.setSystem(conceptAssociation.getSystem());
        conceptAssociationDto.setVersion(conceptAssociation.getVersion());
        conceptAssociationDto.setTerm(conceptAssociation.getTerm());
        conceptAssociationDto.setText(conceptAssociation.getText());
        conceptAssociationDto
            .setLinktype(RelationType.valueOf(conceptAssociation.getLinktype().value()));

        try (DSLContext ctx = ResourceManager.getDslContext()) {
          conceptAssociationDto.setSourceId(
              ctx.fetchOne(Tables.SOURCE, Tables.SOURCE.NAME.eq("Samply MDR")).getId());
        }

        conceptAssociationDtos.add(conceptAssociationDto);
      }
    }

    return conceptAssociationDtos;
  }

  /**
   * Convert the definitions from the xsd staging schema to the internal adapter definitions.
   *
   * @param definitions xsd format definitions
   * @return list of adapter definitions in the internal representation
   */
  public static List<AdapterDefinition> convertDefinitions(Definitions definitions) {
    List<AdapterDefinition> adapterDefinitions = new ArrayList<>();

    if (definitions != null) {
      for (DefinitionType definition : definitions.getDefinition()) {
        AdapterDefinition adapterDefinition = new AdapterDefinition();
        adapterDefinition.setLanguage(
            Constants.Language.valueOf(definition.getLanguage().value().toUpperCase()));
        adapterDefinition.setDefinition(definition.getDefinition());
        adapterDefinition.setDesignation(definition.getDesignation());
        adapterDefinitions.add(adapterDefinition);
      }
    }

    return adapterDefinitions;
  }

  /**
   * Convert the validation information from the xsd staging schema to the internal valuedomain.
   * representation
   *
   * @param validation xsd format validation type
   * @return internal valueDomain representation
   */
  public static ValueDomainDto convertValueDomain(int userId, ValidationType validation) {
    ValueDomainDto valueDomain = new ValueDomainDto();
    if (validation instanceof StringValidation) {
      StringValidation sv = (StringValidation) validation;
      valueDomain.setDatatype(Constants.DatatypeField.STRING);
      valueDomain.setMaxLength(sv.getMaxLength().intValue());
      if (valueDomain.getMaxLength() > 0) {
        valueDomain.setUseMaxLength(true);
      }

      if (sv.getRegex() != null && sv.getRegex().length() > 0) {
        valueDomain.setUseRegex(true);
        valueDomain.setFormat(sv.getRegex());
      } else {
        valueDomain.setUseRegex(false);
      }
    } else if (validation instanceof BooleanValidation) {
      valueDomain.setDatatype(Constants.DatatypeField.BOOLEAN);
    } else if (validation instanceof TbdValidation) {
      valueDomain.setDatatype(Constants.DatatypeField.TBD);
    } else if (validation instanceof FloatValidation) {
      FloatValidation fv = (FloatValidation) validation;
      valueDomain.setDatatype(Constants.DatatypeField.FLOAT);
      valueDomain.setUnitOfMeasure(fv.getUnitOfMeasure());
      valueDomain.setWithinRange(fv.getRangeFrom() != null || fv.getRangeTo() != null);

      if (fv.getRangeFrom() != null) {
        valueDomain.setUseRangeFrom(true);
        valueDomain.setRangeFrom(fv.getRangeFrom());
      } else {
        valueDomain.setUseRangeFrom(false);
      }
      if (fv.getRangeTo() != null) {
        valueDomain.setUseRangeTo(true);
        valueDomain.setRangeTo(fv.getRangeTo());
      } else {
        valueDomain.setUseRangeTo(false);
      }
    } else if (validation instanceof IntegerValidation) {
      IntegerValidation iv = (IntegerValidation) validation;
      valueDomain.setDatatype(Constants.DatatypeField.INTEGER);
      valueDomain.setUnitOfMeasure(iv.getUnitOfMeasure());
      valueDomain.setWithinRange(iv.getRangeFrom() != null || iv.getRangeTo() != null);

      if (iv.getRangeFrom() != null) {
        valueDomain.setUseRangeFrom(true);
        valueDomain.setRangeFrom(iv.getRangeFrom());
      } else {
        valueDomain.setUseRangeFrom(false);
      }
      if (iv.getRangeTo() != null) {
        valueDomain.setUseRangeTo(true);
        valueDomain.setRangeTo(iv.getRangeTo());
      } else {
        valueDomain.setUseRangeTo(false);
      }
    } else if (validation instanceof CatalogValidation) {
      CatalogValidation cv = (CatalogValidation) validation;
      valueDomain.setDatatype(Constants.DatatypeField.CATALOG);
      valueDomain.setCatalog(cv.getCatalogUrn());

      try (DSLContext ctx = ResourceManager.getDslContext()) {
        IdentifiedElement catalog = IdentifiedDao.getElement(ctx, userId, cv.getCatalogUrn());

        // TODO: get language instead of setting fixed one
        valueDomain.setCatalogRoot(new TreeNode(Mapper.convert(catalog, Constants.Language.EN)));

        HashMap<Integer, TreeNode> index = new HashMap<>();
        index.put(catalog.getScoped().getId(), valueDomain.getCatalogRoot());

        HashMap<String, TreeNode> urnIndex = new HashMap<>();
        urnIndex.put(valueDomain.getCatalog(), valueDomain.getCatalogRoot());

        for (IdentifiedElement element :
            IdentifiedDao.getAllSubMembers(ctx, userId, valueDomain.getCatalog())) {
          index.put(
              element.getScoped().getId(),
              new TreeNode(Mapper.convert(element, Constants.Language.EN)));
          urnIndex.put(element.getScoped().toString(), index.get(element.getScoped().getId()));
        }

        valueDomain.setSubCodes(
            Mapper.convertElements(
                IdentifiedDao.getEntries(ctx, userId, valueDomain.getCatalog()),
                Constants.Language.EN,
                false));

        for (HierarchyNode node : HierarchyDao.getHierarchyNodes(ctx, valueDomain.getCatalog())) {
          index.get(node.getSuperId()).getChildren().add(index.get(node.getSubId()));
          index.get(node.getSubId()).getParents().add(index.get(node.getSuperId()));
        }

        valueDomain.setUrnIndex(urnIndex);
        valueDomain.setIndex(index);
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
      }
    } else if (validation instanceof CalendarValidation) {
      CalendarValidation cv = (CalendarValidation) validation;
      DateFormatString dfs = cv.getDateFormat();
      valueDomain.setFormat(dfs.value());
      valueDomain.setWithDays(isWithDays(dfs));
      valueDomain.setWithSeconds(isWithSeconds(dfs));
      if (isDate(dfs)) {
        valueDomain.setDatatype(Constants.DatatypeField.DATE);
        valueDomain.setDateRepresentation(convertDateRepresentation(dfs));
      } else if (isTime(dfs)) {
        valueDomain.setDatatype(Constants.DatatypeField.TIME);
        valueDomain.setTimeRepresentation(convertTimeRepresentation(dfs));
      } else {
        valueDomain.setDatatype(Constants.DatatypeField.DATETIME);
        valueDomain.setDateRepresentation(convertDateRepresentation(dfs));
        valueDomain.setTimeRepresentation(convertTimeRepresentation(dfs));
      }
    } else if (validation instanceof PermittedValuesValidation) {
      PermittedValuesValidation pvv = (PermittedValuesValidation) validation;
      valueDomain.setDatatype(Constants.DatatypeField.LIST);
      for (PermittedValueType pv : pvv.getPermittedValue()) {
        AdapterPermissibleValue value = new AdapterPermissibleValue();
        value.setValue(pv.getValue());
        value.getDefinitions().addAll(convertDefinitions(pv.getDefinitions()));
        valueDomain.getPermittedValues().add(value);
      }
    } else {
      // noop
    }
    return valueDomain;
  }

  /**
   * Check whether or not a given dateformat only contains date.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return true if the format only includes date
   */
  private static boolean isDate(DateFormatString dateFormatString) {
    return dateFormatString == DateFormatString.YYYY_MM
        || dateFormatString == DateFormatString.YYYY_MM_DD
        || dateFormatString == DateFormatString.MM_YYYY
        || dateFormatString == DateFormatString.DD_MM_YYYY;
  }

  /**
   * Check whether or not a given dateformat only contains time.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return true if the format only includes time
   */
  private static boolean isTime(DateFormatString dateFormatString) {
    return dateFormatString == DateFormatString.HH_MM
        || dateFormatString == DateFormatString.HH_MM_SS
        || dateFormatString == DateFormatString.HH_MM_A
        || dateFormatString == DateFormatString.HH_MM_SS_A;
  }

  /**
   * Check whether or not a given dateformat contains date and time.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return true if the format includes both date and time
   */
  private static boolean isDateTime(DateFormatString dateFormatString) {
    return !isDate(dateFormatString) && !isTime(dateFormatString);
  }

  /**
   * Check whether or not a given dateformat contains days.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return true if days are included
   */
  private static boolean isWithDays(DateFormatString dateFormatString) {
    return isDateTime(dateFormatString)
        || dateFormatString == DateFormatString.YYYY_MM_DD
        || dateFormatString == DateFormatString.DD_MM_YYYY;
  }

  /**
   * Check whether or not a given dateformat contains seconds.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return true if seconds are included
   */
  private static boolean isWithSeconds(DateFormatString dateFormatString) {
    return dateFormatString == DateFormatString.HH_MM_SS
        || dateFormatString == DateFormatString.HH_MM_SS_A
        || dateFormatString == DateFormatString.YYYY_MM_DD_HH_MM_SS
        || dateFormatString == DateFormatString.YYYY_MM_DD_HH_MM_SS_A
        || dateFormatString == DateFormatString.DD_MM_YYYY_HH_MM_SS
        || dateFormatString == DateFormatString.DD_MM_YYYY_HH_MM_SS_A;
  }

  /**
   * Convert the time representation from the xsd format to the internal mdr format.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return the mdr internal time representation
   */
  private static Constants.TimeRepresentation convertTimeRepresentation(
      DateFormatString dateFormatString) {
    switch (dateFormatString) {
      case HH_MM_SS:
      case HH_MM:
      case YYYY_MM_DD_HH_MM_SS:
      case YYYY_MM_DD_HH_MM:
      case DD_MM_YYYY_HH_MM_SS:
      case DD_MM_YYYY_HH_MM:
        return Constants.TimeRepresentation.HOURS_24;
      case HH_MM_SS_A:
      case HH_MM_A:
      case YYYY_MM_DD_HH_MM_SS_A:
      case YYYY_MM_DD_HH_MM_A:
      case DD_MM_YYYY_HH_MM_SS_A:
      case DD_MM_YYYY_HH_MM_A:
        return Constants.TimeRepresentation.HOURS_12;
      case YYYY_MM_DD:
      case YYYY_MM:
      case DD_MM_YYYY:
      case MM_YYYY:
      default:
        return Constants.TimeRepresentation.LOCAL_TIME;
    }
  }

  /**
   * Convert the date representation from the xsd format to the internal mdr format.
   *
   * @param dateFormatString the datetime format in the xsd format
   * @return the mdr internal date representation
   */
  private static Constants.DateRepresentation convertDateRepresentation(
      DateFormatString dateFormatString) {
    switch (dateFormatString) {
      case YYYY_MM_DD_HH_MM_SS:
      case YYYY_MM_DD_HH_MM:
      case YYYY_MM_DD_HH_MM_SS_A:
      case YYYY_MM_DD_HH_MM_A:
      case YYYY_MM_DD:
      case YYYY_MM:
        return Constants.DateRepresentation.ISO_8601;
      case DD_MM_YYYY_HH_MM_SS:
      case DD_MM_YYYY_HH_MM:
      case DD_MM_YYYY_HH_MM_SS_A:
      case DD_MM_YYYY_HH_MM_A:
      case DD_MM_YYYY:
      case MM_YYYY:
        return Constants.DateRepresentation.DIN_5008;
      case HH_MM_SS_A:
      case HH_MM_A:
      case HH_MM:
      case HH_MM_SS:
      default:
        return Constants.DateRepresentation.LOCAL_DATE;
    }
  }
}
