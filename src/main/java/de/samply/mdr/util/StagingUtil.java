/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.util;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.samply.mdr.dal.ResourceManager;
import de.samply.mdr.dal.dao.ConceptAssociationDao;
import de.samply.mdr.dal.dao.DefinitionDao;
import de.samply.mdr.dal.dao.ElementDao;
import de.samply.mdr.dal.dao.IdentifiedDao;
import de.samply.mdr.dal.dao.ImportDao;
import de.samply.mdr.dal.dao.NamespaceDao;
import de.samply.mdr.dal.dao.PermissibleCodeDao;
import de.samply.mdr.dal.dao.ScopedIdentifierDao;
import de.samply.mdr.dal.dao.ScopedIdentifierHierarchyDao;
import de.samply.mdr.dal.dao.SlotDao;
import de.samply.mdr.dal.dao.StagingDao;
import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.ConceptAssociation;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.DataElementGroup;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.Import;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleCode;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Slot;
import de.samply.mdr.dal.dto.Staging;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dal.jooq.enums.Elementtype;
import de.samply.mdr.dal.jooq.enums.Status;
import de.samply.mdr.gui.BeanValidators;
import de.samply.mdr.gui.MdrNamespace;
import de.samply.mdr.gui.RefreshThread;
import de.samply.mdr.gui.SlotDto;
import de.samply.mdr.gui.ValueDomainDto;
import de.samply.mdr.gui.beans.TreeNode;
import de.samply.mdr.gui.exceptions.ValidationException;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.AdapterPermissibleValue;
import de.samply.mdr.lib.Constants;
import de.samply.mdr.model.AttributesList;
import de.samply.mdr.model.Itree;
import de.samply.mdr.model.StagingTreeNode;
import de.samply.mdr.model.State;
import de.samply.mdr.staging.model.adapter.ValidationTypeAdapter;
import de.samply.mdr.staging.model.enums.DateTimeFormat;
import de.samply.mdr.staging.model.source.excel.Dataelement;
import de.samply.mdr.staging.model.source.excel.Info;
import de.samply.mdr.staging.utils.ImportToStagingConverter;
import de.samply.mdr.xsd.staging.StagedElementType;
import de.samply.string.util.StringUtil;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StagingUtil {

  public static final String NODE_PREFIX_NAMESPACE = "ns_";
  public static final String NODE_PREFIX_IMPORT = "im_";
  public static final String ATTRIBUTE_ELEMENTTYPE = "data-elementtype";
  public static final String ATTRIBUTE_RESULTING_ELEMENT_URN = "data-createdurn";
  public static final String ATTRIBUTE_IMPORT_CONVERTED_AT = "data-convertedat";
  private static final Logger logger = LoggerFactory.getLogger(StagingUtil.class);
  private Gson gson;

  /** TODO: add javadoc. */
  public StagingUtil() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeHierarchyAdapter(
        de.samply.mdr.xsd.staging.ValidationType.class, new ValidationTypeAdapter());
    gsonBuilder.disableHtmlEscaping();
    gson = gsonBuilder.create();
  }

  /** TODO: add javadoc. */
  public static String createInitialTree(List<String> namespaceNames) {
    List<StagingTreeNode> rootNodeList = new ArrayList<>();

    for (String namespace : namespaceNames) {
      createTreeNode(rootNodeList, namespace, NODE_PREFIX_NAMESPACE + namespace, true);
    }
    // Make namespaces non-selectable
    for (StagingTreeNode rootNode : rootNodeList) {
      Itree rootNodeItree = rootNode.getItree();
      if (rootNodeItree == null) {
        rootNodeItree = new Itree();
      }
      State rootNodeItreeState = rootNodeItree.getState();
      if (rootNodeItreeState == null) {
        rootNodeItreeState = new State();
      }
      rootNodeItreeState.setSelectable(false);
      rootNodeItree.setState(rootNodeItreeState);
      rootNode.setItree(rootNodeItree);
    }

    return new Gson().toJson(rootNodeList);
  }

  /** TODO: add javadoc. */
  public static String createImportTreeNodes(List<Import> imports) {
    List<StagingTreeNode> nodeList = new ArrayList<>();

    for (Import theImport : imports) {
      String nodeText = getImportDisplayname(theImport);
      Map<String, String> attributeMap = new HashMap<>();
      attributeMap.put(ATTRIBUTE_ELEMENTTYPE, "import");
      if (theImport.getConvertedAt() != null) {
        attributeMap.put(
            ATTRIBUTE_IMPORT_CONVERTED_AT,
            DateTimeFormat.DATETIME_ISO8601_DAYS_24H_NO_SECONDS
                .getFormat()
                .format(theImport.getConvertedAt().getTime()));
      }
      createTreeNode(
          nodeList,
          nodeText,
          NODE_PREFIX_IMPORT + theImport.getId(),
          true,
          attributeMap);
    }

    return new Gson().toJson(nodeList);
  }

  /** TODO: add javadoc. */
  public static String createTreeNodes(int userId, List<Staging> rootElements) {
    List<StagingTreeNode> nodeList = new ArrayList<>();

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (Staging element : rootElements) {
        Map<String, String> attributeMap = new HashMap<>();

        if (element.getElementId() != null && element.getElementId() > 0) {
          Import theImport = ImportDao.getImportById(ctx, element.getImportId());
          Namespace namespace = ((Namespace) NamespaceDao
              .getNamespaceById(ctx, userId, theImport.getNamespaceId()).getElement());
          List<ScopedIdentifier> scopedIdentifiers =
              ScopedIdentifierDao.getScopedIdentifierForElementById(
                  ctx, element.getElementId(), namespace.getName());
          attributeMap.put(
              ATTRIBUTE_RESULTING_ELEMENT_URN,
              getLatestScopedIdentifier(scopedIdentifiers).toString());
        }

        if (element.getElementType().equals(Elementtype.DATAELEMENT)) {
          attributeMap.put(ATTRIBUTE_ELEMENTTYPE, "dataelement");
          createTreeNode(
              nodeList,
              element.getDesignation(),
              Integer.toString(element.getId()),
              false,
              attributeMap);
        } else {
          if (element.getElementType().equals(Elementtype.DATAELEMENTGROUP)) {
            attributeMap.put(ATTRIBUTE_ELEMENTTYPE, "dataelementgroup");
            createTreeNode(
                nodeList,
                element.getDesignation(),
                Integer.toString(element.getId()),
                true,
                attributeMap);
          } else if (element.getElementType().equals(Elementtype.RECORD)) {
            attributeMap.put(ATTRIBUTE_ELEMENTTYPE, "record");
            createTreeNode(
                nodeList,
                element.getDesignation(),
                Integer.toString(element.getId()),
                true,
                attributeMap);
          }
        }
      }
    }

    return new Gson().toJson(nodeList);
  }

  private static ScopedIdentifier getLatestScopedIdentifier(
      List<ScopedIdentifier> scopedIdentifiers) {
    return Collections.max(
        scopedIdentifiers,
        new Comparator<ScopedIdentifier>() {
          @Override
          public int compare(ScopedIdentifier scopedIdentifier, ScopedIdentifier t1) {
            return Integer.compare(scopedIdentifier.getId(), t1.getId());
          }
        });
  }

  private static void createTreeNode(
      List<StagingTreeNode> rootNodeList, String name, String id, boolean children) {
    createTreeNode(rootNodeList, name, id, children, null, null);
  }

  private static void createTreeNode(
      List<StagingTreeNode> rootNodeList,
      String name,
      String id,
      boolean children,
      Map<String, String> anchorAttributes) {
    createTreeNode(rootNodeList, name, id, children, anchorAttributes, null);
  }

  private static void createTreeNode(
      List<StagingTreeNode> rootNodeList,
      String name,
      String id,
      boolean children,
      Map<String, String> anchorAttributes,
      Map<String, String> listItemAttributes) {
    StagingTreeNode groupNode = new StagingTreeNode();
    groupNode.setText(name);
    groupNode.setId(id);
    groupNode.setChildren(children);

    if (anchorAttributes != null || listItemAttributes != null) {
      Itree itree = new Itree();

      if (anchorAttributes != null) {
        AttributesList list = new AttributesList();
        for (Map.Entry<String, String> anchorAttribute : anchorAttributes.entrySet()) {
          list.addAttribute(anchorAttribute.getKey(), anchorAttribute.getValue());
        }
        itree.setAnchorAttributes(list);
      }

      if (listItemAttributes != null) {
        AttributesList liList = new AttributesList();
        for (Map.Entry<String, String> liAttribute : listItemAttributes.entrySet()) {
          liList.addAttribute(liAttribute.getKey(), liAttribute.getValue());
        }
        itree.setListItemAttributes(liList);
      }

      // Make everything but imports unselectable
      State state = new State();
      if (anchorAttributes != null
          && !"import".equalsIgnoreCase(anchorAttributes.get(ATTRIBUTE_ELEMENTTYPE))) {
        state.setSelectable(false);
      }
      itree.setState(state);

      groupNode.setItree(itree);
    }

    rootNodeList.add(groupNode);
  }

  /**
   * Get a map with query parameters from an URL.
   *
   * @param queryString e.g. http://www.foo.bar?p1=foo&p2=bar
   * @return a map with the query parameters. e.g. "p1"->"foo", "p2"->"bar"
   */
  public static Map<String, String> getQueryParameters(String queryString) {
    String decodedQueryString;
    try {
      decodedQueryString = URLDecoder.decode(queryString, Charsets.UTF_8.name());
    } catch (UnsupportedEncodingException e) {
      decodedQueryString = queryString;
    }

    Map<String, String> queryParameterMap = new HashMap<>();

    String[] tokens = decodedQueryString.split("[\\?&]+");

    for (String token : tokens) {
      String[] keyValueSplit = token.split("=");
      if (keyValueSplit != null && keyValueSplit.length == 2) {
        queryParameterMap.put(keyValueSplit[0], keyValueSplit[1]);
      }
    }

    return queryParameterMap;
  }

  /** TODO: add javadoc. */
  public static String getImportDisplayname(Import theImport) {
    StringBuilder displayName = new StringBuilder();
    String timestampString =
        DateTimeFormat.DATETIME_ISO8601_DAYS_24H_NO_SECONDS
            .getFormat()
            .format(theImport.getCreatedAt().getTime());
    displayName.append(timestampString);
    if (theImport.getLabel() != null && !theImport.getLabel().isEmpty()) {
      displayName.append(": ").append(theImport.getLabel());
    }
    return displayName.toString();
  }

  /** Store a list of excel dataelements as staged elements. */
  public void stageImport(
      Info info,
      Collection<Dataelement> dataelements,
      int userId,
      String namespaceUrn,
      int fallbackNamespaceId) {
    Map<Integer, Integer> idMap = new HashMap<>();
    int namespaceId = fallbackNamespaceId;

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      if (!StringUtil.isEmpty(namespaceUrn)) {
        try {
          DescribedElement namespace = NamespaceDao.getNamespace(ctx, userId, namespaceUrn);
          if (namespace != null) {
            namespaceId = namespace.getElement().getId();
          }
        } catch (Exception e) {
          logger.info("Namespace from Sheet not found. Using fallback namespace");
        }
      }

      Import theImport = new Import();
      theImport.setNamespaceId(namespaceId);
      theImport.setSource("excel");
      theImport.setCreatedBy(userId);
      theImport.setLabel(info.getImportLabel());
      theImport = insertImport(theImport, userId);

      for (Dataelement dataelement : dataelements) {
        logger.debug("Staging element: " + dataelement.getDefinitions());
        Map<Integer, List<Staging>> stagingDtos =
            ImportToStagingConverter.excelDataelementToStaging(theImport, dataelement);

        Iterator iterator = stagingDtos.entrySet().iterator();
        ctx.connection(c -> c.setAutoCommit(false));
        while (iterator.hasNext()) {
          Map.Entry<Integer, List<Staging>> pair =
              (Map.Entry<Integer, List<Staging>>) iterator.next();
          StagingDao.saveStagedElements(ctx, pair.getValue(), idMap);
          iterator.remove();
        }
        ctx.connection(Connection::commit);
      }
    } catch (Exception e) {
      logger.error("Import failed: " + e.getMessage());
    }
  }

  /** TODO: add javadoc. */
  public void persistStagedElements(
      Collection<Staging> stagingCollection, int userId, boolean releaseImmediately) {
    // Map the ids of the staged element to their newly assigned urns and the newly assigned urns
    // of group members to the staging ids of their parents staged elements.
    // This allows to assign the groups later.
    Map<Integer, ScopedIdentifier> idMap = new HashMap<>();
    Map<ScopedIdentifier, Integer> ancestryMap = new HashMap<>();

    for (Staging stagedElement : stagingCollection) {
      Import theImport = getImportById(stagedElement.getImportId());
      ScopedIdentifier assignedId =
          persistStagedElement(theImport, stagedElement, releaseImmediately);
      if (assignedId != null) {
        idMap.put(stagedElement.getId(), assignedId);
        ancestryMap.put(assignedId, stagedElement.getParentId());
        updateStagedElement(stagedElement.getId(), assignedId);
        logger.info("Saved new element: " + assignedId);
      } else {
        logger.error("Could not save staged element. " + stagedElement.getId());
      }
    }
    // Now "merge" the maps, so they contain the group identifier as keys and the members as a
    // list
    Map<ScopedIdentifier, List<ScopedIdentifier>> groups = new HashMap<>();
    Iterator<Map.Entry<ScopedIdentifier, Integer>> it = ancestryMap.entrySet().iterator();

    while (it.hasNext()) {
      Map.Entry<ScopedIdentifier, Integer> entry = it.next();
      ScopedIdentifier groupKey = idMap.get(entry.getValue());
      ScopedIdentifier elementKey = entry.getKey();

      if (groupKey == null) {
        it.remove();
        continue;
      }

      if (groups.get(groupKey) == null) {
        groups.put(groupKey, new ArrayList<>());
      }

      groups.get(groupKey).add(elementKey);
      it.remove();
    }

    Iterator<Map.Entry<ScopedIdentifier, List<ScopedIdentifier>>> grpIterator =
        groups.entrySet().iterator();

    while (grpIterator.hasNext()) {
      Map.Entry<ScopedIdentifier, List<ScopedIdentifier>> grpEntry = grpIterator.next();
      addGroupMembers(grpEntry.getKey(), grpEntry.getValue());
      grpIterator.remove();
    }
  }

  /** TODO: add javadoc. */
  public ScopedIdentifier persistStagedElement(
      Import theImport, Staging stagedElement, boolean releaseImmediately) {
    ScopedIdentifier identifier = null;
    if (stagedElement.getElementType() == Elementtype.DATAELEMENT) {
      identifier =
          saveImportedDataelement(
              gson.fromJson(
                  stagedElement.getData(), de.samply.mdr.xsd.staging.StagedElementType.class),
              theImport.getCreatedBy(),
              theImport.getNamespaceId(),
              releaseImmediately);
    } else if (stagedElement.getElementType() == Elementtype.DATAELEMENTGROUP) {
      identifier =
          saveGroup(
              gson.fromJson(
                  stagedElement.getData(), de.samply.mdr.xsd.staging.StagedElementType.class),
              theImport.getCreatedBy(),
              theImport.getNamespaceId(),
              releaseImmediately);
    } else if (stagedElement.getElementType() == Elementtype.RECORD) {
      identifier =
          saveRecord(
              gson.fromJson(
                  stagedElement.getData(), de.samply.mdr.xsd.staging.StagedElementType.class),
              theImport.getCreatedBy(),
              theImport.getNamespaceId(),
              releaseImmediately);
    }
    return identifier;
  }

  /** TODO: add javadoc. */
  public int saveImportedDataelements(
      Collection<de.samply.mdr.xsd.staging.StagedElementType> dataelements,
      int userId,
      int namespaceId,
      boolean releaseImmediately) {
    int errors = 0;
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      for (de.samply.mdr.xsd.staging.StagedElementType dataelement : dataelements) {
        if (dataelement.getElementType() != null
            && dataelement
            .getElementType()
            .equals(de.samply.mdr.xsd.staging.ElementType.DATAELEMENT)) {
          if (null
              == saveImportedDataelement(dataelement, userId, namespaceId, releaseImmediately)) {
            errors++;
          }
        } else if (dataelement
            .getElementType()
            .equals(de.samply.mdr.xsd.staging.ElementType.DATAELEMENTGROUP)) {
          if (null
              == saveImportedDataelement(dataelement, userId, namespaceId, releaseImmediately)) {
            errors++;
          }
        } else {
          // TODO
        }
      }
    }
    return errors;
  }

  /** TODO: add javadoc. */
  public ScopedIdentifier saveImportedDataelement(
      StagedElementType stagedElement,
      int userId,
      int namespaceId,
      boolean releaseImmediately) {
    ValueDomainDto valueDomain =
        StagingConverter.convertValueDomain(userId, (stagedElement).getValidation());
    List<AdapterDefinition> definitions =
        StagingConverter.convertDefinitions(stagedElement.getDefinitions());
    List<SlotDto> slots = StagingConverter.convertSlots(stagedElement.getSlots());
    List<ConceptAssociation> conceptAssociations = StagingConverter
        .convertConceptAssociations(stagedElement.getConceptAssociations());
    Elementtype elementType = Elementtype.DATAELEMENT;

    try {
      ValueDomain domain = Mapper.convert(valueDomain);

      /*
       * Determine the maxLength of all available permitted values.
       */
      if (domain instanceof EnumeratedValueDomain) {
        int maxLength = 1;
        for (AdapterPermissibleValue dto : valueDomain.getPermittedValues()) {
          if (maxLength < dto.getValue().length()) {
            maxLength = dto.getValue().length();
          }
        }
        domain.setMaxCharacters(maxLength);
      }

      /*
       * Determine the maxLength value for all currently available codes.
       */
      if (domain instanceof CatalogValueDomain) {
        int maxLength = 1;
        for (TreeNode node : valueDomain.getIndex().values()) {
          if (node.getElement().getElement().getElementType() == Elementtype.CODE) {
            Code c = (Code) node.getElement().getElement().getElement();
            int length = c.getCode().length();
            if (node.isSelected() && length > maxLength) {
              maxLength = length;
            }
          }
        }
        domain.setMaxCharacters(maxLength);
      }

      try (DSLContext ctx = ResourceManager.getDslContext()) {
        ctx.connection(c -> c.setAutoCommit(false));

        /*
         * Save the value domain first.
         */
        ElementDao.saveElement(ctx, userId, domain);

        DataElement element = new DataElement();
        element.setValueDomainId(domain.getId());

        ElementDao.saveElement(ctx, userId, element);

        DescribedElement namespaceElement =
            NamespaceDao.getNamespaceById(ctx, userId, namespaceId);
        MdrNamespace mdrNamespace = new MdrNamespace(namespaceElement, Constants.Language.EN);

        ScopedIdentifier identifier =
            saveScopedIdentifier(
                ctx,
                userId,
                element.getId(),
                mdrNamespace,
                definitions,
                slots,
                conceptAssociations,
                elementType,
                userId,
                releaseImmediately);

        Namespace namespace = (Namespace) namespaceElement.getElement();

        if (domain instanceof EnumeratedValueDomain) {
          /*
           * For every value we have to check the namespace constraints.
           */
          boolean valid = true;

          for (AdapterPermissibleValue v : valueDomain.getPermittedValues()) {
            PermissibleValue value = new PermissibleValue();
            value.setValueDomainId(domain.getId());
            value.setPermittedValue(v.getValue());

            ElementDao.saveElement(ctx, userId, value);

            valid = valid && BeanValidators.validateValueConstraints(namespace.getLanguages(), v);

            for (AdapterDefinition definition : v.getDefinitions()) {
              Definition def = Mapper.convert(definition);
              def.setElementId(value.getId());
              def.setScopedIdentifierId(identifier.getId());
              DefinitionDao.saveDefinition(ctx, def);
            }
          }

          /*
           * If the namespace constraints are not met, throw an exception to show the error
           * messages.
           */
          if (!valid) {
            throw new ValidationException();
          }
        }

        if (domain instanceof CatalogValueDomain) {
          for (TreeNode node : valueDomain.getIndex().values()) {
            if (node.getState() != TreeNode.ChildrenSelectionState.NONE
                && node.getElement().getElement().getElementType() == Elementtype.CODE) {
              PermissibleCode code = new PermissibleCode();
              code.setCatalogValueDomainId(domain.getId());
              code.setCodeScopedIdentifierId(node.getElement().getElement().getScoped().getId());
              PermissibleCodeDao.savePermissibleCode(ctx, code);
            }
          }
        }
        ctx.connection(c -> c.commit());
        return identifier;
      }
    } catch (ValidationException e) {
      e.printStackTrace();
      Message.addError("serverError");
      return null;
    }
  }

  private String checkIdentifier(DSLContext ctx, int userId, final String identifier) {
    int identifierSuffix = 1;
    String checkedIdentifier = identifier;
    while (IdentifiedDao.getElement(ctx, userId, checkedIdentifier) != null) {
      checkedIdentifier = identifier + "-" + identifierSuffix++;
    }
    return checkedIdentifier;
  }

  /**
   * Creates a new Scoped Identifier for the given values including all definitions and
   * designations.
   */
  private ScopedIdentifier saveScopedIdentifier(
      DSLContext ctx,
      int userId,
      int elementId,
      MdrNamespace mdrNamespace,
      List<AdapterDefinition> definitions,
      List<SlotDto> slots,
      List<ConceptAssociation> conceptAssociations,
      Elementtype elementType,
      int creatorId,
      boolean releaseImmediately)
      throws ValidationException {

    ScopedIdentifier identifier = new ScopedIdentifier();
    identifier.setElementType(elementType);
    identifier.setNamespace(mdrNamespace.getName());
    identifier.setNamespaceId(mdrNamespace.getId());
    identifier.setElementId(elementId);
    identifier.setCreatedBy(creatorId);
    identifier.setUrl("none");

    if (releaseImmediately) {
      identifier.setStatus(Status.RELEASED);
      ScopedIdentifierDao.outdateIdentifiers(ctx, userId, identifier.toString());
    } else {
      identifier.setStatus(Status.DRAFT);
    }

    String newIdentifier =
        ScopedIdentifierDao.getFreeIdentifier(ctx, mdrNamespace.getName(), elementType);
    identifier.setIdentifier(newIdentifier);
    identifier.setVersion("1");

    identifier.setId(ScopedIdentifierDao.saveScopedIdentifier(ctx, identifier));

    /*
     * Save the definitions
     */
    for (AdapterDefinition dto : definitions) {
      Definition definition = Mapper.convert(dto);
      definition.setElementId(elementId);
      definition.setScopedIdentifierId(identifier.getId());
      DefinitionDao.saveDefinition(ctx, definition);
    }

    /*
     * Save the slots again, because slots are bound to scoped
     * identifiers, not elements!
     */
    for (SlotDto s : slots) {
      Slot slot = new Slot();
      slot.setKey(s.getName());
      slot.setValue(s.getValue());
      slot.setScopedIdentifierId(identifier.getId());
      SlotDao.saveSlot(ctx, slot);
    }

    /*
     * Save the conceptAssociations again, because conceptAssociations are bound to scoped
     * identifiers, not elements!
     */
    if (conceptAssociations != null) {
      for (ConceptAssociation ca : conceptAssociations) {
        ca.setScopedIdentifierId(identifier.getId());
        ConceptAssociationDao.saveConceptAssociation(ctx, ca, userId);
      }
    }

    /*
     * Check the constraints and throw an exception, if the
     * constraints are not met
     */
    Namespace namespace = (Namespace) ElementDao.getElement(ctx, mdrNamespace.getId());

    boolean valid = BeanValidators.validateConstraints(namespace.getLanguages(), definitions);

    valid =
        valid && BeanValidators.validateConstraintsSlots(namespace.getConstraintsSlots(), slots);

    if (!valid) {
      throw new ValidationException();
    }

    return identifier;
  }

  private ScopedIdentifier saveGroup(
      StagedElementType stagedElement,
      int userId,
      int namespaceId,
      boolean releaseImmediately) {
    Elementtype elementType = Elementtype.DATAELEMENTGROUP;
    List<AdapterDefinition> definitions =
        StagingConverter.convertDefinitions(stagedElement.getDefinitions());
    List<SlotDto> slots = StagingConverter.convertSlots(stagedElement.getSlots());

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      DescribedElement namespace = NamespaceDao.getNamespaceById(ctx, userId, namespaceId);
      DataElementGroup group = new DataElementGroup();
      ElementDao.saveElement(ctx, userId, group);
      MdrNamespace mdrNamespace = new MdrNamespace(namespace, Constants.Language.EN);

      ScopedIdentifier identifier = saveScopedIdentifier(
          ctx,
          userId,
          group.getId(),
          mdrNamespace,
          definitions,
          slots,
          null,
          elementType,
          userId,
          releaseImmediately);
      ctx.connection(Connection::commit);
      return identifier;
    } catch (ValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  private ScopedIdentifier saveRecord(
      StagedElementType stagedElement,
      int userId,
      int namespaceId,
      boolean releaseImmediately) {
    Elementtype elementType = Elementtype.RECORD;
    List<AdapterDefinition> definitions =
        StagingConverter.convertDefinitions(stagedElement.getDefinitions());
    List<SlotDto> slots = StagingConverter.convertSlots(stagedElement.getSlots());

    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      DescribedElement namespace = NamespaceDao.getNamespaceById(ctx, userId, namespaceId);
      Record record = new Record();
      ElementDao.saveElement(ctx, userId, record);
      MdrNamespace mdrNamespace = new MdrNamespace(namespace, Constants.Language.EN);

      ScopedIdentifier identifier = saveScopedIdentifier(
          ctx,
          userId,
          record.getId(),
          mdrNamespace,
          definitions,
          slots,
          null,
          elementType,
          userId,
          releaseImmediately);
      ctx.connection(Connection::commit);
      return identifier;
    } catch (ValidationException e) {
      e.printStackTrace();
      return null;
    }
  }

  private void addGroupMembers(
      ScopedIdentifier groupIdentifier,
      Collection<ScopedIdentifier> memberIdentifiers) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      for (ScopedIdentifier elementIdentifier : memberIdentifiers) {
        ScopedIdentifierHierarchyDao
            .addSubIdentifier(ctx, groupIdentifier.getId(), elementIdentifier.getId());
      }
      ctx.connection(Connection::commit);
      refreshViews();
    }
  }

  private String increaseIdentifierSuffix(String identifierString) {
    if (StringUtil.isEmpty(identifierString)) {
      return null;
    }
    String patternString = "^(.+?--)(\\d)$";
    Pattern pattern = Pattern.compile(patternString);
    Matcher matcher = pattern.matcher(identifierString);

    if (matcher.matches()) {
      return matcher.group(1) + (Integer.valueOf(matcher.group(2)) + 1);
    } else {
      return identifierString + "--1";
    }
  }

  /**
   * Called after the element has been created. Stars the newly created element, resets this wizard
   * and redirects the user to the detail view of the created element.
   */
  protected String done() {
    // String urn = getUrn();
    // userBean.star(Mapper.convert(mdr.get(IdentifiedDao.class).getElement(urn),
    //   userBean.getSelectedLanguage()));
    // reset();
    // return "/detail?urn=" + urn + "&faces-redirect=true";
    return "";
  }

  private void updateStagedElement(int stagedElementId, ScopedIdentifier createdElementIdentifier) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      StagingDao.setElementId(ctx, stagedElementId, createdElementIdentifier.getElementId());
    }
  }

  private Import insertImport(final Import theImport, int userId) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      ctx.connection(c -> c.setAutoCommit(false));
      int importId = ImportDao.saveImport(ctx, theImport);
      ctx.connection(Connection::commit);
      return ImportDao.getImportById(ctx, importId);
    }
  }

  private Import getImportById(int importId) {
    try (DSLContext ctx = ResourceManager.getDslContext()) {
      return ImportDao.getImportById(ctx, importId);
    }
  }

  /** Starts the RefreshThread. */
  protected void refreshViews() {
    RefreshThread thread = new RefreshThread();
    thread.start();
  }
}
