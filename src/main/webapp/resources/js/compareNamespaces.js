var SRC_NAMESPACE = '#src_namespace';
var TRG_NAMESPACE = '#trg_namespace';
var LANGUAGE = '#language';
var COMPARISON_TYPE = '#comparison_type';
var COMPARISON = '#comparison';

/* listen on change events for all options */
$(SRC_NAMESPACE).on('change', compareNamespaces);
$(TRG_NAMESPACE).on('change', compareNamespaces);
$(LANGUAGE).on('change', compareNamespaces);
$(COMPARISON_TYPE).on('change', compareNamespaces);

function fetchComparisonValues () {
  return {
    srcNamespace: $(SRC_NAMESPACE).val(),
    trgNamespace: $(TRG_NAMESPACE).val(),
    language: $(LANGUAGE).val(),
    comparisonType: $(COMPARISON_TYPE).val()
  }
}

function buildURL(values) {
  return 'comparison_datasource.xhtml?source_namespace=' + values.srcNamespace
    + '&target_namespace=' + values.trgNamespace
    + '&language=' + values.language
    + '&comparison_type=' + values.comparisonType;
}

function compareNamespaces () {
  var values = fetchComparisonValues();

  if (values.srcNamespace === '' || values.trgNamespace === ''
      || values.language === '' || values.comparisonType === '') {
    // Not everything filled out so do no comparison
    return
  }

  $.getJSON(buildURL(values), handleResponse);
}

function handleResponse(data) {
  var html = '';
  data.forEach(function(c) {
    var ooc = overwriteUndefinedProperties(c.objectOfComparison);
    var rec = overwriteUndefinedProperties(c.recommendation);

    html +=
        '<div class="row">' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.designation + ':</b></div>' +
        '  <div class="col-sm-2">' + ooc.designation + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.similarity + ':</b></div>' +
        '  <div class="col-sm-1">' + similarityBar(rec.designationSimilarity) + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.designation + ':</b></div>' +
        '  <div class="col-sm-2">' + rec.designation + '</div>' +
        '</div>' +
        '<div class="row">' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.definition + ':</b></div>' +
        '  <div class="col-sm-2">' + ooc.definition + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.similarity + ':</b></div>' +
        '  <div class="col-sm-1">' + similarityBar(rec.definitionSimilarity) + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.definition + ':</b></div>' +
        '  <div class="col-sm-2">' + rec.definition + '</div>' +
        '</div>' +
        '<div class="row">' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.datatype + ':</b></div>' +
        '  <div class="col-sm-2">' + ooc.type + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.datatypeMatch + ':</b></div>' +
        '  <div class="col-sm-1">' + typeMatchBox(ooc.type, rec.type) + '</div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.datatype + ':</b></div>' +
        '  <div class="col-sm-2">' + rec.type + '</div>' +
        '</div>' +
        '<div class="row">' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.urn + ':</b></div>' +
        '  <div class="col-sm-2">' + urnLink(ooc.urn) + '</div>' +
        '  <div class="col-sm-2 col-sm-offset-1"></div>' +
        '  <div class="col-sm-1 col-sm-offset-1"><b>' + msg.urn + ':</b></div>' +
        '  <div class="col-sm-2">' + urnLink(rec.urn) + '</div>' +
        '</div>' +
        '<hr class="hr-light"/>';
  });
  $(COMPARISON).html(html);
}
