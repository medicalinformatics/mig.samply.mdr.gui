function similarityBar(sim) {
  var inverseSim = 100 - sim;
  var html =
      '<div class="similaritybar" title="' + sim + '%">' +
      '  <div class="similarity pull-right" style="width: ' + inverseSim + '%">' +
      '  </div>' +
      '</div>';
  return html;
}

function typeMatchBox(type1, type2) {
  return type1 === type2
      ? '<div class="label label-success"><i class="fa fa-check"></i></div>'
      : '<div class="label label-danger"><i class="fa fa-close"></i></div>'
}

function urnLink(urn) {
  return urn
      ? '<a href="/detail.xhtml?urn=' + encodeURIComponent(urn) + '" target="_blank">' +
        '  <i class="fa fa-external-link-square"></i> ' + urn +
        '</a>'
      : ''
}

function overwriteUndefinedProperties(obj) {
  var newObj = {};
  $.each(obj, function(key, val) {
    newObj[key] = val ? val : '';
  });
  return newObj;
}
