var elementTemplate = '\
<div class="row">\
  <div class="col-xs-12">\
    <h2>Selected Element:</h2>\
    <br/>\
  </div>\
</div>\
<div class="row">\
  <div class="col-xs-12">\
    <div class="panel panel-{{elementtype}}">\
      <div class="panel-heading">\
        <h3 class="panel-title" style="text-align: left; font-weight: bold">{{path}}</h3>\
      </div>\
      <div class="panel-body">\
        <div class="row form-group">\
          <label for="elementtype" class="col-xs-3 control-label">Element Type</label>\
          <div class="col-xs-9">\
            <input id="elementtype" name="elementtype" class="form-control" disabled="disabled" type="text" value="{{identification.elementtype}}">\
          </div>\
        </div>\
        <div class="row form-group">\
          <label for="status" class="col-xs-3 control-label">Status</label>\
          <div class="col-xs-9">\
            <input id="status" name="status" class="form-control" disabled="disabled" type="text" value="{{identification.status}}">\
          </div>\
        </div>\
        <div class="row form-group">\
          <label for="urn" class="col-xs-3 control-label">URN</label>\
          <div class="col-xs-9">\
            <input id="urn" name="urn" class="form-control" disabled="disabled" type="text" value="{{identification.urn}}">\
          </div>\
        </div>\
        <br/>\
        {{#designations.length}}\
          {{> designationsTemplate}}\
        {{/designations.length}}\
        \
        {{#validation}}\
          {{> validationTemplate}}\
        {{/validation}}\
        \
        {{#slots.length}}\
          {{> slotsTemplate}}\
        {{/slots.length}}\
      </div>\
    </div>\
  </div>\
</div>';

var designationsTemplate = '\
<div class="row">\
  <div class="col-xs-12">\
    <div class="panel panel-default">\
      <div class="panel-heading">\
        <h3 class="panel-title" style="text-align: left; font-weight: bold">Definition</h3>\
      </div>\
      <div class="panel-body">\
        <table class="table table-striped table-hover table-condensed">\
          <thead>\
          <tr>\
            <th scope="col" class="col-xs-1">Language</th>\
            <th scope="col" class="col-xs-4">Designation</th>\
            <th scope="col" class="col-xs-7">Definition</th>\
          </tr>\
          </thead>\
          <tbody>\
          {{#designations}}\
            <tr class="tableVCenter">\
              <td>\
                {{language}}\
              </td>\
              <td>\
                {{designation}}\
              </td>\
              <td>\
                {{definition}}\
              </td>\
            </tr>\
          {{/designations}}\
          </tbody>\
        </table>\
      </div>\
    </div>\
  </div>\
</div>';

var validationTemplate = '\
<div class="row">\
  <div class="col-xs-12">\
    <div class="panel panel-default">\
      <div class="panel-heading">\
        <h3 class="panel-title" style="text-align: left; font-weight: bold">Validation</h3>\
      </div>\
      <div class="panel-body">\
        <div class="row form-group">\
          <label for="validationtype" class="col-xs-3 control-label">Validation Type</label>\
          <div class="col-xs-9">\
            <input id="validationtype" type="text" class="form-control" disabled="disabled" value="{{validation.datatype}}">\
          </div>\
        </div>\
        {{> validationTypeTemplate}}\
      </div>\
    </div>\
  </div>\
</div>';

// Currently boolean and TBD validations don't need a template.

var numericValidationTemplate = '\
<!-- the range should probably only be rendered if it differs from "x" -->\
  <div class="row form-group">\
    <label for="num-range" class="col-xs-3 control-label">Range</label>\
    <div class="col-xs-9">\
      <input id="num-range" name="num-range" class="form-control" disabled="disabled" type="text" value="{{validation.format}}">\
    </div>\
  </div>\
<div class="row form-group">\
  <label for="unitOfMeasure" value="#{msg.unitOfMeasure}" class="col-xs-3 control-label">Unit of Measure</label>\
  <div class="col-xs-9">\
    <input id="unitOfMeasure" name="unitOfMeasure" class="form-control" disabled="disabled" type="text" value="{{validation.unit_of_measure}}">\
  </div>\
</div>';

var stringValidationTemplate = '\
<div class="row form-group" >\
  <label for="max-length" class="col-xs-3 control-label">Maximum Length</label>\
  <div class="col-xs-9">\
    <div class="input-group">\
      <span class="input-group-addon">\
        <input id="max-length" name="max-length" disabled="disabled" type="checkbox" {{#validation.maximum_character_quantity}}checked="checked"{{/validation.maximum_character_quantity}} >\
      </span>\
      <input class="form-control" disabled="disabled" type="text" value="{{validation.maximum_character_quantity}}" placeholder="The maximum length">\
    </div>\
  </div>\
</div>\
<div class="row form-group">\
  <label for="use-regex" class="col-xs-3 control-label"">Use Regex</label>\
  <div class="col-xs-9">\
    <input id="use-regex" name="use-regex" disabled="disabled" type="checkbox" {{#validation.format}}checked="checked"{{/validation.format}}>\
  </div>\
</div>\
{{#validation.format}}\
  <div class="row form-group">\
    <label for="format" class="col-xs-3 control-label">Regex</label>\
    <div class="col-xs-9 form-control-static">\
      <input id="format" name="format"  class="form-control"disabled="disabled" type="text" value="{{validation.format}}">\
    </div>\
  </div>\
{{/validation.format}}';

var permittedValuesValidationTemplate = '\
<table class="table table-striped table-hover table-condensed">\
  <thead>\
    <tr>\
      <th scope="col" class="col-xs-3">Permitted Value</th>\
      <th scope="col" class="col-xs-1">Language</th>\
      <th scope="col" class="col-xs-4">Designation</th>\
      <th scope="col" class="col-xs-4">Definition</th>\
    </tr>\
  </thead>\
  <tbody>\
  {{#validation.permissible_values}}\
    <tr class="tableVCenter">\
      <td rowspan="{{rowspan}}">{{value}}</td>\
    </tr>\
    {{#meanings}}\
      <tr>\
        <td>{{language}}</td>\
        <td>{{designation}}</td>\
        <td>{{definition}}</td>\
      </tr>\
    {{/meanings}}\
  {{/validation.permissible_values}}\
  </tbody>\
</table>';

var datetimeValidationTemplate = '\
<div class="row form-group">\
  <label for="datetime" class="col-xs-3 control-label"">Representation (WIP)</label>\
  <div class="col-xs-9">\
    <input id="datetime" name="datetime" class="form-control" disabled="disabled" type="text" value="{{validation.format}}">\
  </div>\
</div>\
<div class="row form-group">\
  <label for="info" class="col-xs-3 control-label"">Information (WIP)</label>\
  <div class="col-xs-9">\
    <input id="info" name="info" class="form-control" disabled="disabled" type="text" value="{{validation.validation_data}}">\
  </div>\
</div>';

var slotsTemplate = '\
<div class="row">\
  <div class="col-xs-12">\
    <div class="panel panel-default">\
      <div class="panel-heading">\
        <h3 class="panel-title" style="text-align: left; font-weight: bold">Slots</h3>\
      </div>\
      <div class="panel-body">\
        <table class="table table-striped table-hover table-condensed">\
          <thead>\
            <tr>\
              <th scope="col" class="col-xs-5">Name</th>\
              <th scope="col" class="col-xs-7">Value</th>\
            </tr>\
          </thead>\
          <tbody>\
          {{#slots}}\
            <tr class="tableVCenter">\
              <td>{{slot_name}}</td>\
              <td>{{slot_value}}</td>\
            </tr>\
          {{/slots}}\
          </tbody>\
        </table>\
      </div>\
    </div>\
  </div>\
</div>';

var validationTypeTemplates = {
  numeric: numericValidationTemplate,
  string: stringValidationTemplate,
  permittedValues: permittedValuesValidationTemplate,
  datetime: datetimeValidationTemplate
};

var defaultPartials = {
  designationsTemplate: designationsTemplate,
  validationTemplate: validationTemplate,
  slotsTemplate: slotsTemplate
};
