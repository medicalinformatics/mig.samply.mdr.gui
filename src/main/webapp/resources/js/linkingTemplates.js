var linkTableRow = '\
<tr class="tableVCenter linkRow">\
  <td class="linkedElementLeft" data-urn="{{urnLeft}}">{{nameLeft}}</td>\
  <td>\
    <select class="form-control">\
      <option value="undefined" title="The link type between the concept and the data element is not specified.">undefined</option>\
      <option value="equal" title="The definitions of the concepts are exactly the same (i.e. only grammatical differences) and structural implications of meaning are identical or irrelevant (i.e. intentionally identical).">equal</option>\
      <option value="equivalent" title="The definitions of the concepts mean the same thing (including when structural implications of meaning are considered) (i.e. extensionally identical).">equivalent</option>\
      <option value="wider" title="The target mapping is wider in meaning than the source concept.">wider</option>\
      <option value="subsumes" title="The target mapping subsumes the meaning of the source concept (e.g. the source is-a target).">subsumes</option>\
      <option value="narrower" title="The target mapping is narrower in meaning than the source concept. The sense in which the mapping is narrower SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally.">narrower</option>\
      <option value="specializes" title="The target mapping specializes the meaning of the source concept (e.g. the target is-a source).">specializes</option>\
      <option value="inexact" title="The target mapping overlaps with the source concept, but both source and target cover additional meaning, or the definitions are imprecise and it is uncertain whether they have the same boundaries to their meaning. The sense in which the mapping is inexact SHALL be described in the comments in this case, and applications should be careful when attempting to use these mappings operationally.">inexact</option>\
    </select>\
  </td>\
  <td class="linkedElementRight" data-urn="{{urnRight}}">{{nameRight}}</td>\
  <td><button class="deleteLink btn btn-xs btn-danger" type="button"><i class="fa fa-remove"></i></button></td>\
</tr>';
