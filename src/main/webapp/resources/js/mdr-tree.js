function buildTree(containerClass, selectionId, rest_url, clickFunction, selectedTarget) {
  var tree = new InspireTree({
    selection: {
      mode: 'default'
    },
    data: function (node) {
      return $.ajax({
        type: "GET",
        url: rest_url + (node ? node.id : 'root'),
        data: {
          status: ['RELEASED']
        },
        traditional: true,
        dataType: 'json',
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", 'Bearer '+ accessToken);
        }
      }).fail(function (err)  {
        console.log(err)
      });
    }
  });

  new InspireTreeDOM(tree, {
    target: containerClass
  });

  tree.on('node.click', function (event, node) {
    clickFunction(event, node, selectionId, selectedTarget)
  })
  .on('children.loaded', function(node) {
    addBadges();
    setBorders();
  });

  return tree;
}

function blockSelectedElement(id) {
  $(id).block({
    css: {
      border: 'none',
      padding: '15px',
      backgroundColor: '#000',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: .5,
      color: '#fff'
    }
  });
}

function getAncestors(node) {
  var ancestors = [];
  while(node.getParent() != null) {
    node = node.getParent();
    var ancestorObject = new AncestorNode(node.id, node.text);
    ancestors.push(ancestorObject);
  }
  return ancestors;
}

function getPath(node) {
  var pathArray = getAncestors(node).map(function (ancestor) {return ancestor.text}).reverse();
  pathArray.push(node.text);
  return pathArray.join(' / ');
}

function AncestorNode(id, text) {
  this.id = id;
  this.text = text;
}

function setBorders() {
  $('a[data-elementtype]').each(function () {
    var element = $(this);
    var parent = element.parent();
    if (!parent.hasClass('staging-tree-item')) {
      parent.addClass('staging-tree-item formItem-' + element.data('elementtype'));
    }
  });
}

function addBadges() {
  $('a[data-createdurn]:not(:has("i"))').each(function () {
    var element = $(this);
    element.prepend("<i class=\"fa fa-fw fa-check text-success\" title=\"This dataelement has been imported as " + element.data('createdurn') + "\" />");
  });
  $('a[data-convertedat]:not(:has("i"))').each(function () {
    var element = $(this);
    element.prepend("<i class=\"fa fa-fw fa-check text-success\" title=\"This import has been converted on " + element.data('convertedat') + "\" />");
  });
}
