var scrollPosition;

/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
$(function() {
    $(".btn-load").click(function(event) {
        $(this).parent().find(".btn-load").addClass("disabled");
    });
    $(".positiveinteger").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });
});

/**
 * Enables select2 for all select fields with the select2-class
 */
function enableSelect2() {
    $("select.select2").select2({
        minimumResultsForSearch : 10
    });
}

/**
 * Activates the drag functionality in .sortableItemPanel
 * and .draggable
 */
function activateDrag() {
    $(".sortableItemPanel").sortable({
        start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
        stop : function(event, ui) {
            refreshItems();
            $("#refreshItems").click();
        },
        helper: "clone",
        appendTo: document.body
    });

    $(".draggable").draggable({
        connectToSortable: ".sortableItemPanel",
        revert: "invalid",
        helper: "clone",
        appendTo: document.body,
        stop: function(event, ui) {
            // FIXME: this is really ugly. Waiting 200ms and then call the activateDrag function
            // Because the MDR uses a fix for the draggable and sortable, dropping a draggable widget immediately results
            // in a widget, that is not draggable anymore.
            setTimeout(function(){
                activateDrag();
            }, 200);
        }
    });
}

/**
 * Refreshes the items, using the .formItems
 */
function refreshItems() {
    var data = "";

    if ($(".sortableItemPanel").find(".formItem").length > 0) {
        var newElements = new Array();

        $(".sortableItemPanel").find(".formItem").each(function(i, el) {
            var urn = $(el).find(".urn").html();
            if(newElements.indexOf(urn) == -1) {
                newElements.push(urn);
            }
        });

        $("#items").val(newElements.join(","));

        $("#dragInfo").hide();
    } else {
        $("#items").val("");
        $("#dragInfo").show();
    }
}

/**
 * Called when a "remove" button has been clicked
 * @param element
 */
function removeItem(element) {
    var urn = $(element).parent().parent().parent().remove();
    refreshItems();
    $("#refreshItems").click();
}

function toggleItem(element) {
    var urn = $(element).parent().parent().find(".urn").html();
    $(".inputStarItemUrn").val(urn);
    $(".inputStarItemBtn").click();


    var others = $(".urn").filter(function() { return $(this).html() == urn; });
    var i = $(element).find("i");

    if(i.hasClass("text-muted")) {
        others.each(function(index) {
            $(this).parent().parent().parent().find(".starItem").switchClass("text-muted", "text-primary");
        });
    } else {
        others.each(function(index) {
            $(this).parent().parent().parent().find(".starItem").switchClass("text-primary", "text-muted");
        });
    }
}

function releaseItem(element) {
    var urn = $(element).parent().parent().find(".urn").html();
    $(".inputMarkItemUrn").val(urn);
    $(".inputMarkItemBtn").click();

    var i = $(element).find("i");

    if(i.hasClass("fa-check-square-o")) {
        i.removeClass().addClass("fa fa-fw fa-square-o");
    } else {
        i.removeClass().addClass("fa fa-fw fa-check-square-o");
    }
}

function selectStagedItem(element) {
    var elementId = $(element).data("elementid");
    $(".inputMarkItemUrn").val(elementId);
    $(".inputMarkItemBtn").click();

    var i = $(element).find("i");

    if(i.hasClass("fa-check-square-o")) {
        i.switchClass("fa-check-square-o", "fa-square-o");
    } else {
        i.switchClass("fa-square-o", "fa-check-square-o");
    }
}

function collapseItem(element, page) {
    var divp = $(element).parent().parent().parent();
    var urn = divp.find(".urn").html();
    var d = divp.find(".details").first();
    var draggable = divp.find(".formItem").first().hasClass("ui-draggable");


    // Close all other open root details, if the button clicked can not be found inside the
    // already open collapsible
    /*
    var rootDetails = divp.parent().find("div > .details.in");
    if(rootDetails.parent().find(element).length == 0) {
        rootDetails.collapse('toggle');
        divp.parent().find("i.fa-angle-up").switchClass("fa-angle-up", "fa-angle-down");
    }
    */

    // Load the shortDetail into the #details div
    if(! d.hasClass("in")) {
        if(page.indexOf("?") != -1) {
            page = page + "&";
        } else {
            page = page + "?";
        }

        var parameter = "urn";

        if(page.indexOf("urn=") != -1) {
            parameter = "node";
        }

        d.load(contextPath + page + "draggable=" + draggable + "&" + parameter + "=" + urn + " #detailView",
                function() {
            $(this).collapse('toggle');
            $(element).find("i").switchClass("fa-angle-down", "fa-angle-up");
            activateDrag();
            enableSelect2();
        });
    } else {
        d.collapse('toggle');
        $(element).find("i").switchClass("fa-angle-up", "fa-angle-down");
    }
}

function collapseStagingItem(element, page) {
    var divp = $(element).parent().parent().parent();
    var elementId = divp.find(".elementId").html();
    var d = divp.find(".details").first();

    // Load the shortDetail into the #details div
    if(! d.hasClass("in")) {
        if(page.indexOf("?") != -1) {
            page = page + "&";
        } else {
            page = page + "?";
        }

        var parameter = "id";

        if(page.indexOf("id=") != -1) {
            parameter = "node";
        }

        d.load(contextPath + page + "&" + parameter + "=" + elementId + " #detailView",
            function() {
                $(this).collapse('toggle');
                $(element).find("i").switchClass("fa-angle-down", "fa-angle-up");
                enableSelect2();
            });
    } else {
        d.collapse('toggle');
        $(element).find("i").switchClass("fa-angle-up", "fa-angle-down");
    }
}

function toggleNamespaceView(element) {
    var ns = $(element).closest(".results-pane").find(".namespacelist-pane");
    var elements = $(element).closest(".results-pane").find(".elements-pane");
    if(elements.hasClass("col-xs-8")) {
        ns.hide();
        elements.switchClass("col-xs-8", "col-xs-12", 0);
    } else {
        ns.show();
        elements.switchClass("col-xs-12", "col-xs-8", 0);
    }
}

function selectNamespace(element, input, button) {
    var height = $("#wrapperResults").height();

    $("#wrapperResults").css({"min-height": height});

    $(element).parent().parent().find("li").removeClass("active");
    $(element).parent().addClass("active");

    var name = $(element).find("span.name").html();

    $(input).val(name);

    $(button).click();
}

function activateFilter() {
    $(".namespace-filter").keyup(function(ev) {
        var filter = $(this).val().toLowerCase();

        var pane = $(this).closest(".results-pane").find(".namespacelist-pane");

        if(filter == "") {
            pane.find("ul li.namespace").show();
        } else {
            pane.find("ul li.namespace").each(function(i, el) {
                var definition = $(el).find(".definition").html().toLowerCase();
                var designation = $(el).find(".designation").html().toLowerCase();
                if(definition.indexOf(filter) >= 0 || designation.indexOf(filter) >= 0) {
                    $(el).show();
                } else {
                    $(el).hide();
                }
            });
        }
    });
}

function activateSelectableCatalog() {
    $(".formItem-catalog").click(function(event) {
        var urn = $(this).find(".urn").html();
        $(".catalogURN").val(urn);

        $(".formItem-catalog").removeClass("invert");
        $(this).addClass("invert");
    });
}

function deselectAll(element) {
    $(".selectedCode").val($(element).closest(".formItem").find(".urn").html());
    $(".deselectAll").click();

    $(element).parent().parent().parent().find("i").removeClass().addClass("fa fa-square-o elementCheckState");
    $(element).parents(".elementDetail").siblings().find(".elementCheckState").each(function(e) {
        $(this).switchClass("fa-check-square-o", "fa-minus-square-o");
    });
    $(element).closest(".formItem").siblings().find(".elementCheckState").removeClass().addClass("fa fa-square-o elementCheckState");

    updateParents(element);
}

function selectAll(element) {
    $(".selectedCode").val($(element).closest(".formItem").find(".urn").html());
    $(".selectAll").click();

    $(element).parent().parent().parent().find("i").removeClass().addClass("fa fa-check-square-o elementCheckState");

    $(element).closest(".formItem").siblings().find(".elementCheckState").removeClass().addClass("fa fa-check-square-o elementCheckState");

    updateParents(element);
}

function updateParents(element) {
    $($(element).parents(".elementDetail").siblings().get().reverse()).each(function() {
        var siblings = $(this).siblings();
        var count = siblings.find(".elementCheckState").size();
        var checked = siblings.find(".elementCheckState.fa-check-square-o").size();
        var minus = siblings.find(".elementCheckState.fa-minus-square-o").size();
        var unchecked = siblings.find(".elementCheckState.fa-square-o").size();

        var box = $(this).find(".elementCheckState");
        box.removeClass();

        if(checked == count) {
            box.addClass("fa fa-check-square-o elementCheckState");
        } else {
            box.addClass("fa fa-minus-square-o elementCheckState");
        }
    });
}

function scrollTo(selector) {
    $('html').animate({
        scrollTop: $(selector).offset().top
    }, 1000);
}

function changeStagingPage(page, pageSize) {
    var pageInt = parseInt(page);
    var pageSizeInt = parseInt(pageSize);
    var startIndex = (pageInt-1) * pageSizeInt;
    var endIndex = (pageInt * pageSizeInt);

    // Hide all entries first and then display all from page*pagesize to (page+1)*pagesize
    $('#elements').find('[data-index]').hide();
    for (i = startIndex; i < endIndex; i++) {
        $('#elements').find('[data-index=' + i + ']').show();
    }
}

function initPagination(pageSize) {
    $('.pagination-md').twbsPagination({
        totalPages: $('#pagination-top').children('li').length,
        visiblePages: 15,
        first: '&lt;&lt;',
        prev: '&lt;',
        next: '&gt;',
        last: '&gt;&gt;',
        initiateStartPageClick: false,
        hideOnlyOnePage: true,
        onPageClick: function (event, page) {
            changeStagingPage(page, pageSize);
        }
    });
}
