var TREE_URL = REST_URL + '/tree/';
var DATAELEMENT_URL = REST_URL + '/dataelements/';
var DATAELEMENTGROUP_URL = REST_URL + '/dataelementgroups/';

var selectionIdLeft = '#selected_element_left';
var selectionIdRight = '#selected_element_right';
var selectedFilesId = '#selectedFiles';
var saveButtonId = '#save_button';
var loadButtonId = '#load_button';
var linkButtonId = '#link_button';
var linkedElementsId = '#linked_elements';

var treeClassLeft = '.namespaces_tree_left';
var treeClassRight = '.namespaces_tree_right';
var linkedElementClassLeft = '.linkedElementLeft';
var linkedElementClassRight = '.linkedElementRight';
var formItemSelectedClass = 'formItem-selected';
var deleteLinkClass = '.deleteLink';

var urnDataAttr = 'data-urn';

var selectedLeft = {target: null};
var selectedRight = {target: null};

$( document ).ready(function() {
  var treeLeft = buildTree(treeClassLeft, selectionIdLeft, TREE_URL, treeClickFunction, selectedLeft);
  var treeRight = buildTree(treeClassRight, selectionIdRight, TREE_URL, treeClickFunction, selectedRight);

  $('body').on('click', deleteLinkClass, function() {
    $(this).parent().parent().remove();
  }).on('click', linkedElementClassLeft, function (e) {
    linkedElementClickFunction(e, selectionIdLeft, selectedLeft);
  }).on('click', linkedElementClassRight, function (e) {
    linkedElementClickFunction(e, selectionIdRight, selectedRight);
  });

  $(linkButtonId).on('click', function() {
    if (selectedLeft.target.context.text && selectedRight.target.context.text) {
      var data = {
        nameLeft: selectedLeft.target.context.text,
        urnLeft: selectedLeft.target.context.dataset.uid,
        nameRight: selectedRight.target.context.text,
        urnRight: selectedRight.target.context.dataset.uid
      };
      var rendered = Mustache.render(linkTableRow, data);
      $(linkedElementsId).append(rendered);
    }
  });
  renderExistingLinks();
});

function getUrnRequestUrl(urn) {
  return (urn.includes("dataelementgroup") ? DATAELEMENTGROUP_URL : DATAELEMENT_URL) + urn;
}

function linkedElementClickFunction(e, selectionId, selectedTarget) {
  var target = $(e.currentTarget);
  var urn = target.attr(urnDataAttr);
  var url = getUrnRequestUrl(urn);
  var path = urn.split(':')[1] + ' / ' + target.html();
  openElement(url, selectionId, path, selectedTarget, target);
}

function treeClickFunction(event, node, selectionId, selectedTarget) {
  var target = $(event.currentTarget);
  // Don't do anything when namespace or import is clicked
  if (target.is("a") && !node.id.startsWith("ns_")) {
    var url = getUrnRequestUrl(node.id);
    blockSelectedElement(selectionId);
    openElement(url, selectionId, getPath(node), selectedTarget, target);
    event.preventTreeDefault();
  }
}

function openElement(url, selectionId, path, selectedTarget, target) {
  blockSelectedElement(selectionId);

  $.ajax({
    type: "GET",
    url: url,
    dataType: 'json',
    beforeSend: function (xhr) {
      xhr.setRequestHeader("Authorization", 'Bearer '+ accessToken);
    }
  }).fail(function (err)  {
    console.log(err)
  }).done(function(data) {
    data.path = path;
    data.elementtype = data.identification.elementtype.toLowerCase();
    if (data.validation) {
      if (data.validation.maximum_character_quantity) {
        // unfortunately the max char quantity is a string, not an int, thus we convert here
        data.validation.maximum_character_quantity = parseInt(
            data.validation.maximum_character_quantity);
      }
      if (data.validation.permissible_values) {
        // set the rowspan for permissible values, we can't use length because we need an additional row in the table
        for (var i = 0; i < data.validation.permissible_values.length; i++) {
          data.validation.permissible_values[i].rowspan = data.validation.permissible_values[i].meanings.length + 1;
        }
      }
    }

    var partials = defaultPartials;
    partials.validationTypeTemplate = getValidationTypeTemplate(data);

    var rendered = Mustache.render(elementTemplate, data, partials);
    $(selectionId).empty().append(rendered);
    $(selectionId).unblock();

    selectTarget(selectedTarget, target);
  });
}

function selectTarget(selectedTarget, target) {
  deselectTarget(selectedTarget);
  selectedTarget.target = target;
  target.addClass(formItemSelectedClass);
}

function deselectTarget(selectedTarget) {
  if (selectedTarget.target) {
    selectedTarget.target.removeClass(formItemSelectedClass);
  }
}

function getValidationTypeTemplate(data) {
  if (!data || !data.validation || !data.validation.datatype) {
    return '';
  }

  switch (data.validation.datatype) {
    case 'INTEGER': return validationTypeTemplates.numeric;
    case 'FLOAT': return validationTypeTemplates.numeric;
    case 'STRING': return validationTypeTemplates.string;
    case 'enumerated': return validationTypeTemplates.permittedValues;
    case 'DATE':
    case 'DATETIME':
    case 'TIME': return validationTypeTemplates.datetime;
    // boolean, TBD, catalog
    default: return '';
  }
}

function jsonifyLinks() {
  var links = [];
  $(linkedElementsId).children().each(function() {
    var tds = $(this).children();
    var link = {
      nameLeft: $(tds[0]).text(),
      urnLeft: $(tds[0]).attr(urnDataAttr),
      relation: $(tds[1]).find(':selected').val(),
      nameRight: $(tds[2]).text(),
      urnRight: $(tds[2]).attr(urnDataAttr)
    };
    links.push(link);
  });
  document.getElementById("linked_elements_json").value = JSON.stringify(links);
  return true;
}

function renderExistingLinks() {
  var result = JSON.parse($("#linked_elements_json").val());
  result.forEach(function(obj) {
    $('#linked_elements').append(Mustache.render(linkTableRow, obj));
    $('#linked_elements select').last().val(obj.relation).change();
  });
}