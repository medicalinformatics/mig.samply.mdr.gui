var changes = 0;

var OPTIONS = '#options-btn';
var SEARCHBAR = '#recommendationsearchbar';
var RECOMMENDATIONS = '#recommendations';
var LIMIT = '#limit';
var THRESHOLD = '#threshold';

var RECOMMENDATION_BTN = '.recommendation-btn';
var SEARCH_OPTIONS = '.search-options';
var ADDITIONAL_LANGUAGES = '.additional-languages';

var BASE_URL = 'recommendation_datasource.xhtml?';
var SEARCH_PARAM = 'searchstring=';
var LIMIT_PARAM = 'limit=';
var THRESHOLD_PARAM = 'threshold=';

function getDefinitionsTable(id, definitions) {
  var table =
      '<div id="table-' + id + '" class="languages-table">' +
      '  <table class="table table-striped table-condensed">'+
      '    <thead>' +
      '      <tr>' +
      '        <th scope="col" class="col-xs-1">' + msg.language + '</th>' +
      '        <th scope="col" class="col-xs-4">' + msg.definition + '</th>' +
      '        <th scope="col" class="col-xs-7">' + msg.designation + '</th>' +
      '      </tr>' +
      '    </thead>' +
      '    <tbody>';

  definitions.forEach(function(definition) {
    table +=
        '<tr class="tableVCenter">\n' +
        '  <td>' + definition.language.toUpperCase() + '</td>' +
        '  <td>' + definition.designation + '</td>' +
        '  <td>' + definition.definition + '</td>' +
        '</tr>'
  });

  table +=
      '    </tbody>' +
      '  </table>' +
      '</div>';
  return table;
}

function recommendationClick() {
  var element = $(this);
  var selected = element.data('selected');
  $(RECOMMENDATION_BTN).each(function() {
    var innerElement = $(this);
    innerElement.data('selected', false);
    innerElement.removeClass('btn-primary');
    innerElement.addClass('btn-default');
    innerElement.text(msg.select);
  });
  if (!selected) {
    element.data('selected', true);
    element.addClass('btn-primary');
    element.removeClass('btn-default');
    element.text(msg.deselect);
    selectedUrn = element.data('id');
  } else {
    selectedUrn = "";
  }
  setSelectedUrn({
    urn: selectedUrn
  });
}

function toggleAdditionalLanguages() {
  var element = $('#table-' + $(this).data('id').replace(/:/g, '\\:'));
  if(element.is(':hidden')) {
    element.show();
    $(this).html('<b>' + msg.hideAdditionalLanguages + '</b>');
  } else {
    element.hide();
    $(this).html('<b>' + msg.showAdditionalLanguages + '</b>');
  }
}

function requestRecommendations(data) {
  $(RECOMMENDATIONS).html(
      '<h4>' + msg.foundRecommendations + ':</h4>'
      + '<hr class="hr-light"/>'
  );

  data.forEach(function(element) {
    // scale similarity range from 0-1 to 0-100
    var defSim = element.definitionSimilarity;
    var defSimInverse = 100 - defSim;
    var desSim = element.designationSimilarity;
    var desSimInverse = 100 - desSim;

    var html =
        '<div class="row">' +
        '  <div class="col-md-12">' +
        '    <b class="element-title">' + element.designation + '</b>' +
        '    <p class="element-link">' + urnLink(element.urn) + '</p>' +
        '  </div>' +
        '</div>' +
        '<div class="row">' +
        '  <div class="col-md-10">' +
        '    <div class="row">' +
        '      <div class="col-md-2"><b>' + msg.designation + ':</b></div>' +
        '      <div class="col-md-9">' + element.designation + '</div>' +
        '      <div class="col-md-1">' + similarityBar(element.designationSimilarity) + '</div>' +
        '    </div>' +
        '    <div class="row">' +
        '      <div class="col-md-2"><b>' + msg.definition + '</b></div>' +
        '      <div class="col-md-9">' + element.definition + '</div>' +
        '      <div class="col-md-1">' + similarityBar(element.definitionSimilarity) + '</div>' +
        '    </div>' +
        '    <div class="row">' +
        '      <div class="col-md-2"><b>' + msg.datatype + '</b></div>' +
        '      <div class="col-md-10">' + (element.type !== undefined ? element.type.toUpperCase() : '') + '</div>' +
        '    </div>' +
        '    <div class="row">' +
        '      <div class="col-md-2"><b>' + msg.language + '</b></div>' +
        '      <div class="col-md-10">' + (element.language !== undefined ? element.language.toUpperCase() : '') + '</div>' +
        '    </div>';

    if (element.definitions.length > 0) {
      html +=
          '<br/>'+
          '<div class="row">' +
          '  <div class="col-md-10">' +
          '    <a data-id="' + element.urn + '" class="additional-languages">' +
          '      <b>' + msg.showAdditionalLanguages + '</b>' +
          '    </a>' +
          '  </div>' +
          '</div>'
    }

    html +=
        '</div>' +
        '  <div class="col-md-2">' +
        (selectedUrn === element.urn
            ? '<button type="button" class="btn btn-primary recommendation-btn pull-right" data-selected="true" data-id="' + element.urn + '">' + msg.deselect + '</button>'
            : '<button type="button" class="btn btn-default recommendation-btn pull-right" data-id="' + element.urn + '">' + msg.select  + '</button>') +
        '  </div>' +
        '</div>';

    if (element.definitions.length > 0) {
      html += getDefinitionsTable(element.urn, element.definitions)
    }

    html += '<hr class="hr-light"/>';

    $(RECOMMENDATIONS).append(html);
  });
  $(RECOMMENDATION_BTN).on('click', recommendationClick);
  $(ADDITIONAL_LANGUAGES).on('click', toggleAdditionalLanguages);
}

$(OPTIONS).on('click', function () {
  $(SEARCH_OPTIONS).each(function () {
    var element = $(this);
    if(element.is(':hidden')) {
      element.show();
    } else {
      element.hide();
    }
  });
});

function onInput() {
  changes++;
  var changesOnRequest = changes;
  setTimeout(function () {
    var searchedString = $(SEARCHBAR).val();
    if (changes === changesOnRequest && searchedString && searchedString !== '') {
      var URL = BASE_URL + LIMIT_PARAM + $(LIMIT).val() + '&' + THRESHOLD_PARAM
          + (parseInt($(THRESHOLD).val())) + '&' + SEARCH_PARAM
          + encodeURIComponent(searchedString);
      $.getJSON(URL, requestRecommendations);
      changes = 0;
    }
  }, 1000);
}

$(SEARCHBAR).on('input', onInput);
$(LIMIT).on('input', onInput);
$(THRESHOLD).on('input', onInput);

if (previouslySearched) {
  onInput();
}
