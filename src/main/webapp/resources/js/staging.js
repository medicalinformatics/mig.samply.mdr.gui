$( document ).ready(function() {
    var tree = new InspireTree({
        selection: {
            mode: 'default'
        },
        data: function(node) {
            return $.getJSON('staging_datasource.xhtml?node=' + (node ? node.id : 'root'));
        }
    });

    new InspireTreeDOM(tree, {
        target: '.namespaces_staging-tree'
    });

    tree.on('node.click', function(event, node) {
        var target = $(event.currentTarget);
        // Don't do anything when namespace or import is clicked
        if (target.is("a") && !node.id.startsWith("ns_")) {
            blockSelectedElement();
            var ancestors = getAncestors(node);
            var posting = $.post(contextPath + "/stagingDetail.xhtml?id=" + node.id, { 'ancestors' : JSON.stringify(ancestors) } );
            posting.done(function(data) {
                $('#selected_element').empty().append(data);
                $('#selected_element').unblock();
            });
            event.preventTreeDefault();
        }
    }).on('children.loaded', function(node) {
        addBadges();
        setBorders();
    });
});

function blockSelectedElement() {
    $('#selected_element').block({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function getAncestors(node) {
    var ancestors = [];
    while(node.getParent() != null) {
        node = node.getParent();
        var ancestorObject = new AncestorNode(node.id, node.text);
        ancestors.push(ancestorObject);
    }
    return ancestors;
}

function AncestorNode(id, text) {
    this.id = id;
    this.text = text;
}

function setBorders() {
    $('a[data-elementtype]').each(function () {
        var element = $(this);
        var parent = element.parent();
        if (!parent.hasClass('staging-tree-item')) {
            parent.addClass('staging-tree-item formItem-' + element.data('elementtype'));
        }
    });
}

function addBadges() {
    $('a[data-createdurn]:not(:has("i"))').each(function () {
        var element = $(this);
        element.prepend("<i class=\"fa fa-fw fa-check text-success\" title=\"This dataelement has been imported as " + element.data('createdurn') + "\" />");
    });
    $('a[data-convertedat]:not(:has("i"))').each(function () {
        var element = $(this);
        element.prepend("<i class=\"fa fa-fw fa-check text-success\" title=\"This import has been converted on " + element.data('convertedat') + "\" />");
    });
}